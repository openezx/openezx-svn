#!/bin/bash
##############################################################################
#	doit! v0.1.1
#
#	doit.sh is supposed to create a780/e680(i) software packages from standard
#	open sources.  I should probably modify it to actually create RPMs or
#	debian packages.  Tab = 4 spaces here.  It's also supposed to be generic.
#	This script was inspired by Dan Kegel's crosstool building script.
#
#   Copyright (C) 2006 Daniel Santos (daniel.santos@pobox.com)
#	http://people.openezx.org/daniel
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or (at your
#   option) any later version.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#	INSTRUCTIONS
#
#	doit! expects a config file named a780-*.config.  This file is read to
#	figure out what the hell it's doing.  This file must define the the
#	variables and functions listed under the CONFIG section.  doit! performs
#	7 basic tasks, each of which can be enabled or disabled from the .config
#	file by setting "run${task_name}" to 1 or 0.  An integer value for each
#	of these should be set.  Additionally, a pre-task can optionally be
#	defined in the config file for each of these tasks by creating a function
#	named "pre${task_name}" in the .config file.  These are the basic
#	(built-in) tasks.
#
#	* getTarball	 - Retrieves the tarball from the web if it's not already
#					   downloaded
#	* extractTarball - Extracts the tarball. If the tarball has already been
#					   extracted (i.e., you are running the build for a 2nd
#					   time), then the previous directory is first deleted.
#	* configureIt	 - Runs the configure script setting the prefix to
#					   ${installDir} (./install by default), the HOST to
#					   arm-linux and supplies anything you've set in
#					   configFlags. (called with eval)
#	* makeIt		 - Runs make supplying ${makeFlags} (called with eval).
#	* installIt		 - Runs make install, thus installing the software into
#					   ${installDir}
#	* dist			 - Strips all ELF binaries in ${installDir} and then
#					   tars the contents into the ${distDir} (./dist by
#					   default).  If ${includeUnstripped} is set, then a dbg
#					   copy is first tared into ${distDir} before it is
#					   stripped
#	* cleanUpMess	 - deletes all files and directories downloaded or
#					   otherwise created except for ${distDir} and it's
#					   contents.
#
#	CONFIGURATION FILE
#
#	You can pick up a sample configuration file at
#	http://people.openezx.org/daniel/a780-example.config
#
##############################################################################

die() {
	echo -e "\nERROR: $*\n"
	exit 1
}

# Filter function to run another function in the specified directory, but
# but only if runFunc is non-zero.
runOptFuncInDir() {
	local funcName="$1"
	local dir="$2"

	# grr..
	local upperFuncName=$(echo ${funcName:0:1} | tr 'a-z' 'A-Z')${funcName:1}

	eval typeset preFuncName=pre${upperFuncName}
	eval typeset -i  runFunc=run${upperFuncName}
	typeset -i ret

	typeset -F ${preFuncName} > /dev/null \
		&& ${preFuncName}

	if [ $runFunc -ne 0 ]; then
		pushd "${dir}" > /dev/null \
			|| die "failed to enter directory ${dir}"
		shift 2
		${funcName} "$@"
		ret=$?
		popd > /dev/null
		return $ret
	fi
}

# Get tarball if we need it
getTarball() {
	local tarball="$1"
	local url="$2"

	if [ ! -f ${tarball} ]; then
		echo -e "Downloading ${tarball} ...\n"
		wget "${url}"  || die "Download failed"
	else
		echo -e "${tarball} already downloaded...\n"
	fi
}

# Extract it, remove old if it was there before.
extractTarball() {
	local tarball="$1"
	local dir="$2"
	typeset -i allow_rm=$3

	if [ -e "${dir}" ]; then
		if [ $allow_rm -eq 0 ]; then
			die "${dir} already exists.  Can't continue."
		else
			echo -e "Removing old directory ${dir}...\n"
			rm -rf "${dir}" \
				|| die "Failed to remove directory ${dir}"
		fi
	fi
	echo -e "Extracting ${tarball}...\n"

	# Later versions of tar should be able to figure out if a tarball is
	# compressed and use the right decompression.
	tar xf "${tarball}" \
			|| die "Extract of ${tarball} failed."
}

configureIt() {
	local installDir="$1"
	local additionalFlags="$2"

	echo -e "Running configure...\n"
	eval ./configure '--prefix="${installDir}"' \
			--host=arm-linux \
			${additionalFlags} \
			|| die "configure failed.. bummer"
}

makeIt() {
	local makeFlags="$1"

	echo -e "Running make...\n"
	eval make "${makeFlags}" || die "Make failed.. bummer"
}

# Create install dir and make the install target
installIt() {
	local installDir="$1"

	if [ -e "${installDir}" ]; then
		echo -e "\n\nInstall directory ${installDir} exists.\nDelete it first? (Y/N) \c"
		read
		if [ "${REPLY:0:1}" = "y" -o "${REPLY:0:1}" = "Y" ]; then
			echo "Removing old install directory...\n"
			rm -rf "${installDir}"
		fi
	fi

	echo -e "Running make install...\n"
	mkdir -p "${installDir}" \
			|| die "failed to create directory ${installDir}"
	make install || die "Awww, all that work and then \"make install\" failed"
}

# Enter install dir and strip all ELF executables then create stripped
# distributable tarball
dist() {
	local distDir="$1"
	local pkgName="$2"
	typeset -i includeUnstripped=$3

	local f
	local strip_list=""

	mkdir -p "${distDir}" \
			|| die "failed to create directory ${distDir}"

	if [ $includeUnstripped -ne 0 ]; then
		tar czf "${distDir}/${pkgName}-bin-dbg.tgz" * \
				|| die "Failed to create tarball"
	fi

	# Strip the executables, elf files at least
	for f in $(find . -type f -perm /u=x); do
		if [ "$(cut -c 2-4 $f | head -1)" = "ELF" ]; then
			strip_list="$strip_list $f"
		fi
	done

	arm-linux-strip --strip-all $strip_list \
			|| die "Strip failed"

	tar czf "${distDir}/${pkgName}-bin.tgz" * \
			|| die "Failed to create tarball"
	cd ..
}


# Initialize
init() {
	if [ "$(dirname $0)" != "." -a "$(dirname $0)" != "$(pwd)" ]; then
		die "WRONG!  Run doit.sh from it's own directory please."
	fi

	. ./a780-*.config || die "Yer config file looks pretty ugly."

	echo "

This script was designed to work with ezx-crosstool-0.5
(http://lsb.blogdns.net/ezx-crosstool) and will ideally:

 * download ${packageName} version ${packageVersion}
 * extract it
 * patch it (if needed)
 * configure
 * make it
 * install it into ./install
 * create archives in ./dist (stripped and unstripped)
 * and optionally clean up the mess

Hit enter to continue, CTRL-C to abort, or any politition to make this world a
better place."
	read
}

cleanUpMess() {
	local crap="$*"
	echo -e "\nClean up crap? (leaves your ./dist files alone) (Y/N)? \c"
	read gnihtemos
	if [ "$gnihtemos" = "Y" -o "$gnihtemos" = "y" ]; then
		eval rm -rf ${crap}
	fi
}

celebrate() {
	echo -e "\n
Done, hurray!  Binaries contained in ${distDir}/${packageDir}-bin.tgz and unstriped
binaries are in ${distDir}/${packageBaseName}-bin-dbg.tgz"
}

main() {
	echo -e "\ndoit! version 0.1.1\n"
	init

	runOptFuncInDir getTarball . "${packageTarball}" "${packageUrl}"

	runOptFuncInDir extractTarball . "${packageTarball}" "${packageDir}" 1

	runOptFuncInDir configureIt "${packageDir}" "${installDir}" "${configFlags}"

	runOptFuncInDir makeIt "${packageDir}" "${makeFlags}"

	runOptFuncInDir installIt "${packageDir}" "${installDir}"

	runOptFuncInDir dist "${installDir}" "${distDir}" "${packageBaseName}" ${includeUnstripped}

	runOptFuncInDir cleanUpMess . ${installDir} ${packageDir}

	celebrate
}

main
