/***************************************************************************
 *   Copyright (C) 2006 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERR(val) { printf("%s\n%s\n",val, ""); return -1; };
#define TEMPDIR ""

int chr2int(char ch)
{
        if (ch>='0' && ch <='9') ch-='0'; else
                if (ch>='A' && ch <='F') ch=ch-'A'+10;
        return ch;
}




int parseFlash(char *fn)
{
	#define BUFSIZE 10240
	
	#define LOADBUF { if (bufpos>=bufloaded) { bufloaded=fread(buf,1,BUFSIZE,fp); \
	if (!bufloaded) break; \
	bufpos=0; } }
	
	#define NEXTCH { LOADBUF; ch=buf[bufpos++]; }
	
	#define HEADER "P2K Superfile"
	
	printf("Parsing file %s\n",fn);
	FILE *fp;
	FILE *target;
	FILE *list;
	char buf[BUFSIZE];
	char outbuf[BUFSIZE];
	int bufpos, bufloaded,ps;
	char ch;
	char s[1024];
	int addr,i;
	int val;
	int outpos;
	int needBytes;
	
	fp=fopen(fn,"r");
	if (fp==NULL) ERR("Unable to open file");
	
	sprintf(s,"list.txt");
	list=fopen(s,"w");
	
	bufpos=0;
	bufloaded=0;
	
	//Checking for header...
	ch=0; strcpy(s,HEADER); ps=0;
	do
	{
		NEXTCH;
		if (ch!=s[ps++]) break;
		if (ps>=strlen(s)) break;
	}
	while (1);
	
//	if (ps<strlen(s)) ERR("This is not P2K flash file.\n");
		
	target=0;
	addr=0;
	needBytes=0;
	outpos=0;
	int was7=1;
	
	do
	{
		NEXTCH;
		if (ch=='S')
		{
			NEXTCH;
			switch (ch)
			{
				case '0': //Begin of codegroup
				case '7': //End of codegroup
				{
					printf("CH=%c was7=%d\n",ch,was7);
					if (was7) break;
					was7=1;
					if (target)
					{
						if (outpos) fwrite(outbuf,1,outpos,target);
						fclose(target);
					}
					target=0;
					outpos=0;
					break;
				}
				
				case '3': //Codegroup line
				{
					was7=0;
					NEXTCH;
					val=0;
					val<<=4; val+=chr2int(ch);
					NEXTCH;
					val<<=4; val+=chr2int(ch);
					needBytes=val-5;

					addr=0;
					for (i=0; i<8; i++)
					{
						NEXTCH;
						addr<<=4;
						addr+=chr2int(ch);
					}
					if (!target)
					{
						sprintf(s,"%s%08x.bin",TEMPDIR,addr);
						target=fopen(s,"w");
						outpos=0;
						fprintf(list,"%08x\n",addr);
					}
					break;
				}
			}
		}
		else
		{
			if (target && needBytes && ((ch>='0' && ch <='9') || (ch>='A' && ch <='F')))
			{
				val=0;
				val<<=4; val+=chr2int(ch);
				NEXTCH;
				val<<=4; val+=chr2int(ch);
				needBytes--;
				outbuf[outpos++]=val;
				was7=0;
				if (outpos>=BUFSIZE)
				{
					fwrite(outbuf,1,outpos,target);
					outpos=0;
				}
			}
		}
	}
	while (1);
		
	fclose(fp);
	fclose(list);
}


int main(int argc, char *argv[])
{
	if (argc!=2)
	{
	    printf("USAGE: unshx file.shx\n");
	    return 0;
	}
	if (parseFlash(argv[1])<0) return 0; 
 	return 0;
}
