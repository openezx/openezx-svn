/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PHONE_H
#define PHONE_H

#include <QtGui>

#include <p2kmoto.h>

class Phone : public QThread
{
	Q_OBJECT
private:
	QMutex mutex;
	struct flash_device *dev;
	int devCnt;

	int action;
	int actEnabled;
	int wasOpened;
	int idx1;
	unsigned int addr1;
	unsigned int addr2;
	QString s1;
	QString s2;
	unsigned int totalSize;
	unsigned int totalPos;

	int setAction (int act, int q=0);
	void clearAction();
	int getAction();

	int doUpdateList();
	void doPlug(int);
	void doSendCommand(const QString&cmd, const QString&arg);
	void doRead();
	int doSendAddr(unsigned int addr);
	int doSendFile(unsigned int addr, const QString&fn,int align=0);
	int doSendLoader(unsigned int addr1, unsigned int addrJump,  const QString&fn);
	int doJump(unsigned int addr);
	int doErase();
	int doFullFlash(const QString&fn);
	char crc(char *buf, int sz);
public:
	Phone(QObject *parent =0);
	~Phone();

protected:
	void run();
public:
	void setDevice(int idx);
	void sendCmd(const QString&cmd, const QString& arg);
	void sendFile(const QString&addr, const QString&fn);
	void sendLoader(const QString&addr1, const QString&addr2, const QString&fn);
	void jump(const QString&addr);
	void erase();
	void fullFlash(const QString&fn);
signals:
	void onDeviceList(void*, int);
	void onMessage(const QString &msg);
	void onProgress1(int a, int b);
	void onProgress2(int a, int b);
};

#endif
