/*
 * include/blob/lcd_ezx.h
 *
 * Copyright (C) 2000,  Intel Corporation.
 * Copyright (C) 2003-2006 Motorola Inc.
 *
 * 2003-Mar-18 - (Motorola) Remove lcd descriptor and frambuffer struct defs
 * 2004-May-17 - (Motorola) add the GPIO macro for EZX.
 * 2006-Jan-17 - (Motorola) Modified for EZX platform
 */

#ifndef HEADER_LCD_EZX
#define HEADER_LCD_EZX

/* USB client related GPIO pin */
#define GPIO34_TXENB_MD     (34 | GPIO_ALT_FN_1_OUT)
#define GPIO35_XRXD_MD      (35 | GPIO_ALT_FN_2_IN )
#define GPIO36_VMOUT_MD     (36 | GPIO_ALT_FN_1_OUT)
#define GPIO39_VPOUT_MD     (39 | GPIO_ALT_FN_1_OUT)
#define GPIO40_VPIN_MD      (40 | GPIO_ALT_FN_3_IN )
#define GPIO53_VMIN_MD      (53 | GPIO_ALT_FN_2_IN )

static void EnableLCD_8bit_active(void){
  return;
}

static void printlcd(char *cntrl_string){
  return;
}

static void Delay(int uSec)
{
        int delaytime;
        delaytime = (int)(uSec*3.6864);
        OSCR = 0;
        while( OSCR > 50)
                ;
        while((OSCR) < delaytime)
                ;
}

#endif
