/*
 *  net.c : Simple network layer with TFTP/ICMP builtin
 *
 *  Copyright (c) 2003, Intel Corporation (yu.tang@intel.com)
 *  Copyright (C) 2002 Motorola
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  Oct 30,2002 - (Motorola) Add flags to support auto tool
 *                           Add htons and ntohs helper functions
 *                           Use SerialOutputString instead of printf
 *
 */

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/arch.h>
#include <blob/command.h>
#include <blob/serial.h>
#include <blob/util.h>
#include <blob/lcd_ezx.h>
#include <net.h>

//add global var for done flag
extern int auto_state,auto_busy;
extern char* img_args;
//add state var and val for auto tool
#define AUTO_STATE_CONF 0	/*state for getting conf file*/
#define AUTO_STATE_AUTO 1	/*state for start per image*/

//add flags and val for auto tool
#define AUTO_FLAG_BUSY 1
#define AUTO_FLAG_NOBUSY 0

#define DOWNLOAD_MEM_ADDR "0xa0100000" /*should be same as in main.c*/
//  extern func
char * strcat(char * dest, const char * src);

static struct mybuf in = {0} ,out1 = {0}, out2 = {0};
static unsigned char broadcast_mac_addr[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
static unsigned char broadcast_ip_addr[4] = {0xff, 0xff, 0xff, 0xff};

static unsigned char mac_addr[6] = {0x0, 0x0, 0x0, 0x0, 0xf, 0xe};
static unsigned char ip_addr[4] = {192, 1, 1, 2};

static unsigned char svr_mac_addr[6];
static unsigned char svr_ip_addr[4] = {192,1,1,1};
static unsigned short our_tftp_port = 0;

static char image_name[256];
static unsigned long image_addr;

static int last_block_num;
static unsigned short ip_id=0;

static int tftp_state = 0;

#define MIN(a,b) ((a)<(b) ? (a): (b))

static void arp_input();
static void ip_input();
static void icmp_input();
static void udp_input();

/*
static unsigned short ntohs(unsigned short x)
{
	return (x >> 8) | ( ( x & 0xff) << 8);
}

static unsigned short htons(unsigned short x)
{
	return ( (x >> 8) & 0xff) | ( ( x & 0xff) << 8);
}
*/

/*
 * Very simple buffer management.
 *
 * io = 0 : request for input buffer
 * io = 1 : request for output buffer
 *
 * set buffer length to 0 to free the buffer.
 *
 */
struct mybuf * bget(int io)
{
	if( io == 1) {
		if(out1.len == 0) return &out1;
		if(out2.len == 0) return &out2;
	}else return &in;

	SerialOutputString("can't get buffer\n");
	return 0;
}

static unsigned short in_chksum(unsigned char *p, int len)
{
	unsigned long sum=0;

	while(len > 1) {
		sum += *((unsigned short*)p)++;
		if( sum & 0x80000000 )
			sum = (sum & 0xFFFF) + (sum >> 16);
		len -= 2;
	}

	if(len)
		sum += (unsigned short) *(unsigned char*) p;

	while(sum >> 16)
		sum = (sum & 0xFFFF) + (sum >> 16);

	return ~sum;
}



/* ARP zone */

static void arp_input()
{
	int op;
	struct etherhdr *eh, *ehout;
	struct ether_arp *ea, *eaout ;
	struct mybuf *out;

	out = bget(1);
	if(!out) return ;

	eh = (struct etherhdr*) (in.buf);
	ea = (struct ether_arp*)(eh + 1 );

	ehout = (struct etherhdr*) (out->buf);
	eaout = (struct ether_arp*) (ehout + 1);

	/* sanity check */
	if ( ntohs(ea->ea_hdr.ar_hrd) != ARPHDR_ETHER ) {
		SerialOutputString("check 1\n");
	       	return;
	}
	if ( ntohs(ea->ea_hdr.ar_pro) != ETHPROTO_IP ) {
		SerialOutputString("check 2\n");
		return;
	}
	if ( ea->ea_hdr.ar_hln != 6) {
		SerialOutputString("check 3\n");
		return;
	}

	if ( ea->ea_hdr.ar_pln != 4) {
		SerialOutputString("check 4\n");
		return;
	}

	//SerialOutputString("Checking pass\n");

	/* matching ip */

	op = ntohs(ea->ea_hdr.ar_op);

	switch(op) {
	case ARP_REQUEST:
		//SerialOutputString("ARP_REQUEST\n");
		*ehout = *eh;
		*eaout = *ea;

		if ( !memcmp(ea->arp_spa, svr_ip_addr, 4) ) {
			memcpy(svr_mac_addr, ea->arp_sha, 6);
		}

		memcpy(ehout->eh_dhost, eh->eh_shost, 6);
		memcpy(ehout->eh_shost, mac_addr, 6);

		eaout->ea_hdr.ar_op = htons(ARP_REPLY);

		memcpy(eaout->arp_tha, ea->arp_sha, 6);
		memcpy(eaout->arp_tpa, ea->arp_spa, 4);

		memcpy(eaout->arp_sha, mac_addr, 6);
		memcpy(eaout->arp_spa, ip_addr, 4);

		out->len = sizeof(struct etherhdr) + sizeof(struct ether_arp);

		/* send it */
		eth_xmit(out);

		break;
	case ARP_REPLY:
		//SerialOutputString("ARP_REPLY\n");
		if( (!memcmp(eh->eh_dhost, mac_addr, 6)) && ( !memcmp(ea->arp_spa, svr_ip_addr, 4) ) ) {
			//SerialOutputString("got several mac address\n");
			memcpy(svr_mac_addr, ea->arp_sha, 6);
			do_tftp();
		}
		break;
	case ARP_REVREQUEST:
		//SerialOutputString("ARP_REVREQUEST\n");
		break;
	case ARP_REVREPLY:
		//SerialOutputString("ARP_REVREPLY\n");
		break;
	}
}

static void arp_request()
{
	struct etherhdr *eh;
	struct ether_arp *ea;
	struct mybuf *out;

	out = bget(1);
	if(!out) return;

	eh = (struct etherhdr *) (out->buf);
	ea = (struct ether_arp *) (eh + 1 );

	memcpy(eh->eh_dhost, broadcast_mac_addr, 6);
	memcpy(eh->eh_shost, mac_addr, 6);
	eh->eh_proto = htons( ETHPROTO_ARP);

	ea->ea_hdr.ar_hrd = htons(ARPHDR_ETHER);
	ea->ea_hdr.ar_pro = htons(ETHPROTO_IP);
	ea->ea_hdr.ar_hln = 6;
	ea->ea_hdr.ar_pln = 4;
	ea->ea_hdr.ar_op = htons(ARP_REQUEST);

	memcpy(ea->arp_sha, mac_addr, 6);
	memcpy(ea->arp_spa, ip_addr, 4);
	//memcpy(ea->.tha, 6);
	memcpy(ea->arp_tpa, svr_ip_addr, 4);

	out->len = sizeof(struct etherhdr) + sizeof(struct ether_arp);

	eth_xmit(out);
}

/* IP zone */
static void ip_input()
{
	int hlen;

	struct iphdr *ip = (struct iphdr*) (in.buf + 14 );

	if (ip->ip_v != IPVERSION) {
		SerialOutputString("ip version not matacing\n");
		return;
	}

	hlen =  ip->ip_hl << 2 ;

	if( hlen < sizeof(struct iphdr) ) {
		SerialOutputString("hlen < iphdr\n");
		return;
	}

	if ( ip->ip_sum = in_chksum((unsigned char*)ip, hlen) ) {
		SerialOutputString("chksum error\n");
		return;
	}

	if(ntohs(ip->ip_len) < hlen) {
		SerialOutputString("ip_len < hlen: ");
		SerialOutputHex(ip->ip_len);
		SerialOutputString(" , ");
		SerialOutputDec(hlen);
		SerialOutputString("\n");
		return;
	}

	//ip->ip_id = ntohs(ip->ip_id);
	//ip->ip_off = ntohs(ip->ip_off);

	if( ntohs(ip->ip_off) & IP_OFFMASK ) {
		SerialOutputString("can't handler fragment\n");
		return;
	}

	if( memcmp(&ip->ip_dst, ip_addr, 4) && memcmp(&ip->ip_dst,broadcast_ip_addr,4) ) {
		/* packet not for us, ingore */
		//SerialOutputString("noise, not for us\n");
		return;
	}

	switch(ip->ip_p) {
	case IPPROTO_ICMP:
		//SerialOutputString("icmp\n");
		icmp_input();
		break;
	case IPPROTO_UDP:
		//SerialOutputString("udp\n");
		udp_input();
		break;
	default:
		//SerialOutputString("Unsupported IP packet\n");
		break;
	}
}

/* ICMP zone */
static void icmp_input()
{
	struct in_addr t;
	struct icmphdr *icmp;
	struct mybuf *out;

	struct etherhdr *eh = (struct etherhdr*)(in.buf);
	struct iphdr *ip = (struct iphdr *) (in.buf + sizeof(struct etherhdr));
	int hlen = ip->ip_hl << 2;
	int icmplen = ntohs(ip->ip_len) - hlen;

	if( icmplen < ICMP_MINLEN ) {
		SerialOutputString(".. < ICMP_MINLEN\n");
		return;
	}

	icmp = (struct icmphdr *) (in.buf + sizeof(struct etherhdr) + hlen);

	switch(icmp->type) {
	case ICMP_ECHO:
		//SerialOutputString("icmp echo\n");
		out = bget(1);
		if(!out) return;

		icmp->type = ICMP_ECHOREPLY;
		icmp->cksum = 0;
		icmp->cksum = in_chksum (icmp, icmplen);
		memcpy(out->buf + sizeof(struct etherhdr) + hlen,
				icmp, icmplen);

		memcpy(&t, &ip->ip_dst, sizeof(struct in_addr));
		memcpy(&ip->ip_dst, &ip->ip_src, sizeof(struct in_addr));
		memcpy(&ip->ip_src, &t, sizeof(struct in_addr));

		ip->ip_ttl = MAXTTL;
		ip->ip_hl = sizeof(struct iphdr) >> 2; /* discard the options */
		ip->ip_len = htons(icmplen + sizeof(struct iphdr) );
		ip->ip_sum = 0;
		ip->ip_sum = in_chksum(ip, sizeof(struct iphdr));
		memcpy(out->buf + sizeof(struct etherhdr), ip, sizeof(struct iphdr));

		memcpy(eh->eh_dhost, eh->eh_shost, 6);
		memcpy(eh->eh_shost, mac_addr, 6);
		memcpy(out->buf, eh, sizeof(struct etherhdr) );

		out->len = icmplen + sizeof(struct iphdr) + sizeof( struct etherhdr);
		eth_xmit(out);

		break;

	case ICMP_ECHOREPLY:
		break;

	case ICMP_UNREACH:
		break;
	}

}

/* TFTP zone */
static void udp_input()
{
	struct etherhdr *eh;
	struct iphdr *ip;
	struct udphdr *uh;


	eh = (struct etherhdr*) (in.buf);
	ip = (struct iphdr *) (eh + 1);
	uh = (struct udphdr *) ( (unsigned char*)ip + (ip->ip_hl << 2));

	/* check-sum */

	/* Not for us? */
	if( ntohs(uh->uh_dport) != our_tftp_port ) return;

	/* ok, tftp handler */
	tftp_handler();
}


#define	RRQ	01				/* read request */
#define	WRQ	02				/* write request */
#define	DATA	03				/* data packet */
#define	ACK	04				/* acknowledgement */
#define	ERROR	05				/* error code */


static void do_tftp()
{
	struct mybuf* out;
	char *p , *th ;
	char mode[]="octet";
	int pktlen = 0;
	struct etherhdr *eh;
	struct iphdr *ip;
	struct udphdr *uh;

	/* in progress... */
	if(tftp_state) return ;

	tftp_state = 1;

	out = bget(1);
	if(!out) return;

	eh = (struct etherhdr*) (out->buf);
	ip = (struct iphdr*) (eh + 1);
	uh = (struct udphdr*) (ip + 1);

	SerialOutputString("\ndownloading ");
	SerialOutputString(image_name);
	SerialOutputString(" to ");
	SerialOutputString("0x");
	SerialOutputHex(image_addr);
	SerialOutputString("\n");


	/* reset */
	last_block_num = 0;
	our_tftp_port =0;


	/* tftp header */
	p = th = (unsigned char*)(uh+ 1);
	*(unsigned short *) p = htons(RRQ);
	p += 2;

	strcpy(p, image_name);
	p += strlen(image_name) + 1;

	strcpy(p, mode);
	p += sizeof(mode);

	pktlen = p - th;

	/*
	SerialOutputString("len:");
	SerialOutputDec(pktlen);
	SerialOutputString("\n");
	*/

	/* udp header */
	our_tftp_port = 1024 + ( TimerGetTime() % 3072);
	uh->uh_dport = htons(69);
	uh->uh_sport = htons(our_tftp_port);
	uh->uh_sum = 0;
	uh->uh_len = htons(pktlen + sizeof(struct udphdr) );

	/* ip header */
	ip->ip_v = IPVERSION;
	ip->ip_tos = 0;
	ip->ip_hl = sizeof(struct iphdr) >> 2;
	ip->ip_id = htons(ip_id++);
	ip->ip_off = htons(0x4000); /* no frag */
	ip->ip_p = IPPROTO_UDP;
	ip->ip_len = htons( pktlen + sizeof(struct udphdr) + sizeof(struct iphdr));
	ip->ip_ttl = MAXTTL;
	memcpy(&ip->ip_dst, svr_ip_addr,sizeof(struct in_addr));
	memcpy(&ip->ip_src, ip_addr, sizeof(struct in_addr));
	ip->ip_sum = 0;
	ip->ip_sum = in_chksum((unsigned char*)ip, sizeof(struct iphdr));

	/* ether header */
	memcpy(eh->eh_shost, mac_addr, ETH_ALEN);
	memcpy(eh->eh_dhost, svr_mac_addr, ETH_ALEN);
	eh->eh_proto = htons(ETHPROTO_IP);

	//SerialOutputString("len:");
	//SerialOutputDec(pktlen + sizeof(struct iphdr) + sizeof(struct etherhdr));
	//SerialOutputString("\n");

	out->len = pktlen + sizeof(struct udphdr) + sizeof(struct iphdr)+ sizeof(struct etherhdr);
	/* xmit */
	eth_xmit(out);
}

static void tftp_handler()
{
	unsigned char *p, *th;
	unsigned short opcode, blocknum, t;
	int pktlen;
	struct etherhdr *eh, *ehout;
	struct iphdr *ip, *ipout;
	struct udphdr *uh, *uhout;

	struct mybuf* out;
	//  init download finish flag
	//nDownloadDone=0;
	/* Not in tftp session */
	char  command[128]; /*   for auto issue a command*/
	char * ptemp; /*   for auto ,temp */
	if(!tftp_state) return;

	out = bget(1);
	if(!out) return;

	eh = (struct etherhdr*) (in.buf);
	ip = (struct iphdr*) (eh + 1);
	uh = (struct udphdr*) ( (unsigned char*) ip + (ip->ip_hl << 2) );

	ehout = (struct etherhdr*)(out->buf);
	ipout = (struct iphdr*) ( ehout + 1);
	uhout = (struct udphdr*) (ipout + 1);

	p = th = (unsigned char*)uh + sizeof(struct udphdr);

	pktlen = ntohs(uh->uh_len) - sizeof(struct udphdr) ;


	if( pktlen < 2) return;

	opcode = ntohs(*(unsigned short*)p);
	p += 2;
	switch(opcode) {
	case RRQ:
	case WRQ:
		/* we are client ignore */
		break;
	case DATA:
		blocknum = ntohs(*(unsigned short*)p);
		p += 2;

		/*
		SerialOutputString("block:");
		SerialOutputHex(blocknum);
		SerialOutputString("\n");
		*/

		if ( last_block_num == blocknum ) {
			SerialOutputString("same block\n");
		}
		else if ( blocknum == (last_block_num + 1) ) {
			last_block_num = blocknum;
			/* store data */
			memcpy((unsigned char*)image_addr, p, pktlen - 4);
			image_addr += (pktlen - 4);
		}
		else {
			SerialOutputString("fatal error: uncontinous block no \n");
			return;
		}

		th = (unsigned char*) (uhout + 1);
		*(unsigned short*) th = htons(ACK);
		*((unsigned short*) th + 1) = htons(blocknum);

		uhout->uh_dport = uh->uh_sport;
		uhout->uh_sport = htons(our_tftp_port);
		uhout->uh_len = htons(4 + sizeof(struct udphdr));
		uhout->uh_sum = 0;

		ipout->ip_v = IPVERSION;
		ipout->ip_tos = 0;
		ipout->ip_hl = sizeof(struct iphdr) >> 2;
		ipout->ip_id = htons(ip_id++);
		ipout->ip_off = htons(0x4000); /* no frag */
		ipout->ip_p = IPPROTO_UDP;
		ipout->ip_len = htons( 4 + sizeof(struct udphdr) + sizeof(struct iphdr));
		ipout->ip_ttl = MAXTTL;
		memcpy(&ipout->ip_dst, svr_ip_addr,sizeof(struct in_addr));
		memcpy(&ipout->ip_src, ip_addr, sizeof(struct in_addr));
		ipout->ip_sum = 0;
		ipout->ip_sum = in_chksum((unsigned char*)ipout, sizeof(struct iphdr));

		memcpy(ehout->eh_shost, mac_addr, ETH_ALEN);
		memcpy(ehout->eh_dhost, svr_mac_addr, ETH_ALEN);
		ehout->eh_proto = htons(ETHPROTO_IP);

		out->len = sizeof(struct iphdr) + sizeof(struct etherhdr) + sizeof(struct udphdr) + 4;
		/* ack it */
		eth_xmit(out);
		// : added to check no done issue
		//SerialOutputDec(pktlen);
		//SerialOutputString("\n");
		if(pktlen < 512) {
			/* reset tftp state */
			tftp_state = 0;
			SerialOutputString("download done\n");
			printlcd("Download done\n");
			//  add switcher for state machine in main()
			switch(auto_state)
			{
				case AUTO_STATE_CONF :
					//  conf file ready, drive state machine in main()
					//test code

					//conf_head=0xa3140000;
					//SerialOutputString(conf_head);
					auto_state=AUTO_STATE_AUTO;
					break;
				case AUTO_STATE_AUTO :
				  //char * command;
					if(AUTO_FLAG_BUSY!=auto_busy) //check busy from main()
					  SerialOutputString("ERROR: should get busy in main()...");
					else{
					  //  now we are doing per image
					  //SerialOutputString("Should do fwrite here...");
					  //the command[] max is 128
					  ptemp=command;
					  *ptemp='\0';
					  ptemp=strcat(ptemp, "fwrite ");
					  ptemp=strcat(ptemp, DOWNLOAD_MEM_ADDR);
					  ptemp=strcat(ptemp, " ");
					  ptemp=strcat(ptemp, img_args);
					  SerialOutputString("\ntftp_handler write command=");
					  SerialOutputString(command);
					  SerialOutputString("\n");
					  printlcd("Flash to ..and length..\n");
					  printlcd(img_args);
					  printlcd("\n");
					  parse_command(command);

						//clear busy flag to get next image
						auto_busy=AUTO_FLAG_NOBUSY;
					}
					break;
			}

		}
		else {
		}
		break;
	case ERROR:
		tftp_state = 0;
		SerialOutputString("tftp error, aborted\n");

		break;
	}
}

void net_reset()
{
	out1.len = 0;
	out2.len = 0;
	in.len = 0;
}

void net_rx()
{
	struct etherhdr *eh;
	int proto;

	eh = (struct etherhdr*)in.buf;

	proto = ntohs(eh->eh_proto);

	switch(proto) {
	case ETHPROTO_IP:
		ip_input();
		break;
	case ETHPROTO_ARP:
		arp_input() ;
		break;
	default:
		SerialOutputHex(proto);
		SerialOutputString("\n");
		break;
	}

}

int cmd_tftp(int argc, int argv[])
{
	if(argc < 3) {
		SerialOutputString("usage:\n \ttftp file-name ram-addr\n");
		return 0;
	}

	strcpy(image_name, argv[1]);
	strtou32(argv[2], &image_addr);
	arp_request();
	return 0;
}

static char flashtftphelp[] = "tftp file-name ram-addr\n" ;
__commandlist( cmd_tftp, "tftp", flashtftphelp );


static int str2mac(unsigned char* str, unsigned char* mac)
{
	int i=0;
	unsigned char *p=str;
	unsigned char t[6];
	int addr;

	while(i < 6) {
		addr = 0;
		while( *p && (*p != ':') ) {
			if( (*p >= '0') && (*p <= '9') ) {
				addr = addr * 16 + (*p - '0');
				p++;
			}
			else if( (*p >= 'a') && (*p <= 'f') ) {
				addr = addr * 16 + (*p - 'a');
				p++;
			}
			else if ( (*p >= 'A') && (*p <= 'F') ) {
				addr = addr * 16 + (*p - 'A');
			       	p++;
			}
			else return -1; /* invalid */
		}

		if (addr < 255) t[i] = (addr&0xFF);
		else break;

		i++;

		if(*p) p++;
		else break;
	}

	if( i!=6 )  return -1;

	memcpy(mac, t, sizeof(t));

	return 0;
}

static int str2ip(unsigned char* str, unsigned char* ip)
{
	int i=0;
	unsigned char *p=str;
	unsigned char t[4];
	int addr;

	while(i < 4) {
		addr = 0;
		while( *p && (*p != '.') ) {
			if( (*p >= '0') && (*p <= '9') ) {
				addr = addr * 10 + (*p - '0');
			       	p++;
			}
			else return -1; /* invalid */
		}

		if (addr < 255) t[i] = (addr&0xFF);
		else break;

		i++;

		if(*p) p++;
		else break;

	}

	if( i!=4 )  return -1;

	memcpy(ip, t, sizeof(t));

	return 0;
}

static int cmd_ifconfig(int argc, char* argv[])
{
	if(argc == 1) {
		SerialOutputString("Our IP addr   : ");
		SerialOutputDec(ip_addr[0]);
		SerialOutputString(".");
		SerialOutputDec(ip_addr[1]);
		SerialOutputString(".");
		SerialOutputDec(ip_addr[2]);
		SerialOutputString(".");
		SerialOutputDec(ip_addr[3]);

		SerialOutputString("\n");

		SerialOutputString("Server IP addr: ");
		SerialOutputDec(svr_ip_addr[0]);
		SerialOutputString(".");
		SerialOutputDec(svr_ip_addr[1]);
		SerialOutputString(".");
		SerialOutputDec(svr_ip_addr[2]);
		SerialOutputString(".");
		SerialOutputDec(svr_ip_addr[3]);

		SerialOutputString("\n");

	}else if(argc < 3) {
		SerialOutputString("usage:\n \t ifconfig [ip|mac|server] addr\n");
		return 0;
	}

	if(!strncmp(argv[1], "ip", 6)) {
		return str2ip(argv[2],ip_addr);
	}

	if(!strncmp(argv[1],"mac", 6)) {
		return str2mac(argv[2],mac_addr);
	}

	if(!strncmp(argv[1],"server", 6)) {
		return str2ip(argv[2],svr_ip_addr);

	}
	return 0;

}

static char flashifconfighelp[] = "ifconfig [ip|mac|server] addr\n" ;
__commandlist( cmd_ifconfig, "ifconfig", flashifconfighelp );

