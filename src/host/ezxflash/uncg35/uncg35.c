/***************************************************************************
 *   Copyright (C) 2006 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
    
#include <stdio.h>

int main(int argc, char *argv[])
{
    if (argc!=3)
    {
	printf("USAGE: uncg35 [inputfile] [outputfile]\n");
	return 0;
    }
    FILE *fp1=fopen(argv[1],"r");
    if (!fp1)
    {
	printf("Unable to open input file\n");
	return 1;
    }
    FILE *fp2=fopen(argv[2],"w");
    if (!fp2)
    {
	printf("Unable to open output file\n");
	return 1;
    }
    
    char buf1[1024];
    char buf2[8];
    int nr,i,k;
    fread(buf1,1,56,fp1);
        
    while (1)
    {
	nr=fread(buf1,1,1024,fp1);
	if (nr!=1024)
	{
	    printf("st1(%d)\n",nr);
	    break;
	}
	fwrite(buf1,1,nr,fp2);
	nr=fread(buf2,1,8,fp1);
	if (nr!=8)
	{
	    printf("st2(%d)\n",nr);
	    break;
	}
	for (k=0;k<7;k++)
	    printf("%02x ",(unsigned char)buf2[k]);
	printf("\n");    
    }
    fclose(fp1);
    fclose(fp2);
}
