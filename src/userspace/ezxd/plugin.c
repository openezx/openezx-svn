/*
 *  plugin.c - plugin helper functions for ezxd
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>

#include "ezxd.h"

int plugin_load(char *name)
{
	int n, ret = -1;
#ifdef DEBUG
	char *error;
#endif
	char tmp[64];
	void *handle;
	struct ezxd_plugin *plugin;

	snprintf(tmp, 64, "%s/%s.so", PLUGINS_PATH, name);
	for (n=0;n<MAX_PLUGINS;n++) {
		if (plugins[n] && !strcmp(name, plugins[n]->name)) {
			error("plugin already loaded");
			goto end;
		}
	}
	for (n=0;n<MAX_PLUGINS;n++)
		if (plugins[n] == NULL)
			break;
	if (n == MAX_PLUGINS) {
		error("MAX_PLUGINS reached");
		goto end;
	}
	
	handle = dlopen(tmp, RTLD_LAZY);
	if (!handle) {
		error(dlerror());
		goto end;
	}

	sprintf(tmp, "%s_plug", name);
	plugin = dlsym(handle, tmp);
	if (plugin == NULL)
		goto dlclose;

	plugin->handle = handle;
	strncpy(plugin->name, name, 27);
	plugins[n] = plugin;

	if (plugin->start) {
		if (plugin->start() != 0) {
			error("_start() failed");
			goto err;
		}
	}
	dbg("%s loading successful\n", name);
	return 0;
err:
	plugins[n] = NULL;
dlclose:
	dlclose(handle);
end:
	dbg ("%s loading failed (%s)\n", name, error);
	return ret;
}

int plugin_unload(char *name)
{
	int n, ret = -1;
#ifdef DEBUG
	char *error;
#endif

	for (n=0;n<MAX_PLUGINS;n++)
		if (plugins[n] && strcmp(name, plugins[n]->name) == 0)
			break;

	if (n == MAX_PLUGINS) {
		error ("plugin not found");
		goto err;
	}

	if (plugins[n]->stop) {	
		if(plugins[n]->stop() != 0) {
			error("_stop failed");
			goto err;
		}
	}
	dlclose(plugins[n]->handle);
	plugins[n] = NULL;
	dbg ("%s unloading successful\n", name);
	return 0;
err:
	dbg ("%s unloading failed (%s)\n", name, error);
	return ret;
}

static int regmatch(char *regex, char *str)
{
	regex_t preg;
	int ret;

	ret = regcomp(&preg, regex, REG_EXTENDED | REG_NOSUB);
	if (ret) {
		dbg ("invalid regexp '%s'\n", regex);
		return ret;
	}
	ret = regexec(&preg, str, 0, NULL, 0);
	regfree(&preg);
	return ret;
}

int plugin_cmd(unsigned int mux, char *cmd, struct sb *client)
{
	int n, c, i = 0;
	int ret = 0;
	
	for(n=0;n<MAX_PLUGINS;n++) {
		if (plugins[n] == NULL)
			continue;
		for(c=0;c<plugins[n]->n_cmds;c++) {
			if(!mux && !plugins[n]->cmds[c].flags &&
				  !regmatch(plugins[n]->cmds[c].str, cmd)) {
				ret |= plugins[n]->cmds[c].handler(mux, cmd, client);
				i++;
			}
			if(mux && (plugins[n]->cmds[c].flags & mux) &&
				  !regmatch(plugins[n]->cmds[c].str, cmd))
				ret |= plugins[n]->cmds[c].handler(mux, cmd, client);
		}			
	}
	if (!mux && !i) ret = 0xbadc0de; //FIXME
	return ret;
}


