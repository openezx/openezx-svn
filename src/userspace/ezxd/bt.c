/*
 *  bt.c - ezxd plugin to control a780/e680 bcm2035 chip
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <string.h>
#include "ezxd.h"

#define BT_ON	0x1

extern int cmdui_write(struct sb *, char *);
extern int mux_write(unsigned int, char *);

static char ba[6];
static unsigned int f = 0;

static int cmd_bt_status(unsigned int mux, char *s, struct sb *client)
{
	char buf[32];
	if (!client) return -1;
	sprintf(buf, "+EBTP: %d\r\n", ((f & BT_ON) ? 1 : 0));
	cmdui_write(client, buf);
	cmdui_write(client, "OK\r\n");
	return 0;
}

static int cmd_bt_toggle(unsigned int mux, char *s, struct sb *client)
{
	char *arg = strstr(s, "=")+1;
	if (!strcmp(arg, "1")) {
		mux_write(MUX(5), "AT+EBTP=1\r");
		f |= BT_ON;
	}
	else {
		mux_write(MUX(5), "AT+EBTP=0\r");
		f &= ~BT_ON;
	}
	return 0;
}

static int cmd_btaddr(unsigned int mux, char *s, struct sb *client)
{
	char buf[32];
	if (!client) return -1;
	sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x\r\n", ba[0], ba[1],
						ba[2], ba[3], ba[4], ba[5]);
	cmdui_write(client, buf);
	cmdui_write(client, "OK\r\n");
	return 0;
}

static int cmd_set_ba(unsigned int mux, char *s, struct sb *client)
{
	if (sscanf(s, "+EBAD:%hhd,%hhd,%hhd,%hhd,%hhd,%hhd", &ba[0], &ba[1],
					&ba[2], &ba[3], &ba[4], &ba[5]) != 6)
		return -1;
	return 0;
}

/* Just a quick hack to hijack ATZ, this shouldnt be here. */
static int cmd_foo(unsigned int mux, char *s, struct sb *client)
{
	if (client)
		cmdui_write(client, "OK\r\n");
	return 0;
}

struct ezxd_plugin bt_plug = {
	.cmds = {
		{ "^AT\\+EBTP=[0-1]$",					0,	cmd_bt_toggle, },
		{ "^AT\\+EBTP\\?$",					0,	cmd_bt_status, },
		{ "^AT\\+EBAD\\?$",					0,	cmd_btaddr, },
		{ "EBAD:[0-9]*,[0-9]*,[0-9]*,[0-9]*,[0-9]*,[0-9]*$",	MUX(1),	cmd_set_ba, },
		{ "^ATZ$",						0,	cmd_foo, },
	},
	.n_cmds = 5,
};
