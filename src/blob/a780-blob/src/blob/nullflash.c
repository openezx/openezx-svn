/*
 * nullflash.c: NULL flash driver
 *
 * Copyright (C) 2001  Erik Mouw (J.A.K.Mouw@its.tudelft.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ident "$Id: nullflash.c,v 1.4 2001/12/28 01:06:12 erikm Exp $"

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/errno.h>
#include <blob/error.h>
#include <blob/flash.h>
#include <blob/serial.h>




static int flash_erase_null(u32 *addr)
{
#ifdef BLOB_DEBUG
	printerrprefix();
	SerialOutputString("No flash erase() function\n");
#endif
	return -EFLASHERASE;
}




static int flash_write_null(u32 *dst, const u32* src)
{
#ifdef BLOB_DEBUG
	printerrprefix();
	SerialOutputString("No flash write() function\n");
#endif
	return -EFLASHPGM;
}




static int flash_lock_block_null(u32 *blockStart)
{
#ifdef BLOB_DEBUG
	printerrprefix();
	SerialOutputString("No flash lock_block() function\n");
#endif
	return -EFLASHPGM;
}



static int flash_unlock_block_null(u32 *blockStart)
{
#ifdef BLOB_DEBUG
	printerrprefix();
	SerialOutputString("No flash unlock_block() function\n");
#endif
	return -EFLASHPGM;
}




static int flash_query_block_lock_null(u32 *blockStart)
{
#ifdef BLOB_DEBUG
	printerrprefix();
	SerialOutputString("No flash query_block_lock() function\n");
#endif
	return 0;
}




/* flash driver structure */
flash_driver_t null_flash_driver = {
	erase:			flash_erase_null,
	write:			flash_write_null,
	lock_block:		flash_lock_block_null,
	unlock_block:		flash_unlock_block_null,
	query_block_lock:	flash_query_block_lock_null
};
