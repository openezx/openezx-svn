/* ezx ipc_test - Test program for playing with EZX inter processor
 * communication
 *
 * (C) 2006 by Harald Welte <laforge@gnumonks.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>

#include <usb.h>

const char *
hexdump(const void *data, unsigned int len)
{
	static char string[65535];
	unsigned char *d = (unsigned char *) data;
	unsigned int i, left;

	string[0] = '\0';
	left = sizeof(string);
	for (i = 0; len--; i += 3) {
		if (i >= sizeof(string) -4)
			break;
		snprintf(string+i, 4, " %02x", *d++);
	}
	return string;
}

#define IPC_VENDOR_ID	0x22b8
#define IPC_PRODUCT_ID	0x3006

static struct usb_dev_handle *hdl;
static struct usb_device *find_ezx_device(void)
{
	struct usb_bus *bus;

	for (bus = usb_busses; bus; bus = bus->next) {
		struct usb_device *dev;
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == IPC_VENDOR_ID
			    && dev->descriptor.idProduct == IPC_PRODUCT_ID
			    //&& dev->descriptor.iManufacturer == 1
			    //&& dev->descriptor.iProduct == 2
			    && dev->descriptor.bNumConfigurations == 1
			    && dev->config->bNumInterfaces == 3)
				return dev;
		}
	}
	return NULL;
}

#if 0
static int ezx_blob_recv_reply(void)
{
	char buf[8192];
	int ret, i;

	memset(buf, 0, sizeof(buf));

	ret = usb_bulk_read(hdl, EZX_IN_EP, buf, sizeof(buf), 0);

	for (i = 0; i < ret; i ++)
		if (buf[i] == 0x03)
			buf[i] = 0x00;

	printf("RX: %s (%s)\n", buf, hexdump(buf, ret));

	return ret;
}


static int ezx_blob_send_command(char *command, char *payload, int len)
{
	char buf[8192];
	int cmdlen = strlen(command);
	int cur = 0;
	int ret;

	memset(buf, 0, sizeof(buf));

	buf[cur++] = STX;
	memcpy(&buf[cur], command, cmdlen);
	cur += cmdlen;

	if (payload) {
		buf[cur++] = RS;
		memcpy(&buf[cur], payload, len);
		cur += len;
	}
	buf[cur++] = ETX;
	//buf[cur++] = NUL;

	if (!strcasecmp(command, "bin"))
		printf("TX: %u bytes\n", cur);
	else
		printf("TX: %s (%s)\n", buf,  hexdump(buf, cur));

	ret = usb_bulk_write(hdl, EZX_OUT_EP, buf, cur, 0);
	if (ret < 0)
		return ret;

	/* this usleep is required in order to make the process work.
	 * apparently some race condition in the bootloader if we feed
	 * data too fast */
	usleep(5000);

	return ezx_blob_recv_reply();
}
#endif

struct ipc_usb_data
{
	u_int8_t		write_finished_flag;
	struct usb_device	dev;
	char *obuf, *ibuf;
	int writesize;

	u_int8_t bulk_in_ep;
	u_int8_t bulk_out_ep;

	u_int8_t bulk_in_ep_dsplog;
};

static struct ipc_usb_data bvd_ipc;

int main(int argc, char **argv)
{
	struct usb_device *dev;
	int fd;

	usb_init();
	if (!usb_find_busses())
		exit(1);
	if (!usb_find_devices())
		exit(1);

	dev = find_ezx_device();
	if (!dev) {
		printf("Cannot find IPC device\n");
		exit(1);
	}

	/* FIXME: iterate over endpoints and save readsize/writesize */
	bvd_ipc.bulk_in_ep = ;
	bvd_ipc.bulk_out_ep = ;

	/* FIXME: allocate ibuf/obuf */

	bvd_ipc.write_finished_flag = 1;
	bvd_ipc.suspend_flag = 1;



	hdl = usb_open(dev);
	if (!hdl) {
		printf("Unable to open usb device: %s\n", usb_strerror());
		exit(1);
	}

	if (usb_claim_interface(hdl, 0) < 0) {
		printf("Unable to claim usb interface 0 of device: %s\n", 
			usb_strerror());
		exit(1);
	}

	if (usb_claim_interface(hdl, 1) < 0) {
		printf("Unable to claim usb interface 1 of device: %s\n", 
			usb_strerror());
		exit(1);
	}
	/* interface 2 seems to be DSPLOG */

	exit(0);
}
