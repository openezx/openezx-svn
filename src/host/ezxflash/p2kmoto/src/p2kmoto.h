/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*! \file p2kmoto.h
    \brief A p2kmoto library header file.
	
    P2kmoto is a library for accessing filesystem of motorola p2k phones like C350, C650	
*/

/*! \mainpage p2kmoto
 *
 * \section intro_sec Introduction
 *
 * p2kmoto is shared lib to access filesystem and SEEM records in p2kphone.
 *
 * \section patforms_sec Supported platforms
 *
 * Currently supported are: Linux, Windows (using MinGW & MSYS)
 *
 */



#ifndef P2K_MOTO_H
#define P2K_MOTO_H

// Error codes
#define P2K_E_NOPHONE -1
#define P2K_E_CANT_OPEN -2
#define P2K_E_CANT_SETCONFIG -3
#define P2K_E_CANT_CLAIMIFACE -4
#define P2K_E_CANT_CLOSE -5
#define P2K_E_NOT_CONNECTED -6
#define P2K_E_ANSWER_E001 -7
#define P2K_E_ANSWER_E002 -8
#define P2K_E_CPH_00 -9
#define P2K_E_CPH_01 -10
#define P2K_E_CPH_02a -11
#define P2K_E_CPH_02b -12
#define P2K_E_CPH_03 -13
#define P2K_E_OUTDATA -14
#define P2K_E_INPSIZE -15
#define P2K_E_INPDATA -16
#define P2K_E_CPH -17
#define P2K_E_INCORRECT_SEEMSIZE -18
#define P2K_E_BUFFER_TOOLONG -19

#define P2K_E_BUG -100
#define P2K_E_OLD -200

// Phone state
/**
  @brief Phone is not connected
 */
#define P2K_PHONE_NONE 0
/**
  @brief Phone is connected as CDC ACM device (AT Mode)
 */
#define P2K_PHONE_AT 1
/**
  @brief Phone is connected as P2K device (P2K Mode)
 */
#define P2K_PHONE_P2K 2

/**
  @brief Phone is connecten in FLASH mode
 */
#define P2K_PHONE_FLASH 3

/**
  @brief Seek from Begin of file
 */
#define P2K_SEEK_BEGIN 0

/**
  @brief Seek from current pos
 */
#define P2K_SEEK_CURRENT 1

/**
  @brief Seek from enf of file
 */
#define P2K_SEEK_END 2

/** 
 *  @brief Contains information about one USB device
 *  @param vendor Device vendor ID
 *  @param product Device product ID
 *  @param manufacturerStr[256] Device manufacturer name
 *  @param productStr[256] Product name
 *
 *  This information can be retrived with p2k_getDevList()
 */
struct p2k_devInfo
{
	int vendor;
	int product;
	char manufacturerStr[256];
	char productStr[256];
};

/** 
 *  @brief Contains information about one file in phone
 *  @param id File number. Reserved for future purposes
 *  @param name[256] File name
 *  @param size File size in bytes
 *  @param owner File owner
 *  @param attr File attributes
 *
 *  This information can be retrived with p2k_fileList()
 *  @warning For phones with folders \a name[256] contains full path to file. 
 *  For phones without folders \a name[256] contains only file name
 */
struct p2k_fileInfo
{
	long id; //File number
	char name[256]; //File name. For phones with directories is like
	long size; //File size
	unsigned char owner; //File owner
	unsigned char attr;  //File attributes
};

struct p2k_devList
{
	unsigned int vendor;	// Vendor ID
	unsigned int product;	// Product ID
	char name[150];			// Name
};


/** 
 *   @brief typedef for function which will be called for each file in phone
 */
typedef void (*p2k_onFile) (struct p2k_fileInfo file);

extern struct p2k_devList p2k_dev[];
extern char p2k_debug;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialize library
 * 
 * This function initialize p2kmoto library.
 * @warning Must be called before call to any other function.
 */
void p2k_init();

/**
 * @brief Set ACM device port
 * @param dev ACM device port
 * 
 * This function set's ACM device port. For example /dev/ttyACM0 on Unix and 
 * COM3 for Windows. You must call this if you want to switch phone to P2K using
 * p2k_setP2Kmode()
 */
void p2k_setACMdevice(const char * dev);

/**
 * @brief Set AT device information
 * @param vendor vendor ID
 * @param product product ID
 * 
 * Set AT device information. 
 * \a vendor and \a product can be get with p2k_getDevList()
 */
void p2k_setATconfig (unsigned int vendor, unsigned int product);

/**
 * @brief Set P2K device information
 * @param vendor vendor ID
 * @param product product ID
 * 
 * Set P2K device information. 
 * \a vendor and \a product can be get with p2k_getDevList()
 */
void p2k_setP2Kconfig (unsigned int vendor, unsigned int product);

/**
 * @brief Get current AT vendor ID
 */
int p2k_getATvendor();

/**
 * @brief Get current AT product ID
 */
int p2k_getATproduct();

/**
 * @brief Get current P2K vendor ID
 */
int p2k_getP2Kvendor();

/**
 * @brief Get current P2K product ID
 */
int p2k_getP2Kproduct();

void p2k_getACMdevice(char *dev);

/**
 * @brief Try to automaticaly detect AT & P2K vendor/product ID
 * 
 * If phone detected, p2k_setATconfig() and p2k_setP2Kconfig() will be called autmaticaly.
 * If phone is not detected, it's possible to set it manually with p2k_setATconfig() and p2k_setP2Kconfig().
 *
 * @return 0 if success.
 */
int p2k_detectPhone();

/**
 * @brief Get list of connected USB devices
 *
 * This function can get list of all USB devices, connected to machine.
 * It is like lsusb unix command
 *
 * @return Pointer to array p2k_devInfo structure. After last useful element
 * there will be element with \a vendor < 0 and \a product < 0
 */
struct p2k_devInfo* p2k_getDevList();

/**
 * @brief Detect current phone state
 *
 * Call this to detect current phone state
 * AT and P2K Vendor/Product ID must be set correctly before calling this. 
 * You can use p2k_setATconfig() and p2k_setP2Kconfig() to do this.
 * @return Current phone state. One of 
 * #P2K_PHONE_NONE, #P2K_PHONE_AT, #P2K_PHONE_P2K
 */
int p2k_findPhone();

/**
 * @brief Switch phone to P2K mode
 *
 * Call this to switch phone to P2K mode
 * AT and P2K Vendor/Product ID, ACM device must be set correctly before calling this. 
 * You can use p2k_setACMdevice(), p2k_setATconfig() and p2k_setP2Kconfig() to do this.
 * @return 0 if success
 *
 */
int p2k_setP2Kmode(int timeout);

/**
 * @brief Open phone
 *
 * Estabilish connection to phone.
 * @param timeout Connect timeout.
 *
 * \a timeout value is useful for systems where hotplug change permissions to phone. There will be at least
 * one attemption to connect.
 *
 * @warning Phone must be in P2K mode before calling this. You can check phone state with p2k_findPhone().
 * Also P2K vendor/product ID must be set correctly. Use p2k_setP2Kconfig() to do this
 * 
 * @return 0 if connection is estabilished or < 0 if there is error.
 */
int p2k_openPhone(int timeout);

/**
 * @brief Close connection with P2K phone
 * 
 * Connection must be estabilished by p2k_openPhone() before.
 * @return 0 if connection is closed or < 0 if there is error.
 */
int p2k_closePhone();

/**
 * @brief Reboot phone
 * 
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @warning Connection with phone will be closed. After reboot phone will be in AT Mode.
 * 
 * @return 0 if connection is closed or < 0 if there is error. 
 */
int p2k_reboot();

/**
 * @brief Suspend phone
 * 
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return Returns 0 if success.
 */
int p2k_suspend();

//TODO: Remove obsolete function
/**
 * @brief Get all drive names
 * @param  buf Buffer, where drive names will be placed
 *  
 * Connection must be estabilished by p2k_openPhone() before.
 * @warning For phones without folders will return FLASH. 
 * For phones with more than one drive  (like E398, E1000) will return all drives
 * separated by space (for Example "/a /b")
 *
 * @return 0 if success.
 */
int p2k_getDriveNames(unsigned char * buf);

/**
 * @brief Get phone model
 * @param  buf Buffer, where phone model will be placed
 *  
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return 0 if success.
 */
int p2k_getPhoneModel(unsigned char * buf);

//TODO: Remove obsolete function
/**
 * @brief Get free space on phone drive
 *  
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return Returns >=0 if success (This will be free space).
 */
int p2k_freeSpace();

//TODO: Remove obsolete function
/**
 * @brief Get file count
 * 
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return Returns >=0 if success (This will be file count).
 */
int p2k_fileCount();


/**
 * @brief Read SEEM record from phone
 * @param x SEEM number
 * @param y SEEM page
 * @param seemBuf Buffer where SEEM record will be placed
 * @param seemBufSize Size of \a seemBuf.
 *
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return Returns SEEM size if success or < 0 if error
 */
int p2k_readSeem(int x, int y, unsigned char * seemBuf, int seemBufSize);

/**
 * @brief Write SEEM record to phone
 * @param x SEEM number
 * @param y SEEM page
 * @param seemBuf Buffer where SEEM record will be placed
 * @param seemSize Size of seem record
 *
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return 0 if success.
 */
int p2k_writeSeem(int x, int y, unsigned char * seemBuf, int seemSize);

// FSAC functions

/**
 * @brief Get FSAC version
 * @param buf Buffer to store version
 * 
 * This function get FSAC version, supported by phone. For example "02.00"
 * 
 * @return 0 if success
 */
int p2k_FSAC_getVersion(char *buf);
	
/**
 * @brief Get phone volume names
 * @param  buf Buffer, where volume names will be placed
 *  
 * Connection must be estabilished by p2k_openPhone() before.
 * @warning For phones without folders will return FLASH. 
 * For phones with more than one drive  (like E398, E1000) will return all drives
 * separated by space (for Example "/a /b")
 *
 * @return 0 if success.
 */	
int p2k_FSAC_getVolumes(char *buf);
	
/**
 * @brief Get Free space for volume
 * @param volume volume name. For examle "/a"
 * 
 * This function returns free space on volume in bytes
 * 
 * @return free space. <0 if there is error
 */
int p2k_FSAC_getVolumeFreeSpace(char *volume);
	
/**
 * @brief Perform file search request
 * @param request search request
 * 
 * This function perform file serach request. It's possible to get
 * search result using p2k_FSAC_fileList()
 * 
 * \a request can be used to specify volume name to search and file mask. A recursive search will be done on the volume
 * and filename selected, therefore all sub-directories for the given volume will be searched. Here are some examples:
 * 
 * *.wav - all wav files on first volume (/a)
 * 
 * /b/*.mp3 - all mp3 files on volume /b
 *
 * @return count of founded files
 */
int p2k_FSAC_searchRequest(char *request);
	
/**
 * @brief Get file list
 * @param onGetFile procedure which will be called for each file.
 *
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @warning You must call p2k_fileCount() before
 *
 * @return 0 if success.
 */
int p2k_FSAC_fileList(p2k_onFile onGetFile);
		
/**
 * @brief Delete file from Phone
 * @param fname File name to delete
 *
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return 0 if success.
 */
int p2k_FSAC_delete(char * fname);

/**
 * @brief Open file for R/W in phone
 * @param fname file name to open
 * @param attr file attributes
 *
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * This function try to open file in P2K phone. File attributes will
 * be changed to \a attr. Only one file can be opened at time.
 * Close opened file using p2k_FSAC_close() before open new one.
 *
 * @return 0 if success.
 */
int p2k_FSAC_open(char * fname, unsigned char attr);

/**
 * @brief Close opened file
 *
 * Connection must be estabilished by p2k_openPhone() before.
 *
 * @return 0 if success.
 */
int p2k_FSAC_close();

/**
 * @brief Seek in file
 * @param offset Offset to seek
 * @param dir Direction
 *
 * Connection must be estabilished by p2k_openPhone() before.
 * There is 3 possible value for \a dir: 
 * #P2K_SEEK_BEGIN (Seek from file begin);
 * #P2K_SEEK_CURRENT (Seek from current pos in file);
 * #P2K_SEEK_END (Seek from end of file).
 *
 * @return 0 if success.
 */
int p2k_FSAC_seek(unsigned long offset, char dir);

/**
 * @brief Read from file in phone
 * @param buf Buffer, where to read
 * @param size Size to read (in bytes)
 *
 * Reading is from current position in file. It's possible to set 
 * current position using p2k_FSAC_seek(). After reading current position
 * will be automaticaly increased, it's possible to read all file calling only
 * p2k_FSAC_read() without seeking in file.
 *
 * @warning size must be <= 0x400 bytes
 *
 * @return 0 if success.
 */
int p2k_FSAC_read(unsigned char * buf, int size);

/**
 * @brief Write to file in phone
 * @param tbuf Buffer, which contains data to write
 * @param size size of buffer (in bytes)
 *
 * Writing is from current position in file. It's possible to set 
 * current position using p2k_FSAC_seek(). After writing current position
 * will be automaticaly increased, it's possible to write all file calling only
 * p2k_FSAC_write() without seeking in file.
 *
 * @warning size must be <= 0x400 bytes
 *
 * @return 0 if success.
 */
int p2k_FSAC_write(unsigned char * tbuf, int size);

/**
 * @brief Create directory in phone filesystem
 * @param dirname Directory name to create
 *
 * This will create empty directory in phone filesystem.
 *
 * @return 0 if success.
 */
int p2k_FSAC_createDir(char * dirname);

/**
 * @brief Removed directory from phone filesystem
 * @param dirname Directory name to remove
 *
 * This will remove empty directory from phone filesystem.
 *
 * @return 0 if success.
 */
int p2k_FSAC_removeDir(char * dirname);

int p2k_newDevices();

// FLASH MODE IO

struct flash_device
{
	int vendor;
	int product;
	int interface;
	int epIn;
	int epOut;
	char productStr[1024];
	char interfaceStr[1024];
};

int flash_getDeviceList(struct flash_device* arr, int cnt);

void flash_setDevice(const struct flash_device);

int flash_open();

int flash_replug();

void flash_close();

int flash_write(char *buf, int cnt, int timeout);

int flash_writeCommand(char *cmd, char *par, int parsize, int timeout);

int flash_read(char *buf, int cnt, int timeout);

int flash_readCommand(char * par, int timeout);


#ifdef __cplusplus
}
#endif

#endif

