/*
 * usb-mot.c
 *
 * Motorola USB driver
 *
 * Copyright (C) 2002 Motorola
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Oct 30,2002 - (Motorola) Created
 *
 */

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/arch.h>
#include <blob/types.h>
#include <blob/serial.h>
#include <blob/util.h>

#include <pxa_usb.h>


//#define VENDOR_ID 0x49f
//#define PRODUCT_ID 0x505A
#define VENDOR_ID 0x22B8
#define PRODUCT_ID 0x6003

/*
 * static allocated, make sure it's enough!
 */
static char sd_buf[2048];

static char ep2_rx_buf[128];
static char ep7_rx_buf[128];

static int	usb_rsize=64;
static int 	usb_wsize=64;

static char *ezx_str[] = {
	"Motorola Inc.",
	"Blank Ezx",
	"Flashed Ezx",
	"Motorola Ezx Bootloader",
	"Motorola Ezx Flash",
	"Motorola Ezx AP Flash"
	"Motorola Flash",
	"NULL"
};

static void ezx_usb_open(int flag);
static int flashed_ep1_tx_callback(int flag, int size);
static int flashed_ep2_rx_callback(int flag, int size);
static int flashed_ep6_tx_callback(int flag, int size);
static int flashed_ep7_rx_callback(int flag, int size);
static int ap_rx_callback(int flag, int size);
static int ap_tx_callback(int flag, int size);
static int ezx_usb_naked_start();
static int ezx_usb_flashed_start();


/* send data with EP1 */
int ezx_usb_ep1_xmit(char *buf, int len)
{

}

/* send data with EP6 */
int ezx_usb_ep6_xmit(char *buf, int len)
{
}


void usb_mot_open(int flag)
{
	desc_t * pdesc = pxa_usb_get_descriptor_ptr();
	char *str=ezx_str[0], *p;
	string_desc_t *sd = (string_desc_t *) sd_buf;
	int i=0, index = 1 ;

	/* setup string descriptor, language identifier is already setten */
	while( str ) {
		sd->bDescriptorType = USB_DESC_STRING;
		sd->bLength = 2 * strlen(str) + 2;

		i=0;
		while( '\0' != str[i] ) {
			sd->bString[i] = make_word_c( str[i] );
			i++;
		}

		pxa_usb_set_string_descriptor(index, sd);

		/* next... */
		sd = (string_desc_t *) ( (unsigned char*) sd + sd->bLength);
		str = ezx_str[index];
		index ++;
	}

	/* setup device descriptor */

	pdesc->dev.idVendor	= VENDOR_ID;
	pdesc->dev.idProduct    = PRODUCT_ID;
	pdesc->dev.bNumConfigurations = 1;

	switch(flag) {
	case 0x1:
		pdesc->dev.iManufacturer = 0x1; /* Motorola Inc. */
		pdesc->dev.iProduct = 0x2; /* Blank Ezx*/
		ezx_usb_open(flag);
		ezx_usb_naked_start();
		break;

	case 0x2:
		pdesc->dev.iManufacturer = 0x1; /* Motorola Inc. */
		pdesc->dev.iProduct = 0x3; /* Flashed Ezx*/
		ezx_usb_open(flag);
		ezx_usb_flashed_start();
		break;
	}

}

static void ezx_usb_open(int flag)
{
	desc_t * pdesc = pxa_usb_get_descriptor_ptr();
	config_desc_t *cfg;
	intf_desc_t *intf;
	ep_desc_t *ep;


	cfg = (config_desc_t*) (pdesc->cdb);

	cfg->bLength             = sizeof( config_desc_t );
	cfg->bDescriptorType     = USB_DESC_CONFIG;
	cfg->wTotalLength        = make_word_c( sizeof(config_desc_t) +
						   sizeof(intf_desc_t) * 2+
						   sizeof(ep_desc_t) * 4);
	cfg->bNumInterfaces      = 2;
	cfg->bConfigurationValue = 1;
	cfg->iConfiguration      = 0x5;
	cfg->bmAttributes        = USB_CONFIG_SELFPOWERED;
	cfg->MaxPower            = USB_POWER( 10 );

	intf = (intf_desc_t *) ( cfg + 1);
	intf->bLength            = sizeof( intf_desc_t );
	intf->bDescriptorType    = USB_DESC_INTERFACE;
	intf->bInterfaceNumber   = 0;
	intf->bAlternateSetting  = 0;
	intf->bNumEndpoints      = 2;
	intf->bInterfaceClass    = 0xFF;
	intf->bInterfaceSubClass = 0x32;
	intf->bInterfaceProtocol = 0xFF;
	intf->iInterface         = 0x6;

	ep = (ep_desc_t *) (intf + 1);
	ep[0].bLength             = sizeof( ep_desc_t );
	ep[0].bDescriptorType     = USB_DESC_ENDPOINT;
	ep[0].bEndpointAddress    = USB_EP_ADDRESS( 1, USB_IN );
	ep[0].bmAttributes        = USB_EP_BULK;
	ep[0].wMaxPacketSize      = make_word( 64 );
	ep[0].bInterval           = 0;

	ep[1].bLength             = sizeof( ep_desc_t );
	ep[1].bDescriptorType     = USB_DESC_ENDPOINT;
	ep[1].bEndpointAddress    = USB_EP_ADDRESS( 2, USB_OUT );
	ep[1].bmAttributes        = USB_EP_BULK;
	ep[1].wMaxPacketSize      = make_word( 64 );
	ep[1].bInterval           = 0;

	intf = (intf_desc_t*) (ep + 2);
	intf->bLength            = sizeof( intf_desc_t );
	intf->bDescriptorType    = USB_DESC_INTERFACE;
	intf->bInterfaceNumber   = 1;
	intf->bAlternateSetting  = 0;
	intf->bNumEndpoints      = 2;
	intf->bInterfaceClass    = 0xFF;
	intf->bInterfaceSubClass = 0x31;
	intf->bInterfaceProtocol = 0xFF;
	intf->iInterface         = 0x7;

	ep = (ep_desc_t *) (intf + 1);
	ep[0].bLength             = sizeof( ep_desc_t );
	ep[0].bDescriptorType     = USB_DESC_ENDPOINT;
	ep[0].bEndpointAddress    = USB_EP_ADDRESS( 6, USB_IN );
	ep[0].bmAttributes        = USB_EP_BULK;
	ep[0].wMaxPacketSize      = make_word( 64 );
	ep[0].bInterval           = 0;

	ep[1].bLength             = sizeof( ep_desc_t );
	ep[1].bDescriptorType     = USB_DESC_ENDPOINT;
	ep[1].bEndpointAddress    = USB_EP_ADDRESS( 7, USB_OUT );
	ep[1].bmAttributes        = USB_EP_BULK;
	ep[1].wMaxPacketSize      = make_word( 64 );
	ep[1].bInterval           = 0;
}


static int naked_ep1_tx_callback(int flag, int size)
{

}

static int naked_ep2_rx_callback(int flag, int size)
{

}

static int naked_ep6_tx_callback(int flag, int size)
{

}

static int naked_ep7_rx_callback(int flag, int size)
{

}

static int flashed_ep1_tx_callback(int flag, int size)
{

}

static int flashed_ep2_rx_callback(int flag, int size)
{

}

static int flashed_ep6_tx_callback(int flag, int size)
{

}

static int flashed_ep7_rx_callback(int flag, int size)
{

}

static int ezx_usb_naked_start()
{
	/* hook callback */
	ep2_recv(ep2_rx_buf, usb_rsize, naked_ep2_rx_callback);
	ep7_recv(ep7_rx_buf, usb_rsize, naked_ep7_rx_callback);
	return 0;
}

static int ezx_usb_flashed_start()
{
	/* hook callback */
	ep2_recv(ep2_rx_buf, usb_rsize, flashed_ep2_rx_callback);
	ep7_recv(ep7_rx_buf, usb_rsize, flashed_ep7_rx_callback);
	return 0;
}
