/*
 *  clock.c - ezxd plugin to set the system clock from BP clock
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <sys/time.h>
#define __USE_XOPEN /* for strptime() */
#include <time.h>
#include "ezxd.h"

static int cmd_clk(unsigned int mux, char *s, struct sb *client)
{
	time_t now = 0;
	struct tm *t = localtime(&now); /* act like tzset() */
	struct timeval tv;

	if (strptime(s, "+CLK: \"%y/%m/%d,%H:%M:%S\"", t) == NULL)
		return -1;

	dbg("UTC: %s", asctime(t));

	tv.tv_sec = mktime(t);
	if (tv.tv_sec < 0)
		return -1;

	tv.tv_usec = 0;

	dbg("localtime: %s", ctime(&tv.tv_sec));
	return settimeofday(&tv, NULL);
}

struct ezxd_plugin clock_plug = {
	.cmds = {
		{ "CLK: \"[0-9]*/[0-9]*/[0-9]*,[0-9]*:[0-9]*:[0-9]*\"", MUX(5),
			cmd_clk, },
	},
	.n_cmds = 1,
};
/*
int main()
{
	cmd_clk(0, "+CLK: \"07/12/25,00:00:00\"", NULL);
	return 0;
}
*/
