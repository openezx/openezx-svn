/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dionua@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/** \example p2ktest.c
 * This is an example of how to use the p2kmoto library.
 */

#include "../src/p2kmoto.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct p2k_fileInfo tstFile;


void onFile(struct p2k_fileInfo nfo)
{
	printf("%4d %8ld %3d %3d %s\n",nfo.id, nfo.size, nfo.owner, nfo.attr, nfo.name );
	if (!strcmp(nfo.name, "/a/ss_moto_a_128x128_c~~.gif"))
		memcpy(&tstFile, &nfo, sizeof(nfo));
}

char s[1024];
int ps;

int getArg(char *ss)
{
	int pps=0;
	while (ps<strlen(s))
	{
		if (s[ps]!=' ') ss[pps++]=s[ps]; else break;
		ps++;
	}
	ps++;
	ss[pps]=0;
	return (pps);	
}

void doHelp()
{
	printf("Supported commands:\n"
	       "ls [request] - get file list\n");
}

void doLs()
{
	char request[256];
	if (!getArg(request)) strcpy(request,"*");
	p2k_FSAC_searchRequest(request);
	p2k_FSAC_fileList(onFile);
}

void dialog(void)
{
	char c[256];
	printf("Enter \"help\" to see help\n");
	while (1)
	{
		printf("> ");
		gets(s); ps=0;
		if (!getArg(c)) continue;
		if (!strcmp(c,"help")) doHelp();
		if (!strcmp(c,"ls")) doLs();
		if (!strcmp(c,"exit")) break;
	}
}

int main(int argc, char *argv[])
{
	p2k_init();
	struct flash_device devs[10];
	
	int c1=flash_getDeviceList(devs,10);
	if (!c1)
	{
		printf("No devices\n");
		return 1;
	}
		
	flash_setDevice(devs[0]);
	flash_open();
	flash_replug();
	flash_close();
	
	if (flash_open())
	{
		printf("Unable to open flash device\n");
		return 0;
	}
		
	char *s1="RQHW";
	char buf[1024];
	
	flash_writeCommand(s1,0,0,10000);
	flash_readCommand(buf,10000);
		
	return 0;
	
	if (argc!=2)
	{
		printf("Using /dev/ttyACM0 as ACM device\n");
		p2k_setACMdevice("/dev/ttyACM0");
		//printf("Usage: p2ktest DEVICE \n");
		// 	    printf("  DEVICE - ACM device like /dev/ttyACM0\n");
		// 	    printf(" \n");
		// 	    return;
	}
	else
		p2k_setACMdevice(argv[1]);

	printf("P2k Test program\n\n");
// 	p2k_setACMdevice(argv[1]);

	// 	p2k_setATconfig(0x22b8, 0x4902); //For 3G Phones
	// 	p2k_setP2Kconfig(0x22b8, 0x4901);

	printf("Device list:\n");
	struct p2k_devInfo * nfo;
	int i=0;

	nfo=p2k_getDevList();

	while (nfo[i].vendor>=0)
	{
		printf("%04x:%04x: [%s] [%s]\n", nfo[i].vendor, nfo[i].product,
		       nfo[i].manufacturerStr, nfo[i].productStr);
		i++;
	}

	free(nfo);

	printf("-------\n");
	if (p2k_detectPhone()<0)
	{
		printf("No P2K phone found\n\n");
		return;
	}

	printf("Found P2K phone:");
	printf(" ACM device : %s\n",argv[1]);
	printf(" AT device  : %04x:%04x\n", p2k_getATvendor(), p2k_getATproduct());
	printf(" P2K device : %04x:%04x\n", p2k_getP2Kvendor(), p2k_getP2Kproduct());

	int state=p2k_findPhone();

	if (state==P2K_PHONE_AT)
	{
		//We must switch phone to p2k mode by sending AT+MODE=8 command.
		printf("Switching to P2K...\n");

		if (p2k_setP2Kmode(10)<=0)
		{
			printf("Unable to switch mode\n\n");
			return;
		}
		state=p2k_findPhone();
	}

	if (state==P2K_PHONE_NONE)
	{
		printf("No phone found.\n");
		return;
	}


	printf("P2k Phone found....\n\n");
	p2k_openPhone(3000);

	long sz;
	char s[200];
	if (!p2k_getPhoneModel((unsigned char *)s))
		printf("Phone Model: %s\n", s);
	else
		printf("Can not get phone model\n");
	
	if (!p2k_FSAC_getVersion(s))
		printf("FSAC version: %s\n",s);
	else
		printf("Can not get FSAC version\n");
	
	if (!p2k_FSAC_getVolumes(s))
		printf("Drives: %s\n", s);
	else
		printf("Can not get drive name\n");

	dialog();
	p2k_closePhone();
}
