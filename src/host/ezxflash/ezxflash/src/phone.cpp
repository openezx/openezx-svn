/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "phone.h"
#include <p2kmoto.h>

#define ACT_UPDATELIST 1
#define ACT_SENDCOMMAND 2
#define ACT_READFAST 3
#define ACT_SENDFILE 4
#define ACT_SENDLOADER 5
#define ACT_JUMP 6
#define ACT_ERASE 7
#define ACT_FULLFLASH 8

Phone::Phone(QObject *parent) : QThread(parent)
{
	p2k_init();
	dev=0;
	devCnt=0;
	wasOpened=0;
	action=0;
	actEnabled=1;
}

Phone::~Phone()
{}

void Phone::run()
{
#define DELAY 50
	#define NUPD 10
	emit onMessage("Waiting for FLASH mode phone");

	int cnt=0;
	int act;

	while (1)
	{
		act=getAction();
		if (actEnabled)
		{
			if (act==0)
			{
				msleep(DELAY);
				if (++cnt==NUPD)
				{
					cnt=0;
					if (!setAction(ACT_UPDATELIST,1)) continue;
					actEnabled=1;
					doUpdateList();
					clearAction();
				}

				if(wasOpened)
				{
					if (!setAction(ACT_READFAST,1)) continue;
					actEnabled=1;
					doRead();
					clearAction();
				}
				continue;
			}

			if (act==ACT_FULLFLASH)
			{
				doFullFlash(s1);
				clearAction();
				continue;
			}

			if (!wasOpened)
			{
				clearAction();
				continue;
			}

			switch (act)
			{
					case ACT_SENDCOMMAND:
					{
						doSendCommand(s1,s2);
						clearAction();
						break;
					}
					case ACT_SENDFILE:
					{
						doSendFile(addr1,s1);
						clearAction();
						break;
					}
					case ACT_JUMP:
					{
						doJump(addr1);
						clearAction();
						break;
					}

					case ACT_SENDLOADER:
					{
						doSendLoader(addr1,addr2,s1);
						clearAction();
						break;
					}
					case ACT_ERASE:
					{
						doErase();
						clearAction();
						break;
					}
			}
		}
	}
}

int Phone::setAction(int act, int q)
{
	QMutexLocker locker(&mutex);
	if (!actEnabled) return 0;
	if (action)
	{
		if (!q)
			emit onMessage("Phone is busy");
		return 0;
	}
	actEnabled=0;
	action=act;
	return 1;
}

void Phone::clearAction()
{
	QMutexLocker locker(&mutex);
	if (!actEnabled) return;
	action=0;
}

int Phone::getAction()
{
	QMutexLocker locker(&mutex);
	return action;
}

int Phone::doUpdateList()
{
	// 	qDebug() << "phone";
	static char hasList=0;
	int news=p2k_newDevices();

	if (news || !hasList)
	{
		hasList=1;
		struct flash_device lst[10];
		int cnt=flash_getDeviceList(lst,10);
		if (dev)
		{
			free(dev);
			dev=0;
		}
		devCnt=cnt;
		dev = (struct flash_device *) malloc (sizeof(struct flash_device)*cnt);
		memcpy(dev,lst,sizeof(struct flash_device)*cnt);
		emit onDeviceList(dev,devCnt);
	}
	return news;
	// 	emit onDeviceList(0,0);
}

void Phone::setDevice(int i)
{
	// 	qDebug() << "SetDevice";
	if (i<0)
	{
		emit onMessage("No device used");
		wasOpened=0;
		return;
	}
	QString st=QString("%1:%2").arg(
	               QString::number(dev[i].vendor,16).rightJustified(4,QChar('0')),
	               QString::number(dev[i].product,16).rightJustified(4,QChar('0')));

	// 	st.append(QString(" [%1 :: %2]").arg(dev[i].productStr, dev[i].interfaceStr));
	st+=QString(". Interface=%1, endpoints=%2, %3").arg(
	        QString::number(dev[i].interface,16).rightJustified(2,QChar('0')),
	        QString::number(dev[i].epOut,16).rightJustified(2,QChar('0')),
	        QString::number(dev[i].epIn,16).rightJustified(2,QChar('0')));
	emit onMessage("Using device: "+st);
	// 	sleep(10);
	doPlug(i);
}

void Phone::sendCmd(const QString &cmd, const QString&arg)
{
	int ac;
	do
	{
		ac=getAction();
	}
	while (ac>0 && ac<=3);

	if (!setAction(ACT_SENDCOMMAND)) return;
	// 	msleep(500);
	s1=cmd;
	s2=arg;
	actEnabled=1;
}

void Phone::sendFile(const QString&addr, const QString&fn)
{
	int ac;
	do
	{
		ac=getAction();
	}
	while (ac>0 && ac<=3);
	if (!setAction(ACT_SENDFILE)) return;
	addr1=addr.toUInt(0,16);
	s1=fn;
	totalSize=0;
	totalPos=0;
	actEnabled=1;
}

void Phone::sendLoader(const QString&addrFile, const QString&addrJump, const QString&fn)
{
	int ac;
	do
	{
		ac=getAction();
	}
	while (ac>0 && ac<=3);
	if (!setAction(ACT_SENDLOADER)) return;
	addr1=addrFile.toUInt(0,16);
	addr2=addrJump.toUInt(0,16);
	s1=fn;
	actEnabled=1;
}

void Phone::jump(const QString&addr)
{
	int ac;
	do
	{
		ac=getAction();
	}
	while (ac>0 && ac<=3);
	if (!setAction(ACT_JUMP)) return;
	addr1=addr.toUInt(0,16);
	actEnabled=1;
}

void Phone::erase()
{
	int ac;
	do
	{
		ac=getAction();
	}
	while (ac>0 && ac<=3);
	if (!setAction(ACT_ERASE)) return;
	actEnabled=1;
}

void Phone::fullFlash(const QString&fn)
{
	int ac;
	do
	{
		ac=getAction();
	}
	while (ac>0 && ac<=3);
	if (!setAction(ACT_FULLFLASH)) return;
	s1=fn;
	actEnabled=1;
}


void Phone::doPlug(int idx)
{
	if (idx<0)
	{
		emit onMessage("Closing");
		flash_close();
		return;
	}
	if (wasOpened)
	{
		flash_close();
		wasOpened=0;
	}
	flash_setDevice(dev[idx]);
	if (flash_open())
	{
		emit onMessage("Unable to open device");
		return;
	}
	emit onMessage("Device is opened and ready");
	wasOpened=1;
}

void Phone::doSendCommand(const QString&cmd, const QString& arg)
{
	char cm[256];

	strncpy(cm,cmd.toLatin1().data(),256);
	if (flash_writeCommand(cm,arg.toLatin1().data(),arg.toLatin1().size(),5000)<0)
	{
		emit onMessage("Unable to send command");
		return;
	}
// 	emit onMessage(QString("[WRITE] %1(%2)").arg(cmd,arg));
}

void Phone::doRead()
{
	char buf[10240];
	int res=flash_read(buf,sizeof(buf)-1,50);
	if (res<0) return;
	buf[res]=0;
// 	emit onMessage(QString("[READ] %1").arg(buf));
}

int Phone::doSendAddr(unsigned int addr)
{
	char ackaddr[]={0x02, 'A','C','K', 0x1e, 'A','D','D','R',0x00};
	char s1[12];
	char s2[100];
	char buf[256];
	sprintf(s1,"%08X",addr);
	sprintf(s2,"%s%02X",s1,(unsigned char)crc(s1,8));
	int res;

	res=flash_writeCommand("ADDR",s2,strlen(s2),10000);
	if (res<=0)
	{
		emit onMessage(QString("FAILED to send ADDR at %1").arg(s1));
		return 0;
	}
// 	emit onMessage(QString("ADDR %1").arg(s2));
	res=flash_read(buf,256,10000);
	if (res<=0)
	{
		emit onMessage(QString("FAILED to read ADDR answer at %1").arg(s1));
		return 0;
	}
	buf[res]=0;
	QString tmp(buf);
// 	emit onMessage(tmp);
	buf[9]=0x00;
	if (strcmp(buf,ackaddr))
	{
		emit onMessage(QString("Incorrect ADDR answer at %1").arg(s1));
		return 0;
	}
	return 1;
}

int Phone::doSendFile(unsigned int addr, const QString&fn, int align)
{
#define MAXBINSIZE 0x2000
	int binSize=MAXBINSIZE;
	char ackbin[]={0x02, 'A','C','K', 0x1e, 'B','I','N',0x00};
	char buf[MAXBINSIZE+9]={0x02,'B','I','N',0x1e,0x00,0x00};
	char *fbuf=buf+7;
	char ans[256];
	int cnt,addrCnt;
	char cr;
	int realCnt;
	int ps,sz;
	int res;

	QFile file(fn);
	if (!file.open(QIODevice::ReadOnly))
	{
		emit onMessage(QString("Unable to open file %1").arg(fn));
		return 0;
	}

	while (!file.atEnd())
	{
		cnt=file.read(fbuf,binSize);
		if (!cnt) break;

		qDebug()<<"Read:"<<cnt << " POS="<<file.pos() <<" SIZE="<<file.size();

		addrCnt=cnt;
		if(align && !(cnt%8))
		{
			cnt=cnt/8+1;
			cnt=cnt*8;
			qDebug()<<"New cnt="<<cnt;
			memset(fbuf+addrCnt,0xFF,cnt-addrCnt);
		}

		buf[5]=cnt/0x100;
		buf[6]=cnt%0x100;
		cr=crc(fbuf-2,cnt+2);
		fbuf[cnt]=cr;
		fbuf[cnt+1]=0x03;

		if (!doSendAddr(addr))
			return 0;

		realCnt=cnt+9;
		ps=0;
		sz=0x1000;
		while (ps<realCnt)
		{
			if (ps+sz>realCnt)
				sz=realCnt-ps;
			res=flash_write(buf+ps,sz,10000);
			if (res<=0)
			{
				char s[100];
				sprintf(s,"%08X",addr);
				emit onMessage(QString("BIN failed at %1+%2").arg(s,ps));
				return 0;
			}
			ps+=sz;
		}

		res=flash_read(ans,255,10000);
		if (res<=0)
		{
			emit onMessage(QString("FAILED to read BIN answer"));
			return 0;
		}
		ans[res]=0;
// 		emit onMessage(QString(ans));
		ans[8]=0;

		if (strcmp(ans,ackbin))
		{
			emit onMessage(QString("Incorrect BIN answer"));
			return 0;
		}

		addr+=addrCnt;
		emit onProgress1(file.pos(),file.size());
		if (totalSize)
		{
			totalPos+=addrCnt;
			emit onProgress2(totalPos, totalSize);
		}
	}
	emit onProgress1(0,100);

	return 1;
}

int Phone::doSendLoader(unsigned int addrFile, unsigned int addrJump, const QString&fn)
{
	if(!doSendFile(addrFile,fn,1)) return 0;
	if(!doJump(addrJump)) return 0;
	return 1;
}

int Phone::doJump(unsigned int addr)
{
	char ackaddr[]={0x02, 'A','C','K', 0x1e, 'J','U','M','P',0x00};
	char s1[12];
	char s2[100];
	char buf[256];
	sprintf(s1,"%08X",addr);
	sprintf(s2,"%s%02X",s1,(unsigned char)crc(s1,8));
	int res;

	res=flash_writeCommand("JUMP",s2,strlen(s2),10000);
	if (res<=0)
	{
		emit onMessage(QString("FAILED to send JUMP at %1").arg(s1));
		return 0;
	}
	emit onMessage(QString("JUMP %1").arg(s2));
	res=flash_read(buf,256,10000);
	if (res<=0)
	{
		emit onMessage(QString("FAILED to read JUMP answer at %1").arg(s1));
		return 0;
	}
	buf[res]=0;
	QString tmp(buf);
	emit onMessage(tmp);
	buf[9]=0x00;
	if (strcmp(buf,ackaddr))
	{
		emit onMessage(QString("Incorrect JUMP answer at %1").arg(s1));
		return 0;
	}
	return 1;
}

int Phone::doErase()
{
	char cm[]={0x02,'E','R','A','S','E',0x1e,0x80,0x03,0x00};
	char ack[]={0x02, 'A','C','K', 0x1e, 'E','R','A','S','E',0x00};
	char buf[1024];
	int res;

	if (flash_write(cm,strlen(cm),5000)<0)
	{
		emit onMessage("Unable to send ERASE");
		return 0;
	}
	emit onMessage(QString(cm+1));
	while(1)
	{
		res=flash_read(buf,sizeof(buf)-1,1000);
		if (res<=0) continue;
		if (res<0)
		{
			emit onMessage("Failed to read from phone");
			return 0;
		}
		buf[res]=0;
		emit onMessage(buf);
		buf[10]=0x00;
		if (strcmp(buf,ack))
		{
			emit onMessage("Incorrect ERASE answer");
			return 0;
		}
		break;
	}
	return 1;
}

int Phone::doFullFlash(const QString&fn)
{
	if (devCnt!=1)
	{
		emit onMessage("No devices found or devCnt!=1");
		return 0;
	}
	setDevice(0);

	QFileInfo fi(fn);
	QString dir=fi.absolutePath();

	QFile file(fn);
	if (!file.open(QIODevice::ReadOnly))
	{
		emit onMessage(QString("Unable to open file %1").arg(fn));
		return 0;
	}
	QTextStream stream(&file);
	QString line;
	totalSize=0;
	totalPos=0;
	while (!stream.atEnd())
	{
		line=stream.readLine();
		fi.setFile(QString("%1/%2.bin").arg(dir,line));
		if (!fi.exists())
		{
			emit onMessage(QString("File not found for addr=%1").arg(line));
			return 0;
		}
		totalSize+=fi.size();
	}
	stream.seek(0);
	emit onMessage(QString("Need to flash %1 bytes").arg(totalSize));
	int first=1;

	while (!stream.atEnd())
	{
		line=stream.readLine();
		if (first)
		{
			emit onMessage("Sending loader...");
			first=0;
			if (!doSendLoader(line.toUInt(0,16),line.toUInt(0,16),QString("%1/%2.bin").arg(dir,line)))
			{
				emit onMessage(QString("Unable to send loader at %1").arg(line));
				return 0;
			}
			emit onMessage("Waiting for device reenumeration...");

			int wasNew=0;
			for (int i=0; i<100; i++)
			{
				if (doUpdateList())
				{
					wasNew=1;
					if (!devCnt) continue;
					msleep(500);
					doUpdateList();
					break;
				}
				msleep(200);
			}
			if (!wasNew || !devCnt)
			{
				emit onMessage("Unable to find new devices");
				return 0;
			}
			setDevice(0);
			emit onMessage("Erasing phone memory...");
			if (!doErase())
			{
				emit onMessage("Unable to erase phone memory");
				return 0;
			}
		}
		else
		{
			emit onMessage(QString("Sending CG at %1").arg(line));
			if (!doSendFile(line.toUInt(0,16),QString("%1/%2.bin").arg(dir,line),1))
			{
				emit onMessage(QString("Unable to send CG at %1").arg(line));
				return 0;
			}
		}
	}

	emit onMessage("Sending POWER_DOWN..");
	doSendCommand("POWER_DOWN",0);

	return 1;
}

char Phone::crc(char* buf, int sz)
{
	int cr=0;
	for (int i=0;i<sz;i++)
	{
		cr+=(unsigned char)(buf[i]);
		cr%=0x100;
	}
	return cr;
}
