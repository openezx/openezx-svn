#ifndef PHONETOOLS_H
#define PHONETOOLS_H

#include <QtGui/QDialog>
#include "ui_phonetools.h"
#include "UsbPhone.h"

class phonetools : public QDialog
{
	Q_OBJECT

public:
	phonetools(QWidget *parent = 0, Qt::WFlags flags = 0);
	~phonetools();

private:
	Ui::phonetoolsClass ui;

private slots:
	void on_jump_clicked();
	void on_bootLinux_clicked();
	void on_findRamdisk_clicked();
	void on_findKernel_clicked();
	void on_flash_clicked();
	void on_upLoad_clicked();
	void on_downLoad_clicked();
	void on_findButton_clicked();

public:
	UsbPhone *pPhone;
};

#endif // PHONETOOLS_H
