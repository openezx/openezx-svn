/* opentapi - minimal implementation of tapisrv
 *
 * (C) 2006 by Daniel Willmann <daniel@totalueberwachung.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include <stdlib.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>

#include "opentapi.h"

fd_set read_set, write_set, master_read_set, master_write_set;

int max_fd, mux[MUX_FILES_MAX], con[MUX_FILES_MAX], serv[MUX_FILES_MAX], logfile;
int debug = 1;

struct mux_buf con_buffer[MUX_FILES_MAX];
struct mux_buf mux_buffer[MUX_FILES_MAX];

int open_files() {

	int ret, i;
	char filename[40];
	struct sockaddr_in addr;

	memset(&addr, 0, sizeof(addr));
	addr.sin_family      = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	for (i = 0; i < MUX_FILES_MAX; i++) {
		snprintf(filename, 40, "%s%d", MUX_BASE, i);
		if ((mux[i] = open(filename, O_RDWR|O_NONBLOCK|O_NOCTTY)) == -1)
			error(1, errno, "Failed to open file %s", filename);

		FD_SET(mux[i], &master_read_set);
		if (mux[i] > max_fd)
			max_fd = mux[i];

		printf("Opened /dev/mux%d as filedescriptor %d\n", i, mux[i]);
	}
	for (i = 0; i < MUX_FILES_MAX; i++) {

		if ((serv[i] = socket(PF_INET, SOCK_STREAM, 0)) == -1)
			error(1, errno, "Failed to open socket for /dev/mux%s", filename);

		addr.sin_port = htons(PORTBASE+i);

		if (bind(serv[i], (struct sockaddr *)&addr, sizeof(addr)) == -1)
			error(1, errno, "Error binding socket to port %d", PORTBASE+i);
	
		if (listen(serv[i], 0) == -1)
			error(1, errno, "Error listening on socket on port %d", PORTBASE+i);

		FD_SET(serv[i], &master_read_set);
		if (serv[i] > max_fd)
			max_fd = serv[i];
		printf("Bound Socket on port %d\n", PORTBASE+i);
	}
		if ((logfile = open(LOGFILENAME, O_WRONLY | O_APPEND | O_CREAT)) == -1)
			error(0, errno, "Failed to open %s for appending", LOGFILENAME);

}

void init_mux_buffer(struct mux_buf *mb) {
	mb->length = 0;
}

void remove_data(struct mux_buf *buf, int length) {
	memmove(&buf->buffer[0], &buf->buffer[length], length);
	buf->length -= length;
}

void add_data(struct mux_buf *mbuf, char *buf, int length) {
	if (length > MUX_BUFFER_SIZE) {
		printf("%s: length too big(%i)!\n", __func__, length);
		exit(1);
	}

	if (mbuf->length + length >= MUX_BUFFER_SIZE)
		remove_data(mbuf, (mbuf->length + length) - MUX_BUFFER_SIZE);

	memcpy(&mbuf->buffer[mbuf->length], buf, length);
	mbuf->length += length;
}

int main (void) {
	struct timeval tv;
	int result, i, length;
	char buffer[1005];
	char header[50];
	
	char *initcmd = "AT\r";

	FD_ZERO(&master_read_set);
	FD_ZERO(&master_write_set);
	FD_ZERO(&read_set);
	FD_ZERO(&write_set);

	max_fd = 0;
	for (i=0; i < MUX_FILES_MAX; i++) {
		init_mux_buffer(&con_buffer[i]);
		init_mux_buffer(&mux_buffer[i]);
		con[i] = -1;
	}

	open_files();

	add_data(&mux_buffer[0], initcmd , strlen(initcmd));
	FD_SET(mux[0], &master_write_set);
	printf("%d: %s\n", mux_buffer[0].length, mux_buffer[0].buffer);

	while (1) {
		memcpy(&read_set, &master_read_set, sizeof(master_read_set));
		memcpy(&write_set, &master_write_set, sizeof(master_write_set));
		tv.tv_sec = 5;
		tv.tv_usec = 0;
		result = select(max_fd+1, &read_set, &write_set, NULL, &tv);
		for (i = 0; (i < MUX_FILES_MAX) && (result > 0); i++) {
			if (FD_ISSET(serv[i], &read_set)) {
				result--;
				if (con[i] == -1) {
					if (debug)
						printf("New conn: %d\n", i);
					con[i] = accept(serv[i], NULL, 0);
					if (con[i] != -1) {
						if (con[i] > max_fd)
							max_fd = con[i];
						FD_SET(con[i], &master_read_set);
						if (con_buffer[i].length > 0)
							FD_SET(con[i], &master_write_set);
					}
					if (debug)
						printf("Got connection for /dev/mux%d\n", i);
	
					snprintf(header, 50, "Connect for /dev/mux%d:\n", i);
					write(logfile, header, strlen(header));
					write(logfile, buffer, length);
					write(logfile, "\n", 1);
				} else {
					int tmp;
					if (debug)
						printf("Rejecting conn: %d\n", i);
					tmp = accept(serv[i], NULL, 0);
					close(tmp);
				}
			}

			// Socket handling
			if ((con[i] > 0) && FD_ISSET(con[i], &read_set)) {
				if (debug)
					printf("Socket%d: read\n", i);
				result--;
				length = read(con[i], buffer, 1000);
				if (length > 0) {
					add_data(&mux_buffer[i], buffer, length);
					FD_SET(mux[i], &master_write_set);
					if (debug)
						printf("Got %i bytes for /dev/mux%d\n", length, i);

					snprintf(header, 50, "Read for /dev/mux%d:\n", i);
					write(logfile, header, strlen(header));
					write(logfile, buffer, length);
					write(logfile, "\n", 1);
				} else if (length == 0) {
					FD_CLR(con[i], &master_read_set);
					FD_CLR(con[i], &master_write_set);
					FD_CLR(con[i], &read_set);
					FD_CLR(con[i], &write_set);
					con[i] = -1;
					if (debug)
						printf("Closing connection for for /dev/mux%d\n", i);

					snprintf(header, 50, "Closing connection for /dev/mux%d:\n", i);
					write(logfile, header, strlen(header));
					write(logfile, buffer, length);
					write(logfile, "\n", 1);
				}
			}
			if ((con[i] > 0) && FD_ISSET(con[i], &write_set)) {
				if (debug)
					printf("Socket%d: write\n", i);
				result--;
				length = write(con[i], con_buffer[i].buffer, con_buffer[i].length);

				if (length > 0) {
					remove_data(&con_buffer[i], length);
				}
				if (con_buffer[i].length == 0) {
					FD_CLR(con[i], &master_write_set);
				}

				if (debug)
					printf("Wrote %i bytes for /dev/mux%d\n", length, i);
				
				snprintf(header, 50, "Write for /dev/mux%d:\n", i);
				write(logfile, header, strlen(header));
				write(logfile, con_buffer[i].buffer, con_buffer[i].length);
				write(logfile, "\n", 1);
			}

			// Mux File handling
			if (FD_ISSET(mux[i], &read_set)) {
				if (debug)
					printf("Mux%d: read\n", i);
				result--;
				ioctl(mux[i], FIONREAD, &length);
				length = read(mux[i], buffer, length);
				if (length > 0) {
					add_data(&con_buffer[i], buffer, length);
					if (con[i] != -1)
						FD_SET(con[i], &master_write_set);
				}

				if (debug)
					printf("Got %i bytes from /dev/mux%d\n", length, i);

				snprintf(header, 50, "Read from /dev/mux%d:\n", i);
				write(logfile, header, strlen(header));
				write(logfile, buffer, length);
				write(logfile, "\n", 1);
			}

			if (FD_ISSET(mux[i], &write_set)) {
				if (debug)
					printf("Mux%d: write\n", i);
				result--;

				length = write(mux[i], mux_buffer[i].buffer, mux_buffer[i].length);

				if (length > 0) {
					remove_data(&mux_buffer[i], length);
				}
				if (mux_buffer[i].length == 0) {
					FD_CLR(mux[i], &master_write_set);
				}

				if (debug)
					printf("Wrote %i bytes to /dev/mux%d\n", length, i);
				
				snprintf(header, 50, "Write to /dev/mux%d:\n", i);
				write(logfile, header, strlen(header));
				write(logfile, mux_buffer[i].buffer, mux_buffer[i].length);
				write(logfile, "\n", 1);
			}
		}
	}
}

