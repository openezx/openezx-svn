/*-------------------------------------------------------------------------
 * Filename:      uucodec.c
 * Version:       $Id: uucodec.c,v 1.3 2002/01/03 16:07:18 erikm Exp $
 * Copyright:     Copyright (C) 1999, Jan-Derk Bakker
 * Author:        Jan-Derk Bakker <J.D.Bakker@its.tudelft.nl>
 * Description:   uu{en|de}code functiuonality for uploading of kernels 
 *                and the like
 * Created at:    Wed Aug 25 20:00:00 1999
 * Modified by:   Erik Mouw <J.A.K.Mouw@its.tudelft.nl>
 * Modified at:   Tue Sep 28 23:25:15 1999
 *-----------------------------------------------------------------------*/
/*
 * uucodec.c: uu{en|de}code functiuonality for uploading of kernels and 
 *            the like
 *
 * Copyright (C) 1999  Jan-Derk Bakker (J.D.Bakker@its.tudelft.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ident "$Id: uucodec.c,v 1.3 2002/01/03 16:07:18 erikm Exp $"

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/errno.h>
#include <blob/serial.h>
#include <blob/util.h>




#define INT_BUF_SIZE 1024
#define	MAX_RETRIES 10

#define TEST_MAX_RETRIES do { 				                   \
	if(retries++ > MAX_RETRIES) {	 		                   \
		return -ETIMEOUT;				           \
	}						                   \
} while(0)

#define	DEC(c)	(((c) - ' ') & 077)		/* single character decode */
#define IS_DEC(c) ( (((c) - ' ') >= 0) &&  (((c) - ' ') <= 077 + 1) )
/* #define IS_DEC(c) (1) */

#define OUT_OF_RANGE do {	\
	serial_write('\n');	\
        SerialOutputString(buf);	\
	serial_write('\n');	\
        return -ERANGE; \
} while(0)

#define PUT_CHAR(x) do {	             \
	if(bytesWritten < bufLen)	     \
	        bufBase[bytesWritten++] = x; \
} while(0)




int UUDecode(char *bufBase, int bufLen) {
/*	Receives and decodes an incoming uuencoded stream. Returns the number of 
	bytes put in the buffer on success, or -1 otherwise. */

	int n, linesReceived = 0;
	char ch, *p;
	int bytesWritten = 0, retries = 0;
	char buf[INT_BUF_SIZE];

	/* Search for header line. We don't care about the mode or filename */
	retries = 0;
	do {
		SerialInputString(buf, sizeof(buf), 6);
		TEST_MAX_RETRIES;		
	} while (strncmp(buf, "begin ", 6) != 0);

/*
	SerialOutputString("Bytes received : 0x00000000");
*/
	/* for each input line */
	for (;;) {
		if (SerialInputString(p = buf, sizeof(buf), 2) == 0) {
			return -ETOOSHORT;
		}
		/* Status print to show where we are at right now */
		if((linesReceived++ & 0x007F) == 0) {
			serial_write('.');
		}
		/*
		 * `n' is used to avoid writing out all the characters
		 * at the end of the file.
		 */
		if ((n = DEC(*p)) <= 0)
			break;
		for (++p; n > 0; p += 4, n -= 3)
			if (n >= 3) {
				if (!(IS_DEC(*p) && IS_DEC(*(p + 1)) && 
				     IS_DEC(*(p + 2)) && IS_DEC(*(p + 3))))
                                	OUT_OF_RANGE;

				ch = DEC(p[0]) << 2 | DEC(p[1]) >> 4;
				PUT_CHAR(ch);
				ch = DEC(p[1]) << 4 | DEC(p[2]) >> 2;
				PUT_CHAR(ch);
				ch = DEC(p[2]) << 6 | DEC(p[3]);
				PUT_CHAR(ch);
				
			}
			else {
				if (n >= 1) {
					if (!(IS_DEC(*p) && IS_DEC(*(p + 1))))
						OUT_OF_RANGE;
					ch = DEC(p[0]) << 2 | DEC(p[1]) >> 4;
					PUT_CHAR(ch);
				}
				if (n >= 2) {
					if (!(IS_DEC(*(p + 1)) && 
						IS_DEC(*(p + 2))))
						OUT_OF_RANGE;

					ch = DEC(p[1]) << 4 | DEC(p[2]) >> 2;
					PUT_CHAR(ch);
				}
				if (n >= 3) {
					if (!(IS_DEC(*(p + 2)) && 
						IS_DEC(*(p + 3))))
						OUT_OF_RANGE;
					ch = DEC(p[2]) << 6 | DEC(p[3]);
					PUT_CHAR(ch);
				}
			}
	}
	serial_write('\n');
	if (SerialInputString(p = buf, sizeof(buf), 2) == 0 || (strncmp(buf, "end", 3))) {
		/* no "end" line */
		return -ETOOSHORT;
	}
	return(bytesWritten);
} /* UUDecode */


/* we currently don't need to upload files, so disable UUEncode() */
#if 0
/* ENC is the basic 1 character encoding function to make a char printing */
#define	ENC(c) ((c) ? ((c) & 077) + ' ': '`')


void UUEncode(char *bufBase, int bufLen) {
	register int ch, n;
	register char *p;
	char buf[80];

	SerialOutputString("begin 644 testme.jdb\n");
	while (bufLen > 0) {
		n = (bufLen > 45) ? 45 : bufLen;
		MyMemCpyChar(buf, bufBase, n);
		bufBase += n;
		bufLen -= n;
		ch = ENC(n);
		serial_write(ch);
		for (p = buf; n > 0; n -= 3, p += 3) {
			ch = *p >> 2;
			ch = ENC(ch);
			serial_write(ch);
			ch = ((*p << 4) & 060) | ((p[1] >> 4) & 017);
			ch = ENC(ch);
			serial_write(ch);
			ch = ((p[1] << 2) & 074) | ((p[2] >> 6) & 03);
			ch = ENC(ch);
			serial_write(ch);
			ch = p[2] & 077;
			ch = ENC(ch);
			serial_write(ch);
		}
		serial_write('\n');
	}
	ch = ENC('\0');
	serial_write(ch);
	serial_write('\n');
	SerialOutputString("end\n");
	
} /* UUEncode */
#endif
