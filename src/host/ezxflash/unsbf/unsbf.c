/***************************************************************************
 *   Copyright (C) 2006 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INT32 unsigned int

int main(int argc, char *argv[])
{
    FILE *fp;
    FILE *fpl;
    
    if (argc!=2)
    {
	printf("Usage: parseheader file.sbf\n");
	return 0;
    }
    
    fp=fopen(argv[1],"r");
    if (fp==0)
    {
	printf("fp==0\n");
	return 2;
    }
    
    // Looks like 0x553 is correct offset. If not, maybe it's stored somewhere
    // in sbf file.
    //fseek(fp, 0x573,0);
    fseek(fp,0x553,0);

    fpl=fopen("out.txt","r");
    if (fpl==0)
    {
	printf("out.txt not found.. launch parseheader first\n");
	return 3;
    }
    
    char s1[1024];
    int cg;
    int sz;
    int i;

    char ch[2];
    
    FILE *fpo =0;
    int cg_idx=0;
    int addr;

    while (fscanf(fpl,"%d %x %d",&cg,&addr,&sz) >0)
    {
	printf("=> CG=%d, START=%08x SIZE=%d\n",cg,addr,sz);
	sprintf(s1,"%08x.bin",addr);
	if (fpo) fclose(fpo);
	fpo=fopen(s1,"w+");
	for (int i=0; i<(sz/2); i++)
	{
	    if (fread(ch,1,2,fp)!=2)
	    {
		printf("No more data to read\n");
		return 0;
	    }	
	    fwrite(ch,1,2,fpo);
	}

	cg_idx++;	
	if (cg_idx!=3)
	    fseek(fp,0x1d,SEEK_CUR);
    }    
    printf("Filepos=%d\n",ftell(fp));
}
