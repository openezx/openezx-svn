	#pragma once


struct phonetype {
	const char * name;
	unsigned short product_id;
	unsigned char out_ep;
	unsigned char in_ep;
	unsigned int kernel_addr;
	unsigned int initrd_addr;
	unsigned int params_addr;
	const char *code;
	int code_size;
};

struct usb_dev_handle;

class UsbPhone
{
public:
	UsbPhone(const struct phonetype *type, struct usb_dev_handle *handle);
public:
	virtual ~UsbPhone(void);
	const struct phonetype *type;
	unsigned long protect_addr;
private:
	struct usb_dev_handle *handle;
public:

	int ezx_blob_recv_reply(char *b);
	int ezx_blob_send_command(const char *command, const char *payload, int len, char *reply);
	unsigned char ezx_csum(const char *data, int len);
	int ezx_blob_cmd_addr(unsigned int addr);
	int ezx_blob_cmd_jump(unsigned int addr);
	int ezx_blob_cmd_rbin(unsigned int addr, unsigned short size, unsigned char *response);
	int ezx_blob_cmd_bin(const char *data, unsigned short size);
	int ezx_blob_cmd_flash(unsigned int source, unsigned int dest, unsigned int size);
	int ezx_blob_dload_program(unsigned int addr, char *data, int size);
	int ezx_blob_load_program(unsigned short phone_id, unsigned int addr, const char *data, int size);
	int ezx_blob_flash_program(unsigned int addr, const char *data, int size);
public:
	int bootLinux(const char * kernelFileName, unsigned int machine_id=0, const char * cmdLine=0, const char *ramdiskFileName = 0);
};
