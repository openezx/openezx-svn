/*
 *  pm.c - ezxd plugin that takes care PM related tasks
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <linux/input.h>
#include <linux/uinput.h>
#define UINPUT_DEV "/dev/input/uinput"

#include "ezxd.h"

extern int mux_write(unsigned int, char *);


int uinp_fd = -1;

static int uinput_create(void)
{
	int keycode = KEY_POWER;
	struct uinput_user_dev uinp;

	uinp_fd = open(UINPUT_DEV, O_WRONLY|O_NONBLOCK);
	if (uinp_fd < 0)
		return -1;

	/* initialize uinput */
	memset(&uinp, 0, sizeof(uinp));
	strncpy(uinp.name, "ezxd", UINPUT_MAX_NAME_SIZE);
	uinp.id.version = 4;
	uinp.id.bustype = BUS_VIRTUAL;

	ioctl(uinp_fd, UI_SET_EVBIT, EV_KEY);
	ioctl(uinp_fd, UI_SET_EVBIT, EV_REL);

	/* allow the keycode to be passed */
	ioctl(uinp_fd, UI_SET_KEYBIT, keycode);

	/* create input device into input sub-system */
	write(uinp_fd, &uinp, sizeof(uinp));
	if (ioctl(uinp_fd, UI_DEV_CREATE) < 0)
		return -1;

	return 0;
}

static int uinput_destroy(void)
{
	if (uinp_fd >= 0) {
		ioctl(uinp_fd, UI_DEV_DESTROY);
		close(uinp_fd);
		uinp_fd = -1;
	}
	return 0;
}

static int powerkey_uinput_write(int pressed)
{
	int keycode = KEY_POWER;
	struct input_event event;

	if (uinp_fd < 0)
		return -1;

	/* inject the event with the correct value (pressed/released) */
	memset(&event, 0, sizeof(event));
	gettimeofday(&event.time, NULL);

	event.type = EV_KEY;
	event.code = keycode;
	event.value = pressed;
	write(uinp_fd, &event, sizeof(event));

	event.type = EV_SYN;
	event.code = SYN_REPORT;
	event.value = 0;
	write(uinp_fd, &event, sizeof(event));

	return 0;
}

static int do_poweroff(void)
{
	/* FIXME CPWR should be sent by a signal handler */
	system("halt -p");
	mux_write(MUX(5), "AT+CPWR=6\r");
	return 0;
}

static int cmd_usbcharge(unsigned int mux, char *s, struct sb *client)
{
	char *arg = strstr(s, " ") + 1;
	if (!strcmp(arg, "on"))
		mux_write(MUX(5), "AT+USBSTAT=255,1\r");
	else
		mux_write(MUX(5), "AT+USBSTAT=0,0\r");
	return 0;

}

static int cmd_batt_low(unsigned int mux, char *s, struct sb *client)
{
	return do_poweroff(); /* or powerkey_uinput_write(1) ? */
}

static int cmd_key_event(unsigned int mux, char *s, struct sb *client)
{
	char key, press;
	if (sscanf(s, "+EKEV: %hhd,%hhd", &key, &press) != 2)
		return -1;
	if (key == 19)
		powerkey_uinput_write(press);
	return 0;
}

struct ezxd_plugin pm_plug = {
	.start = uinput_create,
	.stop = uinput_destroy,
	.cmds = {
		{ "^charger (on|off)$", 0, cmd_usbcharge, },
		{ "^\\+EBLVT$", (MUX(1) | MUX(2)), cmd_batt_low, },
		{ "EKEV: [0-9]*,[0-1]$", MUX(5), cmd_key_event, },
	},
	.n_cmds = 3,
};
