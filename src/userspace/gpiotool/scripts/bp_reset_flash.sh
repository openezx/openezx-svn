#!/bin/sh
GT=/home/root/gpio/gpiotool

$GT 	--write-gpio 82 1 --write-af 82 OUT 	\
	--write-gpio 57 0 --write-af 57 OUT 	\
	--write-gpio 82 0 			\
	--msleep 1000				\
	--write-gpio 82 1			\
	--write-af 58 IN

