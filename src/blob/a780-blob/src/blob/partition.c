/*
 * partition.c: flash partitioning
 *
 * Copyright (C) 2001  Erik Mouw <J.A.K.Mouw@its.tudelft.nl>
 *
 * $Id: partition.c,v 1.2 2002/01/03 16:07:18 erikm Exp $
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ident "$Id: partition.c,v 1.2 2002/01/03 16:07:18 erikm Exp $"

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/flash.h>
#include <blob/partition.h>
#include <blob/serial.h>
#include <blob/error.h>



static partition_table_t flash_partition_table;


/* FIXME: this is for the time being */
extern int read_bootldr_partition_table(partition_table_t *ptable);


static int read_partition_table(void)
{
	int rv;
	int i;
	
	/* so far we only know about bootldr flash partitions */
	rv = read_bootldr_partition_table(&flash_partition_table);

	if(rv < 0) {
		printerror(rv, "can't get partition table\n");
		return rv;
	}

	SerialOutputString("Flash partition layout:\n");
	for(i = 0; i < flash_partition_table.numpartitions; i++) {
		SerialOutputString("  0x");
		SerialOutputHex(flash_partition_table.partition[i].size);
		SerialOutputString(" @ 0x");
		SerialOutputHex(flash_partition_table.partition[i].offset);

		if(flash_partition_table.partition[i].flags & PART_LOAD) {
			SerialOutputString(", load at 0x");
			SerialOutputHex(flash_partition_table.partition[i].mem_base);
			SerialOutputString(", entry point at 0x");
			SerialOutputHex(flash_partition_table.partition[i].entry_point);
		}
		
		serial_write('\n');
	}

	return 0;
}
