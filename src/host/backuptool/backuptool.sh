#!/bin/bash
# This is a firmware extraction and identification tool
# on early development and still not useful

# config
BLOB=../gen-blob/src/blob/blob
BOOT_USB=../boot_usb/boot_usb
DD=/bin/dd
# end config

PHONE=unknown

function boot_usb {
	$BOOT_USB $@ > /dev/null
	if [ $? != 0 ]; then
		echo boot_usb error
		exit 1
	fi
}

function dd {
	$DD $@ > /dev/null 2>&1
	if [ $? != 0 ]; then
		echo dd error
		exit 1
	fi
}

if [ ! -x $BOOT_USB ]; then
	echo $BOOT_USB not found
	exit 1
fi

if [ ! -f $BLOB ];then
	echo $BLOB not found
	exit 1
fi

echo sending gen-blob
boot_usb setflag usb
boot_usb $BLOB 0

echo waiting for reenumeration
sleep 10

echo -n "reading bootloader: "
# gen1 bootloaders
boot_usb read 0 131072 tmpfile
MD5=`md5sum tmpfile | cut -f1 -d' '`
if [ "$MD5" = "d933dc478dadd5dbd5cfc6e545bd2571" ]; then
	PHONE_TYPE="A780"
	PHONE_GEN="1"
	echo "A780 3.0 2004-05-18 unpatched $MD5"
elif [ "$MD5" = "f644966ad2486cb4640066f83330d378" ]; then
	PHONE_TYPE="A780"
	PHONE_GEN="1"
	echo "A780 3.0 2004-05-18 with suspend/resume patch $MD5"
elif [ "$MD5" = "e035ed9de8dd28e682aea77084661e45" ]; then
	PHONE_TYPE="E680"
	PHONE_GEN="1"
	echo "E680 3.0 2004-05-18 $MD5"
else
	# gen2 bootloaders
	rm -f tmpfile
	boot_usb read 0 262144 tmpfile
	MD5=`md5sum tmpfile | cut -f1 -d' '`
	if [ "$MD5" = "336a8b944526fccc2a04e0dc873a5ad2" ];then
		PHONE_TYPE="A1200"
		PHONE_GEN="2"
		echo "A1200 2.15R HAINAN $MD5"
	elif [ "$MD5" = "a1668bbb8bf572896f09f3d42c88b3e3" ];then
		PHONE_TYPE="A910"
		PHONE_GEN="2"
		echo "A910 2.15R MARTINIQUE $MD5"
	elif [ "$MD5" = "dd21ce104f4a264fe8fd61aef0029ae8" ];then
		PHONE_TYPE="A910"
		PHONE_GEN="2"
		echo "A910 2.45R MARTINIQUE $MD5"
	else
		echo "unknown $MD5"
		rm -f tmpfile
		exit 1
	fi
fi

if [ "$PHONE_GEN" = "1" ];then
	BOOTLOADER_ADDR=0
	BOOTLOADER_SIZE=131072
	KERNEL_ADDR=131072
	KERNEL_SIZE=917504
elif [ "$PHONE_GEN" = "2" ];then
	BOOTLOADER_ADDR=0
	BOOTLOADER_SIZE=262144
	KERNEL_ADDR=655360
	KERNEL_SIZE=1048576
	LBL_ADDR=524288
	LBL_SIZE=131072
fi
mv tmpfile `printf '%08x' $BOOTLOADER_ADDR`-bootloader.part

if [ "$PHONE_GEN" = "2" ];then
	echo -n "reading linux loader: "
	boot_usb read $LBL_ADDR $LBL_SIZE tmpfile
	MD5=`md5sum tmpfile | cut -f1 -d' '`
	echo "unknown $MD5"
	dd if=tmpfile of=linuxloader.sig bs=1024 count=2
	dd if=tmpfile of=linuxloader.bin bs=1024 skip=2
	mv tmpfile `printf '%08x' $LBL_ADDR`-linuxoader.part
fi

echo -n "reading kernel: "
boot_usb read $KERNEL_ADDR $KERNEL_SIZE tmpfile
# gen-blob detection
if [ "$PHONE_GEN" = "2" ]; then
	dd if=tmpfile of=tmpfile2 bs=1024 skip=2 count=50
else
	dd if=tmpfile of=tmpfile2 bs=1024 count=50
fi
GB_MD5=`md5sum tmpfile2 | cut -f1 -d' '`
rm -f tmpfile2
if [ "$GB_MD5" = "efddb486c4eb55df8cc186db0aca6c5e" ];then
	echo "gen-blob 090215 $GB_MD5"
else
	# original kernel detection
	MD5=`md5sum tmpfile | cut -f1 -d' '`
	if [ "$MD5" = "asdasd" ]; then
		echo bla bla bla $MD5
	else
		echo "unknown $MD5/$GB_MD5"
	fi
fi
if [ "$PHONE_GEN" = "2" ];then
	dd if=tmpfile of=kernel.sig bs=1024 count=2
	dd if=tmpfile of=kernel.bin bs=1024 skip=2
fi
mv tmpfile `printf '%08x' $KERNEL_ADDR`-kernel.part

if [ "$PHONE_TYPE" = "A780" ] || [ "$PHONE_TYPE" = "E680" ];then
	ROOTFS_ADDR=1179648
	ROOTFS_SIZE=26083328
	USERFS_ADDR=27262976
	USERFS_SIZE=5767168
	SETUP_ADDR=33161216
	SETUP_SIZE=131072
	LOGO_ADDR=33292288
	LOGO_SIZE=131072
fi

echo -n "reading rootfs: "
boot_usb read $ROOTFS_ADDR $ROOTFS_SIZE tmpfile
MD5=`md5sum tmpfile | cut -f1 -d' '`
if [ "$MD5" = "858e4dcf1c006fda7950e18e8b668f55" ]; then
	echo "A780 european rootfs ripped by wyrm $MD5"
elif [ "$MD5" = "806e79f51e76bab5c4c9bd7e18ebfb6e" ];then
	echo "E680 unknown rootfs ripped by wyrm $MD5"
else
	echo "unknown $MD5"
fi
mv tmpfile `printf '%08x' $ROOTFS_ADDR`-rootfs.part

echo -n "reading userfs: "
boot_usb read $USERFS_ADDR $USERFS_SIZE tmpfile
MD5=`md5sum tmpfile | cut -f1 -d' '`
echo $MD5
mv tmpfile `printf '%08x' $USERFS_ADDR`-userfs.part

echo -n "reading setup: "
boot_usb read $SETUP_ADDR $SETUP_SIZE tmpfile
MD5=`md5sum tmpfile | cut -f1 -d' '`
echo $MD5
mv tmpfile `printf '%08x' $SETUP_ADDR`-setup.part

echo -n "reading logo: "
boot_usb read $LOGO_ADDR $LOGO_SIZE tmpfile
MD5=`md5sum tmpfile | cut -f1 -d' '`
if [ "$MD5" = "e8f7aed00d584032547abd3325a97ae9" ] ||
   [ "$MD5" = "e5740801849cd1388d820dac551188b5" ];then
	echo "static HelloMoto logo $MD5"
else
	echo "unknown $MD5"
fi
mv tmpfile `printf '%08x' $LOGO_ADDR`-logo.part

#FIXME: implement reading/identification of other partitions and phones

boot_usb off
