#include "UsbPhoneFactory.h"
#include "UsbPhone.h"
#include <usb.h>
#include <stdio.h>
#define EZX_VENDOR_ID 0x22b8
#define pxa_code "\x08\x10\x9F\xE5\x0C\x00\x4F\xE2\x01\x0A\x80\xE2\x00\xF0\xA0\xE1"
#define pxa_code_s 16


struct phonetype phonetypes[] = {
{ "A780/E680",        0x6003, 0x02, 0x81, 0xa0200000, 0xa0400000, 0xa0000100, pxa_code, pxa_code_s },
{ "Generic Blob",     0xbeef, 0x02, 0x81, 0xa0200000, 0xa0400000, 0xa0000100, pxa_code, pxa_code_s }, /* pxa_code is temporary here */
{ "A780/E680 Blob2",  0x6021, 0x02, 0x81, 0xa0300000, 0xa0400000, 0xa0000100, pxa_code, pxa_code_s },
{ "E2/A1200/E6/A910", 0x6023, 0x01, 0x82, 0xa0de0000, /*FIXME*/0, /*FIXME*/0, pxa_code, pxa_code_s },
{ "RAZR2 V8",         0x6403, 0x01, 0x82, 0xa0de0000, /*FIXME*/0, /*FIXME*/0, NULL, 0 },
{ "Unknown",          0x0000, 0x00, 0x00, 0x00000000, 0x00000000, 0x00000000, NULL,	0 }
};

UsbPhoneFactory::UsbPhoneFactory(void)
{
}

UsbPhoneFactory::~UsbPhoneFactory(void)
{
}

UsbPhone * UsbPhoneFactory::FindPhone(void)
{
	usb_init();
	usb_find_busses();
	usb_find_devices();


	
	struct usb_bus *bus;

	for (bus = usb_busses; bus; bus = bus->next) {
		struct usb_device *dev;
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == EZX_VENDOR_ID)
			{
				int n;
				for (n=0;phonetypes[n].product_id!=0;n++) {
					if (dev->descriptor.idProduct == phonetypes[n].product_id) {
						UsbPhone *pPhone = new UsbPhone(&phonetypes[n],usb_open(dev));
						return pPhone;
					}
				}
			}
		}
	}
	return NULL;
}
