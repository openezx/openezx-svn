#include <QtGui>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	app.setStyle("plastique");
	
	MainWindow win;
	win.show();//Maximized();
	int res=app.exec();
	return res;
}
