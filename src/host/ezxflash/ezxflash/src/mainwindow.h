/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"

#include "phone.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	MainWindow(QWidget *parent=0);
	~MainWindow();
private:
	Ui::MainWindow ui;
	Phone *phone;

	struct flash_device *devLst;
	int devCnt;

	void setDevice(int);
public slots:
	void sltMessage(const QString& msg);
	void sltProgress1(int a, int b);
	void sltProgress2(int a, int b);
	void sltDeviceList(void *lst, int cnt);
	void on_btnUseDevice_clicked();
	void on_btnSendCommand_clicked();
	void on_btnSendFile_clicked();
	void on_btnSendLoader_clicked();
	void on_btnClear_clicked();
	void on_btnErase_clicked();
	void on_btnFullFlash_clicked();


signals:
	void sigTest();
};

#endif
