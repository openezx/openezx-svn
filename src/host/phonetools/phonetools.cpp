#include "phonetools.h"
#include "UsbPhoneFactory.h"
#include <QFileDialog>
#include <QFile>
phonetools::phonetools(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags), pPhone(0)
{
	ui.setupUi(this);
}

phonetools::~phonetools()
{

}


void phonetools::on_findButton_clicked()
{
	if(pPhone)
		delete pPhone;
	pPhone = UsbPhoneFactory::FindPhone();
	if (pPhone){
		unsigned int firstCode;
		ui.phoneStatus->setText(pPhone->type->name);
		pPhone->ezx_blob_send_command("RQSN", NULL, 0, NULL);
		pPhone->ezx_blob_send_command("RQVN", NULL, 0, NULL);
		if (pPhone->type->product_id == 0xbeef) {
			pPhone->ezx_blob_dload_program(0x0, (char *)&firstCode, 4);
			if (firstCode == 0xea00c087)
				pPhone->protect_addr = 0x60000;
			else
				pPhone->protect_addr = 0x20000;
		}
	} else
		ui.phoneStatus->setText(tr("Not found!"));
}

void phonetools::on_downLoad_clicked()
{
	// TODO: Add your control notification handler code here

	if(!pPhone)
		this->on_findButton_clicked();
	if(!pPhone)
		return;
	QString saveFileName = QFileDialog::getSaveFileName(this, tr("Select save file"), QString(), tr("Bin files(*.bin)"));
	unsigned int start = ui.startAddr->text().toUInt(0,16);
	unsigned int size = ui.flashSize->text().toUInt(0,16);
	unsigned char *pData;
	QFile flashFile(saveFileName);
	flashFile.open(QIODevice::ReadWrite);
	flashFile.resize(size);
	pData = flashFile.map(0, size);
	pPhone->ezx_blob_dload_program(start, (char *)pData, size);
	flashFile.unmap(pData);
	
}

void phonetools::on_upLoad_clicked()
{
	if(!pPhone)
		this->on_findButton_clicked();
	if(!pPhone)
		return;

	QString saveFileName = QFileDialog::getOpenFileName(this, tr("Select source file"), QString(), "bin");
	unsigned int start = ui.startAddr->text().toUInt(0,16);
	unsigned int size = ui.flashSize->text().toUInt(0,16);
	unsigned char *pData;
	QFile flashFile(saveFileName);
	flashFile.open(QIODevice::ReadOnly);
	if(size > flashFile.size())
		size = flashFile.size();
	pData = flashFile.map(0, size);
	pPhone->ezx_blob_load_program(pPhone->type->product_id, start, (char *)pData, size);
	flashFile.unmap(pData);
}

void phonetools::on_flash_clicked()
{
	if(!pPhone)
		this->on_findButton_clicked();
	if(!pPhone)
		return;

	QString saveFileName = QFileDialog::getOpenFileName(this, tr("Select source file"), QString(), QString());
	unsigned int start = ui.startAddr->text().toUInt(0,16);
	unsigned int size = ui.flashSize->text().toUInt(0,16);

	if(start < pPhone->protect_addr)
		return;
	unsigned char *pData;
	QFile flashFile(saveFileName);
	flashFile.open(QIODevice::ReadOnly);
	if(size > flashFile.size())
		size = flashFile.size();
	pData = flashFile.map(0, size);
	pPhone->ezx_blob_flash_program( start, (const char *)pData, size);
	flashFile.unmap(pData);
	
}

void phonetools::on_findKernel_clicked()
{
	QString saveFileName = QFileDialog::getOpenFileName(this, tr("Select kernel file"), QString());
	ui.kernelImagePathName->setText(saveFileName);
}

void phonetools::on_findRamdisk_clicked()
{
	QString saveFileName = QFileDialog::getOpenFileName(this, tr("Select ramdisk file"), QString());
	ui.kernelImagePathName->setText(saveFileName);
}

void phonetools::on_bootLinux_clicked()
{
	if(!pPhone)
		this->on_findButton_clicked();
	if(!pPhone)
		return;
	pPhone->bootLinux(ui.kernelImagePathName->text().toUtf8().data(),
		ui.machineID->text().toInt(), ui.commandLine->text().toUtf8().data(),
		ui.ramdiskImagePathName->text().toUtf8().data());

}

void phonetools::on_jump_clicked()
{
	if(!pPhone)
		this->on_findButton_clicked();
	if(!pPhone)
		return;
	pPhone->ezx_blob_cmd_jump(ui.startAddr->text().toUInt(0,16));
}
