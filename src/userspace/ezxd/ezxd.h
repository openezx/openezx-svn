/*
 *  ezxd.h - ezxd header file
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#define MAX_PLUGINS	32
#define MAX_CLIENTS	32
#define N_MUX		8

#define CONFIG_PATH "/etc"
#define PLUGINS_PATH "/usr/lib/ezxd"

#define SERIAL_DEVICE "/dev/ttyIPC0"
#define TS0710_LDISC 19

#define MUX(x)		(1 << x)

#define FLAG_TSRAW	MUX(21)

struct sb {
	int fd;
	unsigned int flags;
	char *inp;
	char inb[1024];
	char *outp;
	char outb[1024];
};

struct sb *clients[MAX_CLIENTS];
struct sb *muxes[N_MUX+1];

int sfd;

struct cmd {
	char *str;
	unsigned int flags;
	int (*handler)(unsigned int, char *, struct sb *);
};

struct ezxd_plugin {
	char name[27];
	void *handle;			/* dlopen() handle */
	int (*start)();			/* start() and stop() should NOT */
	int (*stop)();			/* write to mux devices */
	int n_cmds;
	struct cmd cmds[];
};

struct ezxd_plugin *plugins[MAX_PLUGINS];

#ifdef DEBUG
#define dbg(x, args...) fprintf(stderr, x, ## args)
#define error(x) error = x
#else
#define dbg(x, args...)
#define error(x)
#endif
