/*
 * Generic xmit layer for the PXA USB client function
 *
 * This code was loosely inspired by the original version which was
 * Copyright (c) Compaq Computer Corporation, 1998-1999
 * Copyright (c) 2001 by Nicolas Pitre
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * 02-May-2002
 *   Frank Becker (Intrinsyc) - derived from sa1100 usb_send.c
 *
 * TODO: Add support for DMA.
 *
 */

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/arch.h>

#include <linux/errno.h>
#include "pxa_usb.h"
#include "usb_ctl.h"

#if DEBUG
static unsigned int usb_debug = DEBUG;
#else
#define usb_debug 0     /* gcc will remove all the debug code for us */
#endif

static char *ep6_buf;
static int   ep6_len;
static int   ep6_remain;
static usb_callback_t ep6_callback;
static int tx_pktsize;

/* device state is changing, async */
void
ep6_state_change_notify( int new_state )
{
}

/* set feature stall executing, async */
void
ep6_stall( void )
{
	UDCCS6 |= UDCCS_BI_FST;
}

static void
ep6_send_packet(void)
{
	int i;
	char *buf = ep6_buf + ep6_len - ep6_remain;
	int out_size = tx_pktsize;

	//if( usb_debug) printk( "ep6_send_packet: UICR0=%x UDCCS6=%x\n", UICR0, UDCCS6);

	if( out_size > ep6_remain) 
	{
		out_size = ep6_remain;
	}

	for( i=0; i<out_size; i++)
	{
		UDDR6 = *buf++;	
	}

	UDCCS6 = UDCCS_BI_TPC;
	if( out_size < tx_pktsize)
	{
		/* short packet */
		UDCCS6 = UDCCS_BI_TSP;
	}
	ep6_remain -= out_size;

	/*
	if( usb_debug) printk( "ep6_send_packet: "
		"UICR0=%x UDCCS6=%x send bytes=%d left=%d\n", 
		UICR0, UDCCS6, out_size, ep6_remain);
	*/
}

static void
ep6_start(void)
{
	if (!ep6_len)
		return;

        UICR0 &= ~UICR0_IM6;

	ep6_send_packet();
}

static void
ep6_done(int flag)
{
	int size = ep6_len - ep6_remain;
	if (ep6_len) {
		ep6_len = 0;
		if (ep6_callback)
			ep6_callback(flag, size);
	}
}

int
ep6_init(int chn)
{
	/*
	int i;
	desc_t * pdesc = pxa_usb_get_descriptor_ptr();
	for ( i = 0; i < pdesc->ep_num; i++ ) {
		if( BULK_IN2 == ( 0xF & pdesc->ep[i].bEndpointAddress) ) {
			tx_pktsize = __le16_to_cpu( pdesc->ep[i].wMaxPacketSize );
		}
	}
	*/

	/* FIXME */
	tx_pktsize = 64;
	ep6_done(-EAGAIN);
	return 0;
}

void
ep6_reset(void)
{
	/*
	int i;
	desc_t * pdesc = pxa_usb_get_descriptor_ptr();	
	for ( i = 0; i < pdesc->ep_num; i++ ) {
		if( BULK_IN2 == ( 0xF & pdesc->ep[i].bEndpointAddress) ) {
			tx_pktsize = __le16_to_cpu( pdesc->ep[i].wMaxPacketSize );
		}
	}
	*/

	/* FIXME */
	tx_pktsize = 64;

	UDCCS6 &= ~UDCCS_BI_FST;
	ep6_done(-EINTR);
}

void
ep6_int_hndlr(int usir0)
{
	int status = UDCCS6;


	if (ep6_remain != 0) {
		/* more data to go */
		ep6_start();
	} else {
		if( status & UDCCS_BI_TPC)
		{
			UDCCS6 = UDCCS_BI_TPC;
		}
		ep6_done(0);
	}
}

int
ep6_send(char *buf, int len, usb_callback_t callback)
{
	int i=0;
	int flags;

	/*
	if( usb_debug) printk( "pxa_usb_send: "
		"data len=%d state=%d blen=%d\n", 
		len, usbd_info.state, ep6_len);
	*/

	if (usbd_info.state != USB_STATE_CONFIGURED)
		return -ENODEV;

	if (ep6_len) {
		SerialOutputString("busy...\n");
		return -EBUSY;
	}

	/*
	for ( i=0; i < len; i++) {
		SerialOutputHex(buf[i]);
		SerialOutputString(" ");
	}

	SerialOutputString("\n");
	*/

	//local_irq_save(flags);
	ep6_buf = buf;
	ep6_len = len;
	ep6_callback = callback;
	ep6_remain = len;
	ep6_start();
	//local_irq_restore(flags);

	return 0;
}

int 
ep6_xmitter_avail( void )
{
	if (usbd_info.state != USB_STATE_CONFIGURED)
		return -ENODEV;
	if (ep6_len)
		return -EBUSY;
	return 0;
}
