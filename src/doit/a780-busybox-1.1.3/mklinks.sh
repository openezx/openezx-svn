#!/bin/bash

shopt -s xpg_echo

die() {
	echo "\nERROR: $*\n"
	exit 1
}

if [ $# -ne 2 ]; then
	echo "Usage: mklinks.sh busybox_location out_dir"
	echo "You should probbly cd to the out_dir first"
fi

target=$1
destDir=$2

if [ ! -e "${destDir}" ]; then
	echo "${destDir} does not exist.\nCreate it? (Y/N) \c"
	read
	if [ "${REPLY:0:1}" = "y" -o "${REPLY:0:1}" = "Y" ]; then
		mkdir -p "${destDir}" || die "Failed to create ${destDir}."
	else
		exit 1
	fi
else
	if [ ! -d "${destDir}" ]; then
		die "${destDir} exists but is not a directory!"
	fi
fi

cd "${destDir}" || die "Can't enter ${destDir}"

if [ ! -x "${target}" ]; then
	echo "${target} doesn't exist or is not an executable file.  This path should be relative to the out_dir or absolute."
	exit 1
fi

for a in $(${target} --help | grep ',' | sed 's/,//g'); do
	ln -s ${target} $a
done