#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <asm/io.h>
#include <asm/byteorder.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/cfi.h>
#include <linux/mtd/compatmac.h>
#include <linux/mtd/cfi_cmdset_0001_mp.h>
#include <linux/proc_fs.h>

#ifdef CONFIG_MTD_CACHED_READS
#error "Should not have cached mapping for flash!!"
#endif

#define CONFIG_L18_DEBUG

// **
// ** stolen from mtdpart.c
// **
/* Our partition node structure */
struct mtd_part {
	struct mtd_info mtd;
	struct mtd_info *master;
	u_int32_t offset;
	int index;
	struct list_head list;
	int registered;
};

/*
 * Given a pointer to the MTD object in the mtd_part structure, we can retrieve
 * the pointer to that structure with this macro.
 */
#define PART(x)  ((struct mtd_part *)(x))

// **
// **  End of stolen code
// **

static struct mtd_info *ezx_mymtd; // L18 flash mtd device
static void (*old_resume) (struct mtd_info *mtd); // original resume
static int (*old_suspend) (struct mtd_info *mtd); // original suspend

static int fake_cfi_intelext_suspend(struct mtd_info *mtd);
static void fake_cfi_intelext_resume(struct mtd_info *mtd);

static int unlocked = 0;
static struct map_info *bulverde_map;

static int my_suspend (struct mtd_info *mtd) {
	printk(KERN_INFO "my_suspend!!\n");
	int ret = fake_cfi_intelext_suspend(mtd);
	printk(KERN_INFO "fake_cfi_intelext_suspend returns %d\n", ret);
	return ret;
}

static void my_resume (struct mtd_info *mtd) {
	printk(KERN_INFO "my_resume!!\n");
	fake_cfi_intelext_resume(mtd);
}

#ifdef	CONFIG_PROC_FS
static struct proc_dir_entry *rwflash_proc_entry;
static int rwflash_read_proc(char *, char **, off_t, int, int *, void *);
#endif

// We create our own writable partitions
#define NB_OF(x)  (sizeof(x)/sizeof(x[0]))
static struct mtd_partition extra_partitions[] = {
	//{ offset: 0x00000000, size: 0x00020000, name: "R/W Bootloader" },
	{ offset: 0x00020000, size: 0x000E0000, name: "R/W Kernel" },
	//{ offset: 0x00100000, size: 0x00020000, name: "R/W Unknown Gap 1" },
	{ offset: 0x00120000, size: 0x018E0000, name: "R/W RootFS" },
	//{ offset: 0x01A00000, size: 0x00580000, name: "R/W VFM" },
	//{ offset: 0x01F80000, size: 0x00020000, name: "R/W Unknown Gap 2" },
	{ offset: 0x01FA0000, size: 0x00020000, name: "R/W Setup" },
	{ offset: 0x01FC0000, size: 0x00020000, name: "R/W Logo" },
	//{ offset: 0x01FE0000, size: 0x00020000, name: "Unknown Gap 3" },
};

static int __init init_rwflash(void) {
	int ret;
	struct mtd_part *part0 = PART(__get_mtd_device(NULL, 0)); // get first mtd partition
	ezx_mymtd = part0->master; // from partition get master mtd device
	bulverde_map = ezx_mymtd->priv;

	printk(KERN_INFO "RW Flash loaded\n");

	// check if config is as expected (flash is 32mb)
	if(ezx_mymtd->size != 32 * 1024 * 1024) {
		printk(KERN_ERR "Found %s at 0x%x size:%d. It should not be like this!!\n", ezx_mymtd->name, (unsigned int)ezx_mymtd, ezx_mymtd->size);
		return 1;
	} else {
		printk(KERN_INFO "Found %s at 0x%x size:%d\n", ezx_mymtd->name, (unsigned int)ezx_mymtd, ezx_mymtd->size);
	}

	old_suspend = ezx_mymtd->suspend;
	old_resume = ezx_mymtd->resume;
	ezx_mymtd->suspend = NULL;
	ezx_mymtd->resume = NULL;

	// add extra r/w partitions
	ret = add_mtd_partitions(ezx_mymtd, extra_partitions, NB_OF(extra_partitions));
	if(ret == 0) {
		printk(KERN_INFO "Added extra_partitions.\n");
	} else {
		printk(KERN_INFO "Error during partition add. add_mtd_partitions returned %d\n", ret);
	}

	// hijack cfi_intelext_resume/cfi_intelext_suspend from cfi_cmdset_0001_mp.c to our own versions
	ezx_mymtd->suspend = my_suspend;
	ezx_mymtd->resume = my_resume;

	//add_mtd_device(ezx_mymtd);

	printk(KERN_INFO "old suspend fn: 0x%x. suspend fn has been hijacked.\n", (unsigned int)old_suspend);
	printk(KERN_INFO "old resume fn: 0x%x. resume fn has been hijacked.\n", (unsigned int)old_resume);
	printk(KERN_INFO "Now, wait for flash to be unlocked when system suspends.\n");

#ifdef	CONFIG_PROC_FS
	if ((rwflash_proc_entry = create_proc_entry("rwflash_status", 0444, 0))) {
		rwflash_proc_entry->read_proc = rwflash_read_proc;
	}
#endif

	return 0;
}

static void __exit exit_rwflash(void) {
	ezx_mymtd->resume = old_resume;
	ezx_mymtd->suspend = old_suspend;
	printk(KERN_INFO "unload rwflash. Restoring original suspend/resume\n");

}

#ifdef CONFIG_PROC_FS
static int rwflash_read_proc(char *page, char **start, off_t offset, int len, int *eof, void *data) {
	if(unlocked) {
		return sprintf(page, "unlocked\n");
	} else {
		return sprintf(page, "locked\n");
	}
}
#endif

// **
// **  Stolen and modified from cfi_cmdset_0001_mp.c
// **

// cyph: This is the juicy part here we define which part of flash to make R/W
#define L18_RW_REGION_START 0x00020000  // we don't want to fuck up the bootloader, so keep it RO
#define L18_RW_REGION_END   0x02000000  // till the end of flash memory

static int msc0_val, sxcnfg_val;
static int mode = 0;

#define TIME_OUT   1000000L
// cyph: we create our own pm_flash_count here, but we are just _hoping_ pm_flash_count is 0
#ifdef CONFIG_PM
static struct pm_flash_s
{
	struct pm_dev *pm_dev;
	atomic_t pm_flash_count;
}pm_flash;
#endif

int cfi_intelext_unlockdown_L18(struct mtd_info *mymtd)
{
	struct map_info *map;
	unsigned short i, j;
	struct cfi_private *cfi;
	unsigned short npartitions = 0;
	struct flpartition *this_partition = NULL;
	struct flchip *this_chip = NULL;
	struct flprivate *this_flprivate = NULL;
	
	__u32 timeout = TIME_OUT;
	__u32 flash_status;
	__u32 OK_status = CMD(0x80);

//Susan for pm
#ifdef CONFIG_PM
	atomic_inc(&(pm_flash.pm_flash_count));
#endif

	map = mymtd->priv;
	cfi = (struct cfi_private *)(map->fldrv_priv);

	#ifdef CONFIG_L18_DEBUG
	printk("Enter into <cfi_intelext_unlockdown_L18>\n");
	#endif

	cfi_write(map, CMD(0x70), 0);
	flash_status = cfi_read(map, 0);
	cfi_write(map, CMD(0xff), 0);
	
	if ( (flash_status & CMD(0x80)) != OK_status )
	{
	#ifdef CONFIG_L18_DEBUG
	printk("cfi_intelext_unlockdown_L18: initialization status wrong\n");
	#endif
		cfi_write(map, CMD(0x50), 0);  /* Clear status register */
#ifdef CONFIG_PM
		atomic_dec(&(pm_flash.pm_flash_count));
#endif
		return -EINVAL;
	}

	for ( i = 0; i < mymtd->numeraseregions; i ++ )
	{
		int j;
		unsigned short ebnums;
		unsigned long the_ebsize = 0;
		unsigned long the_offset = 0;

		ebnums = mymtd->eraseregions[i].numblocks;
		the_ebsize = mymtd->eraseregions[i].erasesize;
		the_offset = mymtd->eraseregions[i].offset;
		
		for (j = 0; j < ebnums; j ++)
		{
			if ( (the_offset >= L18_RW_REGION_START) && (the_offset < L18_RW_REGION_END) )
			{
				/* unlock it */
				cfi_write(map, CMD(0x60), the_offset);
				cfi_write(map, CMD(0xD0), the_offset);

				while ( -- timeout )
				{
					cfi_write(map, CMD(0x70), the_offset);
					flash_status = cfi_read(map, the_offset);
					if ( (flash_status & CMD(0x80)) == OK_status )
						break;
				}

				cfi_write(map, CMD(0xff), the_offset);  /* Force flash returns to read-array mode */
				if (!timeout)
				{
					#ifdef CONFIG_L18_DEBUG
					printk("cfi_intelext_unlockdown_L18: unlock failed at %x\n", the_offset);
					#endif
					cfi_write(map, CMD(0x50), the_offset);  /* Clear status register before we leave here*/
#ifdef CONFIG_PM
					atomic_dec(&(pm_flash.pm_flash_count));
#endif
					return -EINVAL;
				}
				#ifdef CONFIG_L18_DEBUG
				printk("cfi_intelext_unlockdown_L18: unlock suceed at %x\n", the_offset);
				#endif
			}
			else
			{
				/* lock down it */
				cfi_write(map, CMD(0x60), the_offset);
				cfi_write(map, CMD(0x2F), the_offset);

				while ( -- timeout )
				{
					cfi_write(map, CMD(0x70), the_offset);
					flash_status = cfi_read(map, the_offset);
					if ( (flash_status & CMD(0x80)) == OK_status )
						break;
				}

				cfi_write(map, CMD(0xff), the_offset);  /* Force flash returns to read-array mode */
				if (!timeout)
				{
					#ifdef CONFIG_L18_DEBUG
					printk("cfi_intelext_unlockdown_L18: lockdown failed at %x\n", the_offset);
					#endif
					cfi_write(map, CMD(0x50), the_offset);  /* Clear status register before we leave here*/
#ifdef CONFIG_PM
					atomic_dec(&(pm_flash.pm_flash_count));
#endif
					return -EINVAL;
				}
				#ifdef CONFIG_L18_DEBUG
				printk("cfi_intelext_unlockdown_L18: lockdown succeed at %x\n", the_offset);
				#endif
			}
			the_offset += the_ebsize;
			timeout = TIME_OUT;
		}

		cfi_write(map, CMD(0x50), (the_offset - the_ebsize));  /* clear status register when we leave this region */
		cfi_write(map, CMD(0xFF), (the_offset - the_ebsize));  /* Make sure the related partition to be in read-array mode */
	}

	/* Resume each partition's state to be the same as before entering sleep mode */
	this_chip = cfi->chips;
	this_flprivate = (struct flprivate *)(this_chip->priv);
	npartitions = this_flprivate->num_partitions;

	for ( i = 0 ; i < npartitions ; i ++ )
	{
		this_partition = (struct flpartition *)(&(this_flprivate->partitions[i]));
//the old method		if (this_partition->state == FL_STATUS)
//the old method			cfi_write(map, CMD(0x70), (this_partition->offset));
		this_partition->state = FL_READY;
		// For other case except for FL_READY, there must be something error somehow, ignore it at this point //
	}
	
#ifdef CONFIG_PM
	atomic_dec(&(pm_flash.pm_flash_count));
#endif
	unlocked = 1;
	printk(KERN_INFO "****************************************\n");
	printk(KERN_INFO "** !!!!! YOUR FLASH IS UNLOCKED !!!!! **\n");
	printk(KERN_INFO "****************************************\n");

	return 0;
}

static int fake_cfi_intelext_suspend(struct mtd_info *mtd)
{
#if (0)
	struct map_info *map = mtd->priv;
	struct cfi_private *cfi = map->fldrv_priv;
	int i, j;
	struct flchip *chip;
	struct flprivate *priv;
	struct flpartition *partition;
	int ret = 0;
#endif
	int flags;
	int status = 0;
	struct map_info *map = bulverde_map;
	struct cfi_private *cfi = map->fldrv_priv;
	
	/* return flash status indicating sleep available or inavailable */
	status = atomic_read(&(pm_flash.pm_flash_count));
	printk(KERN_NOTICE "flash device refer-count(%d) when try to sleep\n",status);
	if (!status) 
	{
		printk("switch to async mode\n");

		local_irq_save(flags);

		/* zxf -- Disable MMU cache and buffer */
		__asm__ __volatile__("mrc	p15, 0, r0, c1, c0, 0\n\
			bic	r0, r0, #0x000c\n\
			b	1f			\n\
			.align	5			\n\
	1:		mcr	p15, 0, r0, c1, c0, 0":::"r0","memory");
				
		msc0_val = MSC0;
		sxcnfg_val = SXCNFG;

//This is for A760			MSC0 = 0x12bb12bb;
//This is for A760				cfi_write(map, CMD(0x60), 0x0001fffe);
//This is for A760				cfi_write(map, CMD(0x03), 0x0001fffe);
//This is for A760				cfi_write(map, CMD(0x60), 0x0101fffe);
//This is for A760				cfi_write(map, CMD(0x03), 0x0101fffe);
		MSC0 = 0x128b128b;
		cfi_write(map, CMD(0x60), 0x00017f9e);
		cfi_write(map, CMD(0x03), 0x00017f9e);

		SXCNFG = 0;
		MDREFR &= ~(MDREFR_K0DB2 | MDREFR_K0RUN | MDREFR_E0PIN);

//no necessary			cfi_write(map, CMD(0xff), 0x0);
//no necessary			cfi_write(map, CMD(0xff), 0x01000000);

		/* zxf -- Enable MMU cache and buffer */
		__asm__ __volatile__("mrc	p15, 0, r0, c1, c0, 0\n\
			orr	r0, r0, #0x000c\n\
			b	2f			\n\
			.align 5			\n\
	2:		mcr	p15, 0, r0, c1, c0, 0":::"r0","memory");			

		local_irq_restore(flags);
		mode = 1;
	}
	
	return status;

#if (0)  //Susan -- use cfi_intelext_pm_ezx suspend function
	for (i=0; !ret && i<cfi->numchips; i++) {
		chip = &cfi->chips[i];
		priv = (struct flprivate*) chip->priv;

		for (j=0; j<priv->num_partitions; j++) {
			partition = priv->partitions + j;
			spin_lock_bh(partition->mutex);

			switch(partition->state) {
			case FL_READY:
			case FL_STATUS:
			case FL_CFI_QUERY:
			case FL_JEDEC_QUERY:
				partition->oldstate = partition->state;
				partition->state = FL_PM_SUSPENDED;
				/* No need to wake_up() on this state change - 
				 * as the whole point is that nobody can do anything
				 * with the chip now anyway.
				 */
			case FL_PM_SUSPENDED:
				break;

			default:
				ret = -EAGAIN;
				break;
			}
			spin_unlock_bh(partition->mutex);
		}
	}

	if (!ret) goto done;

	/* Unlock the chips again */
	for (i--; i >=0; i--) {
		chip = &cfi->chips[i];
		priv = (struct flprivate*) chip->priv;
			
		for (j=0; j<priv->num_partitions; j++) {
			partition = priv->partitions + j;

			spin_lock_bh(partition->mutex);
			
			if (partition->state == FL_PM_SUSPENDED) {
				/* No need to force it into a known state here,
				   because we're returning failure, and it didn't
				   get power cycled */
				partition->state = partition->oldstate;
				wake_up(&partition->wq);
			}
			spin_unlock_bh(partition->mutex);
		}
	}
	
done:

	return ret;
#endif
}

static void fake_cfi_intelext_resume(struct mtd_info *mtd)
{
#if (0)
	struct map_info *map = mtd->priv;
	struct cfi_private *cfi = map->fldrv_priv;
	int i,j;
	struct flchip *chip;
	struct flprivate *priv;
	struct flpartition *partition;
#endif
#ifdef CONFIG_ARCH_MAINSTONE
//ss	extern void bulverde_mtd_unlock_all(void);
#endif

	int flags;
	int status = 0;
	struct map_info *map = bulverde_map;
	struct cfi_private *cfi = map->fldrv_priv;

	if (mode) 
	{
		printk("switch to sync mode\n");

		local_irq_save(flags);

		/* zxf -- Disable MMU cache and buffer */
		__asm__ __volatile__("mrc	p15, 0, r0, c1, c0, 0\n\
			bic	r0, r0, #0x000c\n\
			b	1f			\n\
			.align	5			\n\
	1:		mcr	p15, 0, r0, c1, c0, 0":::"r0","memory");

		MSC0 = msc0_val;
		MDREFR |= (MDREFR_K0DB2 | MDREFR_K0RUN | MDREFR_E0PIN);
			
		cfi_write(map, CMD(0x60), 0x00004984);
		cfi_write(map, CMD(0x03), 0x00004984);

		SXCNFG = sxcnfg_val;
	
		printk(KERN_NOTICE "Unlock after reset -- begin(%ld)\n", jiffies);
		status = cfi_intelext_unlockdown_L18(ezx_mymtd);
		printk(KERN_NOTICE "Unlock after reset -- end(%ld)\n", jiffies);
	
		if (status)
			printk(KERN_NOTICE "Unlock flash fail after reset flash.\n");
				
		/* zxf -- Enable MMU cache and buffer */
		__asm__ __volatile__("mrc	p15, 0, r0, c1, c0, 0\n\
			orr	r0, r0, #0x000c\n\
			b	2f			\n\
			.align	5			\n\
	2:		mcr	p15, 0, r0, c1, c0, 0":::"r0","memory");
	
		local_irq_restore(flags);
		mode = 0;
	}
	
#if (0)  //Susan -- use cfi_intelext_pm_ezx resume function which contains lockdown/unlock
	for (i=0; i<cfi->numchips; i++) {
	
		chip = &cfi->chips[i];
		priv = (struct flprivate*) chip->priv;

		for (j=0; j<priv->num_partitions; j++) {
			partition = priv->partitions + j;

			spin_lock_bh(partition->mutex);
		
			/* Go to known state. Chip may have been power cycled */
			if (partition->state == FL_PM_SUSPENDED) {
				// cfi_write(map, CMD(0xFF), 0);  //Susan -- is it an issue??
				cfi_write(map, CMD(0xFF), chip->start + partition->offset );
				partition->state = FL_READY;
				wake_up(&partition->wq);
			}

			spin_unlock_bh(partition->mutex);
		}
	}
#endif

#ifdef CONFIG_ARCH_MAINSTONE
//ss	bulverde_mtd_unlock_all();
#endif
		
	return status;

}

// **
// ** End of stolen code
// **


MODULE_DESCRIPTION("RW Flash");
MODULE_LICENSE("GPL");

module_init(init_rwflash);
module_exit(exit_rwflash);
