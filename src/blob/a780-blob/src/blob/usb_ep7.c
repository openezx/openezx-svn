/*
 * Generic receive layer for the PXA USB client function
 *
 * This code was loosely inspired by the original version which was
 * Copyright (c) Compaq Computer Corporation, 1998-1999
 * Copyright (c) 2001 by Nicolas Pitre
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * 02-May-2002
 *   Frank Becker (Intrinsyc) - derived from sa1100 usb_recv.c
 * 
 * TODO: Add support for DMA.
 *
 */

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/arch.h>

#include <linux/errno.h>
#include "pxa_usb.h"
#include "usb_ctl.h"

#if DEBUG
static unsigned int usb_debug = DEBUG;
#else
#define usb_debug 0     /* gcc will remove all the debug code for us */
#endif

static char *ep7_buf;
static int   ep7_len;
static int   ep7_remain;
static usb_callback_t ep7_callback;
static int rx_pktsize;

static void
ep7_start(void)
{
	/* disable DMA */
	UDCCS7 &= ~UDCCS_BO_DME;

	/* enable interrupts for endpoint 7 (bulk out) */
        UICR0 &= ~UICR0_IM7;
}

static void
ep7_done(int flag)
{
	int size = ep7_len - ep7_remain;

	if (!ep7_len)
		return;
	//SerialOutputHex("done\n");

	ep7_len = 0;
	if (ep7_callback) {
		ep7_callback(flag, size);
	}
}

void
ep7_state_change_notify( int new_state )
{
}

void
ep7_stall( void )
{
	/* SET_FEATURE force stall at UDC */
	UDCCS7 |= UDCCS_BO_FST;
}

int
ep7_init(int chn)
{
	/*
	int i;
	desc_t * pdesc = pxa_usb_get_descriptor_ptr();

	for ( i = 0; i < pdesc->ep_num; i++ ) {
		if( BULK_OUT1 == ( 0xF & pdesc->ep[i].bEndpointAddress) ) {
			rx_pktsize = __le16_to_cpu( pdesc->ep[i].wMaxPacketSize );
		}
	}
	*/

	/* FIXME */
	rx_pktsize = 64;
	ep7_done(-EAGAIN);
	return 0;
}

void
ep7_reset(void)
{
	/*
	int i;
	desc_t * pdesc = pxa_usb_get_descriptor_ptr();

	for ( i = 0; i < pdesc->ep_num; i++ ) {
		if( BULK_OUT1 == ( 0xF & pdesc->ep[i].bEndpointAddress) ) {
			rx_pktsize = __le16_to_cpu( pdesc->ep[i].wMaxPacketSize );
		}
	}
	*/
	/* FIXME */
	rx_pktsize = 64;
	UDCCS7 &= ~UDCCS_BO_FST;
	ep7_done(-EINTR);
}

void
ep7_int_hndlr(int udcsr)
{
	int status = UDCCS7;
	//if( usb_debug) printk("ep7_int_hndlr: UDCCS7=%x\n", status);

	if( (status & (UDCCS_BO_RNE | UDCCS_BO_RSP)) == UDCCS_BO_RSP)
	{
		/* zero-length packet */
	}

	if( status & UDCCS_BO_RNE)
	{
		int len;
		int i;
		char *buf = ep7_buf + ep7_len - ep7_remain;

		/* bytes in FIFO */
		len = (UBCR7 & 0xff) +1;
		
		/*
		if( usb_debug) printk("usb_recv: "
			"len=%d out1_len=%d out1_remain=%d\n",
			len,ep7_len,ep7_remain);
		*/

		if( len > ep7_remain)
		{
			/* FIXME: if this happens, we need a temporary overflow buffer */
			//printk("usb_recv: Buffer overwrite warning...\n");
			len = ep7_remain;
		}

		/* read data out of fifo */
		for( i=0; i<len; i++)
		{
			*buf++ = UDDR7 & 0xff;
		}

		ep7_remain -= len;
		ep7_done((len) ? 0 : -EPIPE);
	}

	/* ack RPC - FIXME: '|=' we may ack SST here, too */
	UDCCS7 |= UDCCS_BO_RPC;

	return;
}

int
ep7_recv(char *buf, int len, usb_callback_t callback)
{
	int flags;

	if (ep7_len)
		return -EBUSY;

	ep7_buf = buf;
	ep7_len = len;
	ep7_callback = callback;
	ep7_remain = len;
	ep7_start();

	return 0;
}

