#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned int getsize(unsigned int addr)
{
    char s[256];
    sprintf(s,"%08x.bin",addr);
    FILE *fp=fopen(s,"r");
    fseek(fp,0,SEEK_END);
    int res=ftell(fp);
    fclose(fp);
    return res;
}

int getcginfo(unsigned int addr, int *num, int *end, int verb)
{
    FILE *fp=fopen("cg.dat","r");
    if (!fp)
    {
	fp=fopen("fixloader/cg.dat","r");
	if (fp==0)
	{
	    printf("cg.dat not found in current directory and in fixloader subdir\n");
	    exit(1);
	}
    }
    int n,st,en;
    while (fscanf(fp,"%d %x %x",&n,&st,&en)==3)
    {
//	printf("try [%d %08x %08x]\n",n,st,en);
	if (st==addr)
	{
	    *num=n;
	    *end=en;
	    fclose(fp);
	    return 1;
	}
    }
    fclose(fp);
    if (verb)
        printf("CG at %08x not found. Possible LTE CG. Ignored\n",addr);
    return 0;
}

int main(void)
{
    FILE *flist;
    int addr;
    int aa;
    int cgcnt=0;
    int _cnum, _cstart, _cend, _cend2;

    flist = fopen("list.txt","r");
    if(flist == NULL)
    {
	    perror("list.txt");
	    exit(1);
    }
	
    
    while (fscanf(flist,"%x",&aa)==1)
    {
	cgcnt++;
    }

    cgcnt--; // Don't erase loader
    printf("Will erase %d CG's\n", cgcnt);
    fclose(flist);
    
    flist=fopen("list.txt","r");
    fscanf(flist,"%x",&aa); // Read loader
    printf("Boot loader: %08x\n",aa);
    int cgidx; 
    int realCnt=0;

    FILE *fp=fopen("a0200000.bin","r+");
    unsigned char buf[4];    

    // Writing CG count
    fseek(fp,0x3C000,SEEK_SET);
    memset(buf,0,4);
    fwrite(&cgcnt,4,1,fp);    

    FILE *flist_ezx=fopen("list_ezx.txt","w");
    fprintf(flist_ezx,"%08x\n",aa);

    for ( cgidx=0; cgidx < cgcnt; cgidx++)
    {
        fscanf(flist,"%x",&_cstart);
	if (!getcginfo(_cstart,&_cnum, &_cend,1)) continue;
	fprintf(flist_ezx,"%08x\n",_cstart);
	realCnt++;
	fwrite(&_cnum,4,1,fp);
	fwrite(&_cstart,4,1,fp);
	_cend2=_cstart+getsize(_cstart)-1;
	fwrite(&_cend2,4,1,fp);
	fwrite(&_cstart,4,1,fp);	
	fwrite(&_cend,4,1,fp);	
	printf("%02d %08x %08x %08x %08x\n",_cnum, _cstart, _cend2, _cstart, _cend);
	if (_cend2>_cend)
	{
	    printf("^^^^^^^^^^^^^^^^ CG is too large!!!\n");
	    exit(0);
	}
    }

    fseek(fp,0x3C000,SEEK_SET);
    memset(buf,0,4);
    fwrite(&realCnt,4,1,fp);

    fclose(fp);
    fclose(flist_ezx);
    return 0;	
}
