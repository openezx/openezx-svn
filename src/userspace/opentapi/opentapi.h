#ifndef _OPENTAPI_H_
#define _OPENTAPI_H_

/* opentapi - minimal implementation of tapisrv
 *
 * (C) 2006 by Daniel Willmann <daniel@totalueberwachung.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#define LOGFILENAME "/tmp/opentapi.log"
#define MUX_BASE "/dev/mux"

#define MUX_FILES_MAX 8
#define MUX_BUFFER_SIZE 1024

#define PORTBASE 7100

struct mux_buf {
	char buffer[MUX_BUFFER_SIZE];
	int length;
};

#endif
