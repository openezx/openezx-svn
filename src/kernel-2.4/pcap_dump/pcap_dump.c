/*
 * Small tool to dump pcap registers on kernel 2.4
 *
 *	Copyright(c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 
 * as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * I created this tool as a simple module to dump pcap registers on the
 * original 2.4 kernel, and only then i realized that ssp_pcap_main.c does not
 * export SSP_PCAP_read_data_from_PCAP() function... I am too lazy to
 * implement full ssp comunication, so this module will only work with a
 * modified kernel, sorry.
 *
 * NOTE: on 2nd gen phones we use  power_ic_read_reg() which is exported, so
 * you can use the module also with the kernel from the original firmware.
 */

#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>

static struct proc_dir_entry *proc_pcap;

#ifdef EZX2ndGEN
extern int power_ic_read_reg(u_int8_t, u_int32_t *);
int (*pcap_read)(u_int8_t, u_int32_t *) = power_ic_read_reg;
#else
extern int SSP_PCAP_read_data_from_PCAP(u_int8_t, u_int32_t *);
int (*pcap_read)(u_int8_t, u_int32_t *) = SSP_PCAP_read_data_from_PCAP;
#warning "Make sure SSP_PCAP_read_data_from_PCAP is exported!"
#endif

char *pcap_registers[] = {
	"ISR\t", "MSR\t", "PSTAT\t", "INT_SEL\t", "SWCTRL\t", "VREG1\t",
	"VREG2\t", "AUXVREG\t", "BATT_DAC", "ADC1\t", "ADC2\t", "AUD_CODEC",
	"RX_AUD_AMPS", "ST_DAC\t", "RTC_TOD\t", "RTC_TODA", "RTC_DAY\t",
	"RTC_DAYA", "MTRTMR\t", "PWRCTRL\t", "BUSCTRL\t", "PERIPH\t",
	"AUXVREG_MASK", "VENDOR_REV", "LOWPWR_CTRL", "PERIPH_MASK",
	"TX_AUD_AMPS", "GP\t",
	NULL, NULL, NULL, NULL
};

static int pcap_read_proc(char *page, char **start, off_t off, int count,
				int *eof, void *data_unused)
{
	int len = 0;
	u_int8_t r;
	u_int32_t v;
	off_t begin = 0;
	
	for(r=0;r<32;r++) {
		if (pcap_registers[r] == NULL)
			continue;
		pcap_read(r, &v);
		len += sprintf(page+len, "%s\t%08X\n", pcap_registers[r], v);
		if(len + begin > off + count)
			goto done;
		if(len + begin < off) {
			begin += len;
			len = 0;
		}
	}
	*eof = 1;
done:
	if (off >= len+begin)
		return 0;
	*start = page + (off-begin);
	return ((count < begin+len-off) ? count : begin+len-off);
}

static int __init pcap_dump_init (void)
{
	if((proc_pcap = create_proc_entry("pcap", 0, NULL)))
		proc_pcap->read_proc = pcap_read_proc;
	
	return 0;
}

static void __exit pcap_dump_exit (void)
{
	if(proc_pcap)
		remove_proc_entry("pcap", NULL);
}

MODULE_AUTHOR ("Daniel Ribeiro <drwyrm@gmail.com>");
MODULE_DESCRIPTION ("PCAP regiter dump proc entry");
MODULE_LICENSE ("GPL");
EXPORT_NO_SYMBOLS;

module_init (pcap_dump_init);
module_exit (pcap_dump_exit);

