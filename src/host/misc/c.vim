" adapted from: http://www.jukie.net/~bart/conf/vim/c.vim

" Setup tabstops to 8 and indents to 8
setlocal tabstop=8
setlocal shiftwidth=8
setlocal noexpandtab

"setlocal textwidth=78        " Set the line wrap length

" Some style errors
if has("gui_running")
  hi  BadStyle           gui=NONE    guifg=#ffffff   guibg=#6e2e2e
else
  hi  BadStyle           cterm=NONE  ctermfg=15      ctermbg=red
endif

" regexp black magic to warn about CodingStyle mistakes

match BadStyle	/.\%>81v/
syntax match BadStyle	/\(\<\(if\|switch\|case\|for\|do\|while\)\>\((\|[[:tab:]]\)\)/
syntax match BadStyle	/\(\(\_^\s*\|#define.*\|\<if\>\|\<switch\>\|\<case\>\|\<for\>\|\<do\>\|\<while\>\|\<return\>\|:\|=\|&\||\|-\|+\|)\|?\|<\|>\|,\)\@<![[:blank:]]\+(\(.*)[[:blank:]]*(.*\)\@!\)/

syntax match BadStyle	/\<\(if\>\|else\>\).*;/
syntax match BadStyle	/\/\//
syntax match BadStyle	/\s\+$/
syntax match BadStyle	/^ \+/
syntax match BadStyle	/	[ ]\+	\+/
syntax match BadStyle	/,\S/
syntax match BadStyle	/\s,/

syntax match BadStyle	/[^\+([:blank:]][+][^\+=[:blank:]]/
syntax match BadStyle	/[^\+[:blank:]][+][[:blank:]]/

syntax match BadStyle	/[^\-([:blank:]]-[^\->=[:blank:]]/
syntax match BadStyle	/[^\-[:blank:]]-[[:blank:]]/

syntax match BadStyle	/\(--\|[+][+]\)[[:blank:]]\+[^[:blank:]]/
syntax match BadStyle	/[^\+\-=<>\*\/~\^|&%\![:blank:]][[:blank:]]\+\(--\|[+][+]\)/

syntax match BadStyle	/[^\*\/([:blank:]]\*[^\*=\/[:blank:]]/
syntax match BadStyle	/[^\*\/([:blank:]]\*[[:blank:]]/

syntax match BadStyle	/[^\*[:blank:]]\/[^=\*[:blank:]]/
syntax match BadStyle	/[^\*[:blank:]]\/[[:blank:]]/
syntax match BadStyle	/[[:blank:]]\/[^=\*[:blank:]]/

syntax match BadStyle	/[^([:blank:]]\(%\|\^\|\~\)[^=[:blank:]]/
syntax match BadStyle	/[^([:blank:]]\(%\|\^\|\~\)[[:blank:]]/
syntax match BadStyle	/[[:blank:]]\(%\|\^\)[^=[:blank:]]/

syntax match BadStyle	/[[:blank:]]\~[[:blank:]]/

syntax match BadStyle	/[^[:blank:]]?/
syntax match BadStyle	/?[^[:blank:]]/
syntax match BadStyle	/:[^[:blank:]]/

syntax match BadStyle	/[^&([:blank:]]&[^=&[:blank:]]/
syntax match BadStyle	/[^&([:blank:]]&[[:blank:]]/

syntax match BadStyle	/[^|[:blank:]]|[^=|[:blank:]]/
syntax match BadStyle	/[^|[:blank:]]|[[:blank:]]/
syntax match BadStyle	/[[:blank:]]|[^=|[:blank:]]/

syntax match BadStyle	/[^[:blank:]]\(==\|[+]=\|-=\|||\|&&\|\!=\||=\|\*=\|\/=\|%=\|&=\|\^=\|\~=\)[^[:blank:]]/
syntax match BadStyle	/[^[:blank:]]\(==\|[+]=\|-=\|||\|&&\|\!=\||=\|\*=\|\/=\|%=\|&=\|\^=\|\~=\|->\|\.\)[[:blank:]]/
syntax match BadStyle	/[[:blank:]]\(==\|[+]=\|-=\|||\|&&\|\!=\||=\|\*=\|\/=\|%=\|&=\|\^=\|\~=\|->\)[^[:blank:]]/
syntax match BadStyle	/[^[:blank:]][[:blank:]]\+\.[^[:blank:]]/

syntax match BadStyle	/[^\+\-=<>\*\/~\^|&%\![:blank:]]=[^\+\-=<>\*\/~\^|&%\![:blank:]]/
syntax match BadStyle	/[^\+\-=<>\*\/~\^|&%\![:blank:]]=[[:blank:]]/
syntax match BadStyle	/[[:blank:]]=[^\+\-=<>\*\/~\^|&\![:blank:]]/

syntax match BadStyle	/[^\->[:blank:]]>[^>=[:blank:]]/
syntax match BadStyle	/[^>[:blank:]]>[[:blank:]]/
syntax match BadStyle	/[[:blank:]]>[^>=[:blank:]]/
syntax match BadStyle	/[^>[:blank:]]>=[[:blank:]]/
syntax match BadStyle	/[[:blank:]]>=[^>[:blank:]]/
syntax match BadStyle	/[^[:blank:]]>=[^[:blank:]]/

syntax match BadStyle	/[^<[:blank:]]<[^<=[:blank:]]/
syntax match BadStyle	/[^<[:blank:]]<[[:blank:]]/
syntax match BadStyle	/[[:blank:]]<[^<=[:blank:]]/
syntax match BadStyle	/[^<[:blank:]]<=[[:blank:]]/
syntax match BadStyle	/[[:blank:]]<=[^<[:blank:]]/
syntax match BadStyle	/[^[:blank:]]<=[^[:blank:]]/

syntax match BadStyle	/[^<[:blank:]]<<[^<=[:blank:]]/
syntax match BadStyle	/[^<[:blank:]]<<[[:blank:]]/
syntax match BadStyle	/[[:blank:]]<<[^<=[:blank:]]/
syntax match BadStyle	/[^<[:blank:]]<<=[[:blank:]]/
syntax match BadStyle	/[[:blank:]]<<=[^<[:blank:]]/
syntax match BadStyle	/[^[:blank:]]<<=[^[:blank:]]/

syntax match BadStyle	/[^\->[:blank:]]>>[^>=[:blank:]]/
syntax match BadStyle	/[^>[:blank:]]>>[[:blank:]]/
syntax match BadStyle	/[[:blank:]]>>[^>=[:blank:]]/

syntax match BadStyle	/[^>[:blank:]]>>=[[:blank:]]/
syntax match BadStyle	/[[:blank:]]>>=[^>[:blank:]]/
syntax match BadStyle	/[^[:blank:]]>>=[^[:blank:]]/

syntax match BadStyle	/){/
syntax match BadStyle	/( /
syntax match BadStyle	/ )/
syntax match BadStyle	/ ;/
syntax match BadStyle	/\(for.*\)\@<!;.*;/
syntax match BadStyle	/\(  \)\+/
syntax match BadStyle	/}\n[[:blank:]]*else\>/

" more types...
syn keyword cType uint ubyte ulong uint64_t uint32_t uint16_t uint8_t boolean_t int64_t int32_t int16_t int8_t u_int64_t u_int32_t u_int16_t u_int8_t u8 u16 u32 u64 s8 s16 s32 s64
syn keyword cOperator likely unlikely

" C-mode formatting options
"   t auto-wrap comment
"   c allows textwidth to work on comments
"   q allows use of gq* for auto formatting
"   l don't break long lines in insert mode
"   r insert '*' on <cr>
"   o insert '*' on newline with 'o'
"   n recognize numbered bullets in comments
setlocal formatoptions=tcqlron


" C-mode options (cinoptions==cino)
" N	number of spaces
" Ns	number of spaces * shiftwidth
" >N	default indent
" eN	extra indent if the { is at the end of a line
" nN	extra indent if there is no {} block
" fN	indent of the { of a function block
" gN  indent of the C++ class scope declarations (public, private, protected)
" {N	indent of a { on a new line after an if,while,for...
" }N	indent of a } in reference to a {
" ^N	extra indent inside a function {}
" :N	indent of case labels
" =N	indent of case body
" lN	align case {} on the case line
" tN	indent of function return type
" +N	indent continued algibreic expressions
" cN	indent of comment line after /*
" )N	vim searches for closing )'s at most N lines away
" *N	vim searches for closing */ at most N lines away
setlocal cindent
setlocal cinoptions=:0l1t0g0


" folding
"  - reserve 4 columns on the left for folding tree
"if winwidth(0) > 80
"    setlocal foldcolumn=4
"endif

"setlocal foldmethod=syntax
"setlocal foldnestmax=1
"setlocal foldenable
"map < :foldclose<CR>
"map > :foldopen<CR>
"hi Folded term=standout cterm=bold ctermfg=cyan ctermbg=NONE guifg=cyan guibg=NONE
"hi FoldColumn term=standout cterm=bold ctermfg=cyan ctermbg=NONE guifg=Cyan guibg=NONE

" cool C abbreviations...
abb #i #include
abb #d #define

setlocal makeprg=make\ %:r.o

"source ~/.vim/MakeDoxygenComment.vim
source ~/.vim/ccommenter.vim

