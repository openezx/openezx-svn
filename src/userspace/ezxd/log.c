/*
 *  log.c - ezxd plugin to log messages received by /dev/mux*
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ezxd.h"

FILE *logfd;

static int cmd_log(unsigned int mux, char *s, struct sb *client)
{
	fprintf(logfd, "<%x> %s\n", mux, s);
	fflush(logfd);
	return 0;
}

static int log_start(void)
{
	logfd = fopen("/tmp/ezxd.log", "a");
	if (!logfd) {
		return -1;
	}
	return 0;
}

static int log_stop(void)
{
	fclose(logfd);
	return 0;
}

struct ezxd_plugin log_plug = {
	.start = log_start,
	.stop = log_stop,
	.cmds = {
		{ ".*",	0x3e /* MUX1-5 */, cmd_log, },
	},
	.n_cmds = 1,
};
