/***************************************************************************
 *   Copyright (C) 2006 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INT32 unsigned int

struct Desc
{
    unsigned char descriptor_type;
    unsigned char descriptor_number;
    unsigned char nram;
    unsigned char proctype;
    INT32 startaddr;
    INT32 endaddr;
    INT32 jumpaddr;
    unsigned char n1[4];
    unsigned char n2;
    unsigned char chip1;
    unsigned char arch;
    unsigned char chip2;
    unsigned char n3;
    unsigned char ver[3];
    unsigned char checksum[2];
    unsigned char n4[2];
};


int main(int argc, char *argv[])
{
    struct Desc d;
    FILE *fp;
    unsigned char buf[32];
    int nread;
    int i;
    
    if (argc!=2)
    {
	printf("Usage: parseheader file.shx\n");
	return 0;
    }
    
    fp=fopen(argv[1],"r");
    if (fp==0) return 2;
    fseek(fp,0x300,0);
    
    fread(buf,1,2,fp);
    nread=buf[1]; nread<<=8; nread+=buf[0];
    printf("Codegroups: %d\n\n",nread); 
    
    printf("T    N R  PR Start    End            Size Jump     ???      ?  C1 A  C2 ?  Ver    CRC  ?? \n");

    FILE *fpout=fopen("out.txt","w");

//    while fread(buf,1,sizeof(buf),fp)==sizeof(buf))
    for (i=0; i<nread; i++)
    {
	fread(buf,1,sizeof(buf),fp);
	
	d.descriptor_type=buf[0];
	d.descriptor_number=buf[1];
	d.nram=buf[2];
	d.proctype=buf[3];
	
	d.startaddr=buf[7]; d.startaddr<<=8;
	d.startaddr+=buf[6]; d.startaddr<<=8;	
	d.startaddr+=buf[5]; d.startaddr<<=8;		
	d.startaddr+=buf[4];
	
	d.endaddr=buf[11]; d.endaddr<<=8;
	d.endaddr+=buf[10]; d.endaddr<<=8;	
	d.endaddr+=buf[9]; d.endaddr<<=8;		
	d.endaddr+=buf[8];

	d.jumpaddr=buf[15]; d.jumpaddr<<=8;
	d.jumpaddr+=buf[14]; d.jumpaddr<<=8;	
	d.jumpaddr+=buf[13]; d.jumpaddr<<=8;		
	d.jumpaddr+=buf[12];

	memcpy(d.n1, buf+16,4);
	d.n2=buf[20];
	d.chip1=buf[21];
	d.arch=buf[22];
	d.chip2=buf[22];
	d.n3=buf[23];
	memcpy(d.ver, buf+24,3);
	memcpy(d.checksum, buf+27,3);
	memcpy(d.n4, buf+30,2);	
	
	printf("%02x %3d %02x %02x %08x %08x %10d %08x %02x%02x%02x%02x %02x %02x %02x %02x %02x %02x%02x%02x %02x%02x %02x%02x\n",
	    d.descriptor_type,d.descriptor_number, d.nram, d.proctype, d.startaddr, d.endaddr, d.endaddr-d.startaddr+1, d.jumpaddr,
	    d.n1[0], d.n1[1], d.n1[2], d.n1[3],
	    d.n2,
	    d.chip1, d.arch, d.chip2,
	    d.n3,
	    d.ver[0],d.ver[1],d.ver[2],
	    d.checksum[0],d.checksum[1],
	    d.n4[0],d.n4[1]
	);
	fprintf(fpout,"%3d %08x %10d\n", d.descriptor_number, d.startaddr, d.endaddr-d.startaddr+1);
	    
    }
    fclose(fpout);
}
