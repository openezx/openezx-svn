/* ipclog - a module for hexdumping all traffic on the usb interface between
 * Bulverde and Neptune on the Motorola EZX phone platform
 *
 * (C) 2005 by Harald Welte <laforge@gnumonks.org>
 *
 * This program is licensed under GNU GPL, Version 2
 */


#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Harald Welte <laforge@gnumonks.org>");

extern void (*ipcusb_ap_to_bp)(unsigned char*, int);
extern void (*ipcusb_bp_to_ap)(unsigned char*, int);

#define DBG_MAX_BUF_SIZE	1024
static unsigned char tbuf[DBG_MAX_BUF_SIZE];

static void bvd_dbg_hex(__u8 *buf, int len, char *prefix)
{
	int i;
	int c;

	if (len <= 0)
		return;

	c = 0;  
	for (i=0; (i < len) && (c < (DBG_MAX_BUF_SIZE - 3)); i++) {
		sprintf(&tbuf[c], "%02x ",buf[i]);
		c += 3;
	}
	tbuf[c] = 0;

	printk("%s: %s\n", prefix, tbuf);
}

static void ipclog_ap_to_bp(unsigned char *buf, int len)
{
	bvd_dbg_hex(buf, len, __FUNCTION__);
}

static void ipclog_bp_to_ap(unsigned char *buf, int len)
{
	bvd_dbg_hex(buf, len, __FUNCTION__);
}

static int __init ipclog_init(void)
{
	if (ipcusb_ap_to_bp || ipcusb_bp_to_ap)
		return -EBUSY;

	ipcusb_ap_to_bp = &ipclog_ap_to_bp;
	ipcusb_bp_to_ap = &ipclog_bp_to_ap;

	return 0;
}

static void __exit ipclog_fini(void)
{
	if (ipcusb_ap_to_bp == ipclog_ap_to_bp)
		ipcusb_ap_to_bp = NULL;

	if (ipcusb_bp_to_ap == ipclog_bp_to_ap)
		ipcusb_bp_to_ap = NULL;
}

module_init(ipclog_init);
module_exit(ipclog_fini);
