#!/bin/sh
GT=/home/root/gpio/gpiotool

$GT 	--write-gpio 82 1 --write-af 82 OUT 	\
	--write-gpio 57 1 --write-af 57 OUT 	\
	--read-gpio 57 --read-gpio 82		\
	--write-gpio 82 0 			\
	--read-gpio 57 --read-gpio 82		\
	--msleep 1000				\
	--write-gpio 82 1			\
	--read-gpio 57 --read-gpio 82		\
	--write-af 57 IN

