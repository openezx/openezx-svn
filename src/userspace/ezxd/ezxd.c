/*
 *  ezxd.c - daemon for miscellaneous ezx tasks
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/select.h>

#include "ezxd.h"

extern struct cmd builtin_cmds[];
extern int plugin_cmd(unsigned int, char *, struct sb *);
extern int cmdui_new_connection(int);
extern int cmdui_setup_socket(void);
extern int cmdui_read_cmd(struct sb *);
extern int cmdui_write(struct sb *, char *);
extern int cmdui_real_write(struct sb *);
extern int mux_real_write(struct sb *);
extern int mux_read_cmd(struct sb *, int);
extern int mux_write(unsigned int, char *);

int loop = 1;


int eval_cmd (unsigned int mux, char *cmd, struct sb *client)
{
	int ret;
	int n;

	if (!mux) {
		for (n=0;builtin_cmds[n].handler!=NULL;n++) {
			if (!strncmp(builtin_cmds[n].str, cmd,
					strlen(builtin_cmds[n].str))) {
				ret = builtin_cmds[n].handler(mux, cmd, client);
				goto ret;
			}
		}
	}
	else {
		for (n=0;n<MAX_CLIENTS;n++)
			if (clients[n]) {
				strcat(cmd, "\r\n");
				cmdui_write(clients[n], cmd);
			}
	}
	ret = plugin_cmd(mux, cmd, client);

	if (ret == 0xbadc0de) { /* FIXME */
		strcat(cmd, "\r");
		mux_write(MUX(1), cmd);
		ret = 0;
	}
ret:
	return ret;
}	

int parse_config(char *path)
{
	int ret = -1;
	FILE *f;
	char *p, buffer[128];

	f = fopen(path, "r");
	if (!f)
		goto end;
	
	while (fgets(buffer, 128, f)) {
		buffer[strlen(buffer)-1] = 0;
		if ((p = strstr(buffer, "#")) != NULL)
			buffer[p-buffer] = 0;
		while(buffer[strlen(buffer)-1] == ' ')
			buffer[strlen(buffer)-1] = 0;
		if (!strlen(buffer))
			continue;
		if (eval_cmd(0, buffer, NULL)) {
			dbg ("error evaluating '%s'\n", buffer);
			goto fclose;
		}
	}
	fclose(f);
	return 0;
fclose:
	fclose(f);
end:
	dbg ("failed to parse config file.\n");
	return ret;
}



#define max(x, y) (x > y ? x : y)
int main(int argc, char **argv)
{
	int c, max_fd;
	fd_set rfds, tfds;
	char config_file[64];

	snprintf(config_file, 64, "%s/ezxd.conf", CONFIG_PATH);
	if (parse_config(config_file))
		return -1;
	if (cmdui_setup_socket())
		return -1;

	while(loop) {
		FD_ZERO(&rfds); FD_ZERO(&tfds);

		/* select main socket for accept */
		FD_SET(sfd, &rfds);
		max_fd = sfd;

		/* select all open clients for read,
		   write pending clients for write */
		for (c=0;c<MAX_CLIENTS;c++) {
			if (clients[c] && clients[c]->fd >= 0) {
				FD_SET(clients[c]->fd, &rfds);
				if (clients[c]->outb[0])
					FD_SET(clients[c]->fd, &tfds);
				max_fd = max(clients[c]->fd, max_fd);
			}
		}
		/* select all open mux devices for read/write */
		for (c = 1; c <= N_MUX; c++) {
			if (muxes[c] && muxes[c]->fd >= 0) {
				FD_SET(muxes[c]->fd, &rfds);
				if (muxes[c]->outb[0])
					FD_SET(muxes[c]->fd, &tfds);
				max_fd = max(muxes[c]->fd, max_fd);
			}
		}

		if (select(max_fd+1, &rfds, &tfds, NULL, NULL) > 0) {
			if (FD_ISSET(sfd, &rfds))
				cmdui_new_connection(sfd);

			/* read/write clients */
			for (c=0;c<MAX_CLIENTS;c++) {
				if (!clients[c]) continue;
				if (FD_ISSET(clients[c]->fd, &tfds))
					cmdui_real_write(clients[c]);
				if (FD_ISSET(clients[c]->fd, &rfds))
					cmdui_read_cmd(clients[c]);
			}

			/* read/write mux devices */
			for (c = 1; c <= N_MUX; c++) {
				if (!muxes[c]) continue;
				if (FD_ISSET(muxes[c]->fd, &rfds))
					mux_read_cmd(muxes[c], c);
				if (FD_ISSET(muxes[c]->fd, &tfds))
					mux_real_write(muxes[c]);
			}
		}
	}
	return 0;
}
