/*
 *  builtin.c - builtin cmdui commands
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <string.h>
#include "ezxd.h"

extern int plugin_load(char *);
extern int plugin_unload(char *);
extern int cmdui_write(struct sb *, char *);
extern int mux_open_all();

struct cmd builtin_cmds[];

static int cmd_open_mux(unsigned int mux, char *cmd, struct sb *client)
{
	return mux_open_all();
}

static int cmd_load(unsigned int mux, char *cmd, struct sb *client)
{
	char *arg = strstr(cmd, "=") + 1;
	if (arg)
		return (plugin_load(arg));
	return -1;
}

static int cmd_unload(unsigned int mux, char *cmd, struct sb *client)
{
	char *arg = strstr(cmd, "=") + 1;
	if (arg)
		return (plugin_unload(arg));
	return -1;
}

static int cmd_list(unsigned int mux, char *cmd, struct sb *client)
{
	char buf[256];
	int c;

	if (!client)
		return -1;
	cmdui_write(client, "loaded modules:");
	for (c=0;c<MAX_PLUGINS;c++)
		if(plugins[c]) {
			snprintf(buf, sizeof(buf), " %s", 
						plugins[c]->name);
			cmdui_write(client, buf);
		}
	cmdui_write(client, "\r\n");
	return 0;
}

static int cmd_help(unsigned int mux, char *cmd, struct sb *client)
{
	char buf[256];
	int c, n;

	if (!client)
		return -1;
	cmdui_write(client, "builtin commands:");
	for(c=0;builtin_cmds[c].handler;c++) {
		snprintf(buf, sizeof(buf), " '%s'", builtin_cmds[c].str);
		cmdui_write(client, buf);
	}
	cmdui_write(client, "\r\nplugin commands:");
	for(c=0;c<MAX_PLUGINS;c++) {
		if(!plugins[c])
			continue;
		for(n=0;n<plugins[c]->n_cmds;n++)
			if(plugins[c]->cmds[n].flags == 0) {
				snprintf(buf, sizeof(buf), " '%s'",
						plugins[c]->cmds[n].str);
				cmdui_write(client, buf);
			}
	}
	cmdui_write(client, "\r\n");
	return 0;
}

struct cmd builtin_cmds[] = {
	{ "AT+LOADM=",	0, cmd_load, },
	{ "AT+UNLOADM=",0, cmd_unload, },
	{ "AT+MODULES?",0, cmd_list, },
	{ "AT+OPENMUX",	0, cmd_open_mux, },
	{ "AT+HELP",	0, cmd_help, },
	{ NULL, 0, NULL },
};


