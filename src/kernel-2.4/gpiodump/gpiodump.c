#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/config.h>
#include <linux/init.h>
#include <asm/io.h>
#include <asm/arch-pxa/pxa-regs.h>

struct gpio_config {
	__u8	dir;
	__u8	af;
};

static struct gpio_config gconfig[127];

static void read_config(void)
{
	int i, base;
	__u32 reg;

	base = 0;
	reg = GPDR0;
	for (i = 0; i < 32; i++) {
		if (reg & (1 << i))
			gconfig[base+i].dir = 1;
		else
			gconfig[base+i].dir = 0;
	}

	base = 32;
	reg = GPDR1;
	for (i = 0; i < 32; i++) {
		if (reg & (1 << i))
			gconfig[base+i].dir = 1;
		else
			gconfig[base+i].dir = 0;
	}

	base = 64;
	reg = GPDR2;
	for (i = 0; i < 32; i++) {
		if (reg & (1 << i))
			gconfig[base+i].dir = 1;
		else
			gconfig[base+i].dir = 0;
	}
	base = 96;
	reg = GPDR2;
	for (i = 0; i < 32; i++) {
		if (reg & (1 << i))
			gconfig[base+i].dir = 1;
		else
			gconfig[base+i].dir = 0;
	}

	base = 0;
	reg = GAFR0_L;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 16;
	reg = GAFR0_U;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 32;
	reg = GAFR1_L;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 48;
	reg = GAFR1_U;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 64;
	reg = GAFR2_L;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 80;
	reg = GAFR2_U;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 96;
	reg = GAFR3_L;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

	base = 112;
	reg = GAFR3_U;
	for (i = 0; i < 16; i++)
		gconfig[base+i].af = (reg >> (i*2) & 0x03);

}

static void print_config(void)
{
	int i;

	for (i = 0; i < 121; i++) {
		printk("GPIO%3d\t%c\t%d\n", i,
			gconfig[i].dir ? 'O':'I', gconfig[i].af);
	}
}

static void cken_stuart(void)
{
	CKEN |= 0x00000020;
}

static void dump_stuart(void)
{
	printk("STIER=0x%08x, ", STIER);
	printk("STIIR=0x%08x, ", STIIR);
	printk("STLCR=0x%08x, ", STLCR);
	printk("STLSR=0x%08x\n", STLSR);
	printk("STMCR=0x%08x, ", STMCR);
	printk("STMSR=0x%08x, ", STMSR);
	printk("STISR=0x%08x, ", STISR);
	//printk("STFOR=0x%08x, ", STFOR);
	printk("\n");
}

static int init_gpiodump(void)
{

#if 0
	printk("GPDR0=0x%x\nGPDR1=%x\nGPDR2=%x\n", GPDR0, GPDR1, GPDR2);
	printk("GAFR0_L=0x%x\nGAFR0_H=0x%x\n", GAFR0_L, GAFR0_U);
	printk("GAFR1_L=0x%x\nGAFR1_H=0x%x\n", GAFR1_L, GAFR1_U);
	printk("GAFR2_L=0x%x\nGAFR2_H=0x%x\n", GAFR2_L, GAFR2_U);
	printk("GAFR3_L=0x%x\nGAFR3_H=0x%x\n", GAFR3_L, GAFR3_U);
#endif

	read_config();
	print_config();

	printk("CKEN=0x%08x\n", CKEN);
	cken_stuart();
	printk("CKEN=0x%08x\n", CKEN);
	dump_stuart();



	return 0;
}

static void exit_gpiodump(void)
{

}

MODULE_LICENSE("GPL");

module_init(init_gpiodump);
module_exit(exit_gpiodump);

