/*
 * src/blob/boot_modes.c
 * 
 * support more boot modes in blob
 *
 * Copyright (C) 2005, 2006  Motorola
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 2005-Jun-06 - (Motorola) init draft
 * 2005-Dec-21 - (Motorola) add Macau support, and update GPDR3 from HW
 * 2006-Jun-15 - (Motorola) add PengLai support
 *
 */
#ifdef HAVE_CONFIG_H
#include <blob/config.h>
#endif


#include <blob/types.h>
#include <blob/pxa.h>
#include <blob/handshake.h>
#include <blob/flash.h>
#include <blob/arch/lubbock.h>
#include <blob/boot_modes.h>
#include "usb_ctl_bvd.h"
#include <blob/lcd_ezx.h>
#include <blob/mmu.h>
#include "pcap.h"
#include <blob/log.h>
/* check BP_WDI flag */
#define bp_flag_not_set()	(*(unsigned long *)(BPSIG_ADDR) == NO_FLAG)
#define bp_flag_is_set()	(*(unsigned long *)(BPSIG_ADDR) == WDI_FLAG)


/* keypad macros */

//for P2 board //if( KPAS == (0x280000f4) ) 
//for 64MB dataflash P1 board 
//for P1 work -- if( KPAS == (0x04000022) )
#if defined(HAINAN) || defined(SUMATRA) 
#define FLASH_KEY 0x04000025
#define KEY_DEBOUNCE 40000	/* debounce 40ms */
#elif defined(MACAU)
#define FLASH_KEY 0x04000022
#define KEY_DEBOUNCE 40000	/* debounce 40ms */
#elif defined(PENGLAI)
#define FLASH_KEY 0x04000022
#define KEY_DEBOUNCE 40000	/* debounce 40ms */
#else

#define FLASH_KEY 0x04000013
#define KEY_DEBOUNCE 40000	/* debounce 40ms */
#endif

static void naked_usb_flash(u32 data); /* a tool function just start usb flash loop */
extern int is_key_press_down(u32 key_val,u32 Deb_val);
extern void Delay(int uSec);
extern void udc_int_hndlr(int, void *);
extern int usbctl_init( void );
extern void USB_gpio_init(void);
extern struct mem_area io_map[];
/* structure note:
 This module includes all non-boot-linux boot modes in blob,
 such as flash, pass-through mode...etc*/

BOOL check_flash_flag(void)
{
  return (*(unsigned long *)(FLAG_ADDR) == REFLASH_FLAG);
}

BOOL check_valid_code(void)
{
  BOOL rev = TRUE;

#if 0	// wait for ZQ
  u32* pbarkercode = BARKER_CODE_ADDRESS;

  if ( (*pbarkercode) != BARKER_CODE_VALID_VAL )
    { 
      rev = FALSE;
    }
#endif
  return rev; 
}	
/*
 check_pass_through_flag()
 check if flag in memory for pass through mode is set
 return TRUE: pass-through mode
 return FALSE: no pass-thru mode
*/


BOOL check_pass_through_flag(void){
#if 1
  return (*(unsigned long *)(PASS_THRU_FLAG_ADDR) == PASS_THRU_FLAG);
#else  /* just for test phase */
  return TRUE;
#endif
}

/* 
 enter_pass_through_mode
 enter USB pass_through mode and do all initial work for emulator.
 blob will stay in this function in life time
*/
void enter_pass_through_mode(void){
  EnableLCD_8bit_active();
  printlcd("barbado_1223.\n ");
  //  STSerialOutputString("Enter pass-through mode.\n ");
  printlcd("Enter pass-through mode.\n ");

#if 0  //wave form test.we output square wave to test validaty of pin under test
  {
    u32 gpio=GPIO_BP_RDY;
    set_GPIO_mode(gpio | GPIO_OUT); 
   
    while(1){
      u32 i=0x80000;
     
      while(i<=0)
	i--;
      GPSR(gpio) = GPIO_bit(gpio); /* hign level output */
     
      i=0x80000;			/* restore timeout */

      while(i<=0)
	i--;     
      GPCR(gpio) = GPIO_bit(gpio); /* low level output */ 
     


    }
  }


#endif //end waveform test

  //step 1 finish last BP hand shake
#if 1   //we do handshake step 1 again, to make sure
  //   set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);
  //  GPSR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW); 
  //reset BP step by step
  GPSR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);

  GPCR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);

  set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);

  GPSR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);  

  printlcd("bpreset low\n");
  //   restart_bp();
  //  set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_IN);

  {
    int i=BB_RESET_TIMEOUT;
    while(i>0)
      i--;

  }
  if(!( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) ))
    printlcd("error sw low\n");
  set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_IN);
  GPSR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);  
  printlcd("bpreset high\n");

  while( (( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) ))
	 && ((  GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) )) );
#endif
  printlcd("pass-thru stab 0.\n ");
  while(GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) );

#if 0
  while(1);
#endif 
  /*  wait some times before clear MCU_INT_SW */
  //  STSerialOutputString("pass-through mode. stab 1.\n ");
  {
    u32 i=0x1;
    u32 count=0xccccccc;
    while (i>0){
      while(count>0){
       
	count--;
      }
      i--;
     
    }
  }
  printlcd("pass-thru stab 1.\n ");
  //config MCU_INT_SW as output low

  set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);
  GPCR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);
  
  while(!(GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) ))
    {
#if 0
      static int i=0;
      if(!( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) )&& i<=10){
	
	  
	printlcd("MCU_int_sw=low\n");
	i++;

      }
#endif 

    }

  //  STSerialOutputString(" pass-through mode. stab 2.\n ");
  //  printlcd(" pass-thru stab 2.\n ");
  //config MCU_INT_SW as output high
  GPSR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW); 
  printlcd(" Handshake with BP...Pass\n ");
  printlcd(" init USB gpio\n ");


#if 0
  while(1);
#endif 
  //step 2 set USB to pass-through mode
  
  usb_gpio_pass_through_init();

  EnableLCD_8bit_active();
  printlcd("Now blob in\npass-through mode\n");
  {
    u32 i=0x1;
    u32 count=0xccccccc;
    while (i>0){
      while(count>0){
       
	count--;
      }
      i--;
     
    }
  }
  pcap_switch_off_usb();
  {
    int i=5000;
    while(i--);

  }
  //  printlcd(" Auto plug USB cable\n ");  
  pcap_switch_on_usb();

  /*  now enter pass-through mode successfully, clear flag bit */
  *(unsigned long *)(PASS_THRU_FLAG_ADDR) =OFF_FLAG;
  while(1);

}

//this function just set usb to pass-through mode
//for general flash setting, see usb_gpio_init()
void usb_gpio_pass_through_init(void){
  GPCR3=0x40E00124;
  GPDR0=0xC0000000;
  GPDR1=0x01000094;
  GPDR2=0x00040000;
  GPDR3=0x01800008;
  GAFR0_U=0xF0000000;
  GAFR1_L=0x00034190;
  GAFR1_U=0x00010800;
  GAFR2_U=0x00A00000;
  GAFR3_U=0x0000000C;
#if defined(SUMATRA) || defined(MACAU) || defined(PENGLAI)
  GPCR(94) = GPIO_bit(94);
  set_GPIO_mode(94 | GPIO_OUT);
#endif
  PSSR=0x00000020;
  UP2OCR=0x01000000;
  __REG(0x40600024)=1;


}


/* 
 * add enter_simple_pass_through_mode 
 * enter USB pass_through mode and do all initial work for emulator.
 * blob will stay in this function in life time
 */
void enter_simple_pass_through_mode(void){
  
  usb_gpio_pass_through_init();

  EnableLCD_8bit_active();
  printlcd("\n EzX barbados BP flash");             
  printlcd("\n PC->AP->BP Pass through mode! ");             
  printlcd("\n\nEZX AP bootloader.\nVersion 3.x 2005-01-04");  	
  printlcd("Now blob in\nsimple-pass-through\n");
  {
    u32 i=0x1;
    u32 count=0xccccccc;
    while (i>0){
      while(count>0){
       
	count--;
      }
      i--;
     
    }
  }

#if 1 // tide add here
  GPCR(99) = GPIO_bit(99);  // tide ??? USB_READY      //pcap_switch_off_usb();
  {
    int i=5000;
    while(i--);

  }
  //  printlcd(" Auto plug USB cable\n ");  
  GPSR(99) = GPIO_bit(99);  // tide ??? USB_READY     //  pcap_switch_on_usb();
#endif  // tide add here

  /* now enter pass-through mode successfully, clear flag bit */
  //  *(unsigned long *)(PASS_THRU_FLAG_ADDR) =OFF_FLAG;
  while(1);
}




/* from E680/A780 EMU , we must have the feature  VA+slide for E680 and VA+scroll key for A780 to trigger flash mode */
BOOL check_manual_flash_mode(void)
{
  BOOL rev = TRUE;


  if(is_key_press_down(FLASH_KEY,KEY_DEBOUNCE)) 
    {
      logstr("\nezx_test: REC button is pressed!!!.\n");
      rev = TRUE;
    }
  else
    {
      logstr("\nezx_test: REC button is NOT pressed!!!.\n");
      rev = FALSE;
    }

  return rev; 
}

void enter_manual_flash_mode(u32 data){
  u32 hold_count=0;	/* for hold key checking */
  EnableLCD_8bit_active();
#ifdef MBM
  printlcd("\n reset BP\nturn off usb\n");
#endif
#ifndef MBM
  printlcd("\n reset BP ");
#endif
  /* Force BP enter FLASH mode */
  GPSR(41) = GPIO_bit(41);   /* tide add the line to flash BP */
  
  restart_bp();
  printlcd("\n end reset BP ");
  
 
  /* check if key is holding, 4-5 sec to pass-thru BP mode */
  /* now use a long debounce to implement this delay */
  hold_count=8;
  do{
    if(!is_key_press_down(FLASH_KEY,KEY_DEBOUNCE))
      break; /* key released before time out, enter AP flash mode */
    hold_count--;
    //count*Delay=8*0x5=4 sec
    Delay(500*1000); /* check key per 0.5 s */
  }
  while(hold_count>0);

  if (hold_count>0)
    naked_usb_flash(0);
 
  printlcd("Enter BP Pass-thru\n");
  //timeout, enter bp only mode
  enter_simple_pass_through_mode();                    
  while(1) ; //stop here, AP has nothing to do...
  

 
  
}

void enter_tcmd_flash_mode(u32 data){
  *(unsigned long *)(FLAG_ADDR) = 0;
  EnableLCD_8bit_active();
  printlcd("\n reflash flag is set ");
  naked_usb_flash(0);

}
static void naked_usb_flash(u32 data){
  
  
  if (bp_flag_is_set())
    printlcd("\n BP assert BP_WDI before reset");
  if (bp_flag_not_set())
    printlcd("\n No BP_WDI before reset");
  *(unsigned long *)(BPSIG_ADDR) = 0;

  // clear flash section descriptor C,B bits for burn code
  //change mem map after mbm did can cause problem
  //now seems MBM turn off cache buffer, so skip this
#ifndef MBM
  set_uncache_unbuffer(&io_map[0]);
#endif
#ifdef MBM
		
  USB_gpio_init();
		
  usbctl_init();
#endif
  printlcd("\n AP+BP HAB reflash code! ");
#ifdef BARBADOS             
  printlcd("\n\nEZX AP bootloader.\n2005-JUN-07:2\n");
#endif
#ifndef BARBADOS
  printlcd("\n\nEZX AP bootloader.\n2005-JUN-07:2\n");
#endif		
  printlcd("switch off usb cable\n");
  GPCR(99) = GPIO_bit(99);  // USB_READY=low
  {
    int i=5000*4;
    while(i--);
		  
  }
  GPSR(99) = GPIO_bit(99);  // USB_READY =high
  printlcd("switch on usb cable\n");
  /* hwuart_init(230400); */
  for(; ;) {
    if(ICPR & 0x800) {
      udc_int_hndlr(0x11, 0);
    }
  }

  Disablelcd();
  Disablebklight();

}
