/*
 *  raw.c - ezxd plugin to aid debugging
 *
 *  Copyright (c) 2007 Daniel Ribeiro <drwyrm@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include "ezxd.h"

extern int mux_write(unsigned int, char *);
extern int cmdui_write(struct sb *, char *);

static int cmd_print(unsigned int mux, char *s, struct sb *client)
{
	int c;
	char buf[1024];
	snprintf(buf, 1024, "raw(%02x): %s\r\n", mux, s);
	dbg ("raw(%02x): %s\n", mux, s);
	for (c=0;c<MAX_CLIENTS;c++)
		if (clients[c] && clients[c]->flags & FLAG_TSRAW)
			cmdui_write(clients[c], buf);
	return(0);
}

static int cmd_raw(unsigned int mux, char *s, struct sb *client)
{
	int m;
	char buf[1024];
	if (sscanf(s, "raw %d %s", &m, buf) != 2)
		return -1;
	dbg("raw: sending '%s' to mux %d\n", buf, m);
	strcat (buf, "\r");
	return mux_write(MUX(m), buf);
}

static int cmd_raw_toggle(unsigned int mux, char *s, struct sb *client)
{
	char *arg = strstr(s, " ")+1;

	if(!strcmp(arg, "on"))
		client->flags |= FLAG_TSRAW;
	else
		client->flags &= ~FLAG_TSRAW;
	return 0;
}

struct ezxd_plugin raw_plug = {
	.cmds = {
		{ ".*",	0x3e /* MUX1-5 */, cmd_print, },
		{ "^raw (on|off)$", 0, cmd_raw_toggle, },
		{ "^raw [0-4] [[:graph:]]*$", 0, cmd_raw, },
	},
	.n_cmds = 3,
};
