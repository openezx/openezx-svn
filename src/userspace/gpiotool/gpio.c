/*
 * gpiotool - user-level access to PXA27x GPIO registers
 *
 * (C) 2006 by Harald Welte <laforge@openezx.org>
 *
 * This program is Free Software and licensed under GNU GPL Version 2,
 * as published by the Free Software Foundation.
 */


#include <string.h>

#include "gpio_regs.h"
#include "mmio.h"

void pxa_gpio_mode_set(int gpio_mode)
{
	int gpio = gpio_mode & GPIO_MD_MASK_NR;
	int fn = (gpio_mode & GPIO_MD_MASK_FN) >> 8;
	u_int32_t gafr;
	u_int32_t tmp;

	if (gpio_mode & GPIO_DFLT_LOW)
		mmio_wr(GPCR(gpio), GPIO_bit(gpio));
	else if (gpio_mode & GPIO_DFLT_HIGH)
		mmio_wr(GPSR(gpio), GPIO_bit(gpio));

	tmp = mmio_rd(GPDR(gpio));
	if (gpio_mode & GPIO_MD_MASK_DIR)
		tmp |= GPIO_bit(gpio);
	else
		tmp &= ~GPIO_bit(gpio);
	mmio_wr(GPDR(gpio), tmp);

	gafr = mmio_rd(GAFR(gpio));
	gafr &= ~(0x03 << (((gpio) & 0xf)*2));
	gafr |=  (fn   << (((gpio) & 0xf)*2));
	mmio_wr(GAFR(gpio), gafr);
}

int pxa_gpio_mode_get(int gpio_in)
{
	int gpio = gpio_in & GPIO_MD_MASK_NR;
	int mode = 0;
	u_int32_t tmp;

	tmp = mmio_rd(GPDR(gpio));
	if (tmp & GPIO_bit(gpio))
		mode  |= GPIO_OUT;

	tmp = mmio_rd(GAFR(gpio));
	tmp = (tmp >> (((gpio) & 0xf)*2)) & 0x03;
	
	return mode | (tmp << 8);
}

int pxa_gpio_read(int gpio)
{
	unsigned long addr = GPLR(gpio);
	
	return mmio_rd(addr) & GPIO_bit(gpio) ? 1 : 0;
}

int pxa_gpio_set(int gpio, int val)
{
	if (val == 0)
		return mmio_wr(GPCR(gpio), GPIO_bit(gpio));
	else
		return mmio_wr(GPSR(gpio), GPIO_bit(gpio));
}

char *pxa_gpio_mode2txt(int mode)
{
	static char buf[255];

	buf[0] = 0;

	if (mode & GPIO_OUT)
		strcat(buf, "OUT ");
	else
		strcat(buf, "IN  ");

	switch (mode & GPIO_MD_MASK_FN) {
	case GPIO_ALT_FN_3_IN:
		strcat(buf, "AF3 ");
		break;
	case GPIO_ALT_FN_2_IN:
		strcat(buf, "AF2 ");
		break;
	case GPIO_ALT_FN_1_IN:
		strcat(buf, "AF1 ");
		break;
	}

	return buf;
}
