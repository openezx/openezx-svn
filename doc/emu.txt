/dev/emu:

It is yet unknown what EMU means.  /dev/emu doesn't seem to be opened by any
process, at least not on an idle phone.

It is somehow related to multiplexing between multiple devices.

EMU_SW_TO_UART
EMU_SW_TO_USB
EMU_SW_TO_AUDIO_MONO
EMU_SW_TO_AUDIO_STEREO

EMU_PIHF_INIT
EMU_PIHF_SNP_INT_CTL
EMU_PIHF_ID_LOW
EMU_PIHF_ID_HIGH
EMU_PIHF_INIT_DPLUS_CTL

EMU_EIHF_INIT
EMU_EIHF_MUTE
EMU_EIHF_UNMUTE

