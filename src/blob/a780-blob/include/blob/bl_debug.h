#ifndef EZX_DEBUG_H
#define EZX_DEBUG_H
/*
 * bl_debug.h
 *
 * EZX project debug output header file
 * 
 * Copyright (C) 2001-2005 Motorola
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Feb 7,2001 - (Motorola) Created
 * 
 */
/*------------------------------------------------------------------------------
    INCLUDE FILES
------------------------------------------------------------------------------*/

#include "bt_uart.h"


/*------------------------------------------------------------------------------
    CONSTANTS
------------------------------------------------------------------------------*/

void Release_Debug(u8 * str_out);


/*------------------------------------------------------------------------------
    MACROS
------------------------------------------------------------------------------*/



#define PRINT_STR(str)                        \
do                                          \
{                                           \
    BTSerialOutputString(str);                      \
} while(0);                                 \

#define PRINT_HEX(num)                      \
do                                          \
{                                           \
    BTSerialOutputHex(num);                 \
} while(0);                                 \


#define PRINT_DEC(num)                      \
do                                          \
{                                           \
    BTSerialOutputDec(num);                 \
} while(0);                                 \







/*------------------------------------------------------------------------------
    ENUMS, STRUCTURES, UNIONS & OTHER TYPEDEFS
------------------------------------------------------------------------------*/



/*------------------------------------------------------------------------------
    GLOBAL VARIABLE DECLARATIONS
------------------------------------------------------------------------------*/



/*------------------------------------------------------------------------------
    EXTERNAL FUNCTION PROTOTYPES
------------------------------------------------------------------------------*/




#endif /* EZX_DEBUG_H */
