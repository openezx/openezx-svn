/*
 * bl_flash.c
 *
 * The flash driver for RDL.
 * 
 * Copyright (C) 2001-2005 Motorola
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Oct 30, 2002 - (Motorola) Created
 * 
 */
 /*================================================================================
INCLUDE FILES
==============================================================================*/
#include "bl_flash.h"
#include "pxa.h"

#include "bt_uart.h"

#ifdef ZQ_DEBUG
#include "serial.h"
#endif /* ZQ_DEBUG */

/*==============================================================================
LOCAL CONSTANTS
==============================================================================*/


/*==============================================================================
LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==============================================================================*/

/*==============================================================================
LOCAL MACROS
==============================================================================*/
#define K3WRITEBUFSIZE 32
/*==============================================================================
LOCAL FUNCTION PROTOTYPES
==============================================================================*/

/* Check Status Register */
FLASH_STATUS stat_reg_check(u8 sr);



/*==============================================================================
LOCAL VARIABLES
==============================================================================*/


/*==============================================================================
GLOBAL VARIABLES
==============================================================================*/


/*==============================================================================
LOCAL POINTER DECLARATIONS
==============================================================================*/


/*==============================================================================
LOCAL FUNCTIONS
==============================================================================*/



/*==============================================================================

FUNCTION: flash_unlock_block

DESCRIPTION: Unlock a block of flash

ARGUMENTS PASSED:
   flash_addr - The start addr of the block that need to be unlocked.
       
REFERENCE ARGUMENTS PASSED:
   None
  
RETURN VALUE:
   FLASH_STATUS

DEPENDENCIES:
   None

SIDE EFFECTS:
   None

==============================================================================*/

FLASH_STATUS flash_unlock_block (volatile u16 *flash_addr)
{
	u8 sr;					/* status register */

        u8 timeout = 0;

	*flash_addr = 0x60;		
 	*flash_addr = 0xD0;    
	while (!(BIT_7 & *flash_addr))
	{
		 if ((timeout ++) > 5000000)
                {
                         return FLASH_STATUS_PROGRAM_ERR;
                }
	};							
	sr = *flash_addr;	
	*flash_addr = 0x50;		
	*flash_addr = 0xFF;     
	return stat_reg_check(sr);
}


/*==============================================================================

FUNCTION: stat_reg_check 

DESCRIPTION: Check the status of flash register

ARGUMENTS PASSED:
   sr - The value of register that need to be checked.
       
REFERENCE ARGUMENTS PASSED:
   None
  
RETURN VALUE:
   FLASH_STATUS

DEPENDENCIES:
   None

SIDE EFFECTS:
   None

==============================================================================*/

FLASH_STATUS stat_reg_check (u8 sr)
{
	FLASH_STATUS err;   /* error indication */

	if (sr & BIT_3)
		err = FLASH_STATUS_VPP_ERR;
	else if (sr & BIT_1)
		err = FLASH_STATUS_BLOCK_PROTECT_ERR;
	else if ( (sr & BIT_4) && (sr & BIT_5) )
		err = FLASH_STATUS_CMD_SEQ_ERR;
	else if (sr & BIT_5)
		err = FLASH_STATUS_ERASE_ERR;
	else if (sr & BIT_4)
		err = FLASH_STATUS_PROGRAM_ERR;
	else
		err = FLASH_STATUS_OK;
	return (err);
}

/*==============================================================================

FUNCTION: flash_chip_erase_K3RC28f128k

DESCRIPTION: Erase a block of flash

ARGUMENTS PASSED:
   flash_addr - The start addr of this block
   
REFERENCE ARGUMENTS PASSED:
   None

RETURN VALUE:
   None

DEPENDENCIES:
   None

SIDE EFFECTS:
   None

==============================================================================*/

FLASH_STATUS flash_chip_erase_K3RC28f128k(volatile u16 *flash_addr)
{
	u8 sr;				  /* status register */
        u8 timeout = 0;	
	*flash_addr = 0x20;		  
	*flash_addr = 0xD0;		
	while (!(BIT_7 & *flash_addr))
	{
		 if ((timeout ++) > 5000000)
                 {
                         return FLASH_STATUS_PROGRAM_ERR;
                 }

	};						
	sr = *flash_addr;		 
	*flash_addr = 0x50;   
	*flash_addr = 0xFF;  
	return stat_reg_check(sr);
}

/*==============================================================================

FUNCTION: flash_program_K3RC28f128k

DESCRIPTION: Local utility to test if two strings match

ARGUMENTS PASSED:
   str1_ptr, str2_ptr - pointers to strings to compare
    
REFERENCE ARGUMENTS PASSED:
   None
  
RETURN VALUE:
   TRUE - strings match
   FALSE - strings differ

DEPENDENCIES:
   None

SIDE EFFECTS:
   None

==============================================================================*/

FLASH_STATUS flash_program_K3RC28f128k (volatile u16 *flash_addr,
									u32 length, u16 *flash_buf)
{
	u8 sr;

	u32 writebufNum = 0;
	u32 leftBuf = 0;
	u32 timeout = 0;
	u32 i;
    FLASH_STATUS err = FLASH_STATUS_OK;
    
    writebufNum = length / (2 * K3WRITEBUFSIZE); 
    leftBuf = length % (2 * K3WRITEBUFSIZE);

#ifdef ZQ_DEBUG
    SerialOutputString("Begin to program the flash...\n");
#endif /* ZQ_DEBUG */

	while (writebufNum > 0)
	{
		*flash_addr = 0xE8;

		while (!(BIT_7 & *flash_addr))
		{
			if ((timeout ++) > 5000000)
			{
				
				return FLASH_STATUS_PROGRAM_ERR;
			}	
			
		}	
		*flash_addr = K3WRITEBUFSIZE - 1;		
		for (i = 0;  i < K3WRITEBUFSIZE;  i++)
		{
            *(flash_addr+i) = *(flash_buf+i);
        }
				 
		sr = *flash_addr;		 
		*flash_addr = 0xD0;    
		/* wait*/
		timeout =0;
		while (!(BIT_7 & *flash_addr))
		{
			if ((timeout ++) > 5000000)
			{
				
				return FLASH_STATUS_PROGRAM_ERR;
			}	
			
		}    

/* check if error occurs */

/* increments address, decrement counter */

		flash_addr += K3WRITEBUFSIZE;

		flash_buf += K3WRITEBUFSIZE;

		writebufNum -= 1;
	}
	
	while ( leftBuf > 0)
	{
		if (leftBuf == 1)
			*flash_buf = *(flash_buf) | 0x00ff;

		*flash_addr = 0x40;	
		*flash_addr = *flash_buf; 
                timeout = 0;
		while (!(BIT_7 & *flash_addr))
		{
			 if ((timeout ++) > 5000000)
                        {

                                return FLASH_STATUS_PROGRAM_ERR;
                        }

		};						 
		sr = *flash_addr;		 
		*flash_addr = 0x50;      
		*flash_addr = 0xFF;    

/* check if error occurs */
		err = stat_reg_check(sr);
		if (err != FLASH_STATUS_OK)
		{
		    return err;
		}
		/*
		if (sr & BIT_3)
			return (VPP_ERR);
		else if (sr & BIT_1)
			return (BLOCK_PROTECT_ERR);
		else if (sr & BIT_4)
			return (PROGRAM_ERR);
*/
/* increments address, decrement counter */
		++flash_addr;

		++flash_buf;
		leftBuf -=2;
	}
	
	*flash_addr = 0x70;
	*flash_addr = 0xFF;
	return err;
}
/*==============================================================================
GLOBAL FUNCTIONS
==============================================================================*/

/*==============================================================================

FUNCTION: flash_erase_block

DESCRIPTION: 

ARGUMENTS PASSED:

REFERENCE ARGUMENTS PASSED:
   None

RETURN VALUE:
   
DEPENDENCIES:
   None

SIDE EFFECTS:
   None

==============================================================================*/
FLASH_STATUS flash_erase_blocks(u32 start_addr, u32 end_addr)
{
    u32 erase_block_num = 0;
    u32 erase_size = end_addr - start_addr;
    u32 i = 0;
    u32 temp = 0;
    FLASH_STATUS err_code = FLASH_STATUS_OK;


    if( erase_size <= 0 )
    {
        return FLASH_STATUS_ADDR_ERR;
    }

    /* caculate how many blocks it will erase */
    erase_block_num = erase_size / K3_ONEBLOCK_SIZE + 
                   (( erase_size % K3_ONEBLOCK_SIZE ) > 0 ? 1:0);

#ifdef OUTPUT_LOG_TO_BTPORT
    BTSerialOutputString("Entering flash_erase_blocks...\n");
    BTSerialOutputString("We want to erase flash from addr ");
    BTSerialOutputHex(start_addr);
    BTSerialOutputString(" to ");
    BTSerialOutputHex(end_addr);
    BTSerialOutputString("\n");
    BTSerialOutputString("The number of blocks that we want to erase is: ");
    BTSerialOutputDec(erase_block_num);
    BTSerialOutputString("\n");
    
#endif /* ZQ_DEBUG */
    
    for (i = 0; i < erase_block_num; i ++ )
    {
        temp = i * (u32)K3_ONEBLOCK_SIZE;
                               
        err_code = flash_unlock_block
                        ((volatile u16*) (start_addr + temp));	
        if (err_code != FLASH_STATUS_OK)
        {
#ifdef ZQ_DEBUG
            SerialOutputString("Error occurs when unlock the block.\n");
            SerialOutputString("And the error number is: ");
            SerialOutputDec(err_code);
            SerialOutputString("\n");
            SerialOutputString("The start address of the block is: ");
            SerialOutputHex(start_addr + temp);
            SerialOutputString("\n");
#endif /* ZQ_DEBUG */

            return err_code;
        }
			
	
   	    /* Erase all unlocked blocks and check for error */
	  
        err_code = flash_chip_erase_K3RC28f128k 
                          ((volatile u16*) (start_addr + temp));		

        if (err_code != FLASH_STATUS_OK)
        {
#ifdef ZQ_DEBUG
            SerialOutputString("Error occurs when erasing the block.\n");
            SerialOutputString("And the error number is: ");
            SerialOutputDec(err_code);
            SerialOutputString("\n");
            SerialOutputString("The start address of the block is: ");
            SerialOutputHex(start_addr + temp);
            SerialOutputString("\n");

#endif /* ZQ_DEBUG */
			return err_code;
        }
    }
#ifdef ZQ_DEBUG
    SerialOutputString("The flash_erase_blocks will return normally\n");
#endif /* ZQ_DEBUG */
    return err_code;
}

/* check if the block is erased */
/* The speed of read is much faster than erase, but what is the case? */
/* if in most of the cases, the block is not erased, we can't get a high */
/* performance by calling this function */
static BOOLEAN is_flash_block_erased ( u16 *start_addr )
{

   BOOLEAN         sect_erased = TRUE;
   u32*            read_ptr = (u32*)((u32)start_addr);
   u32*            end_ptr  = (u32*)((u32)start_addr + K3_ONEBLOCK_SIZE);

   /* Flash must be in read array mode. */
/*   *start_addr = BLOADER_INTEL_READ_ARRAY;  */
            
   /* 
    *  Scan the flash area in blocks of 'ERASE_SCAN_BLOCK_SIZE' words,
    *  exit loop if a programmed cell is detected.
    */
    while( (*read_ptr++ == 0xFFFFFFFF) && (read_ptr < end_ptr) )
    {
    };
    read_ptr--;
    if( *read_ptr != 0xFFFFFFFF)
    {
        sect_erased = FALSE;
    }

   return( sect_erased );

}


