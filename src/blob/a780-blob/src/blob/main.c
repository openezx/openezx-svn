/*-------------------------------------------------------------------------
 * Filename:      main.c
 * Version:       $Id: main.c,v 1.23 2002/01/07 14:48:25 seletz Exp $
 * Copyright:     Copyright (C) 1999, Jan-Derk Bakker
 * Author:        Jan-Derk Bakker <J.D.Bakker@its.tudelft.nl>
 * Description:   Main file for the trivially simple bootloader for the
 *                LART boards
 * Created at:    Mon Aug 23 20:00:00 1999
 * Modified by:   Erik Mouw <J.A.K.Mouw@its.tudelft.nl>
 * Modified at:   Sat Mar 25 14:31:16 2000
 *-----------------------------------------------------------------------*/
/*
 * main.c: main file for the blob bootloader
 *
 * Copyright (C) 1999 2000 2001
 *   Jan-Derk Bakker (J.D.Bakker@its.tudelft.nl) and
 *   Erik Mouw (J.A.K.Mouw@its.tudelft.nl)
 *  Copyright (C) 2004-2005 Motorola
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Dec 13,2004 - (Motorola) Added support for command line config and reflash options
 *                          Changes made to support our platform
 * May 18,2005 - (Motorola) Changes to enable manual flash mode
 *
 */

#ident "$Id: main.c,v 1.23 2002/01/07 14:48:25 seletz Exp $"

#ifdef HAVE_CONFIG_H
# include <blob/config.h>
#endif

#include <blob/arch.h>
#include <blob/command.h>
#include <blob/errno.h>
#include <blob/error.h>
#include <blob/flash.h>
#include <blob/init.h>
#include <blob/led.h>
#include <blob/main.h>
#include <blob/md5.h>
#include <blob/md5support.h>
#include <blob/memory.h>
#include <blob/param_block.h>
#include <blob/serial.h>
#include <blob/time.h>
#include <blob/util.h>
#include <blob/uucodec.h>
#include <blob/xmodem.h>
#include <blob/time.h>
#include <blob/lcd_ezx.h>
#include <blob/mmu.h>
#include <blob/bt_uart.h>
#include <linux/string.h>
#include "usb_ctl_bvd.h"
#include "pcap.h"
/* kernel commandline config */
#define CMD_FLASH_FLAG   	0x01FE0400
#define CMD_FLASH_CONTENT   	0x01FE0600
#define CMD_FLAG_LEN   		512
#define CMD_LEN   		1024
#define CMD_FLAG   		"CMDL"
int cmd_flag = 0;


/* reflash option */
#define FLASH_TOOL_TEST_VERSION
//#define OUTPUT_LOG_TO_STPORT
BOOL check_pass_through_flag(void);
void enter_pass_through_mode(void);
BOOL check_flash_flag(void);
BOOL check_option1n2_shorted(void);
BOOL check_valid_code(void);
BOOL check_bb_wdi2_is_low(void);
void restart_bp(void);
void set_audio_ctl_to_bp(void);
/* check BP_WDI flag */
#define bp_flag_not_set()	(*(unsigned long *)(BPSIG_ADDR) == NO_FLAG)
#define bp_flag_is_set()	(*(unsigned long *)(BPSIG_ADDR) == WDI_FLAG)

#if 0 				/* use pcap.c defination */
void set_GPIO_mode(u32 gpio_mode)
{
    int gpio = gpio_mode & GPIO_MD_MASK_NR;
    int fn = (gpio_mode & GPIO_MD_MASK_FN)>>8;
    int gafr;

    if(gpio_mode & GPIO_MD_MASK_DIR)
    {
        GPDR(gpio) |= GPIO_bit(gpio);
    }
    else
    {
        GPDR(gpio) &= ~GPIO_bit(gpio);
    }

    gafr = GAFR(gpio) & ~(0x3<<(((gpio)&0xf)*2));
    GAFR(gpio) = gafr |  (fn <<(((gpio)&0xf)*2));
}
#endif

#ifdef FLASH_TOOL_TEST_VERSION

char value_trans_to_char(u8 data )
{
	char ret ='F';

        if(data>15 ||data <0)
		return ret;

        if( data <10) {
		ret = '0'+data;
        } else {
		ret = 'a'+data-10;
        }

        return ret;
}

int translate_xnum_to_string( u8 * pStr,u32 num)
{
        int i;

        pStr[8]=0;
        for(i=0;i<8;i++) {
		pStr[7-i] = value_trans_to_char(num%16);
		num = num/16;
        }
        return 0;
}

/* output reset information for debug */
void output_reset_inf(u32 sleepflag)
{
        u8 mystr[20];

        printlcd("\nFLAG=0x");
        translate_xnum_to_string( mystr,sleepflag);
        printlcd(mystr);

        printlcd("\nRESET=0x");
        translate_xnum_to_string( mystr,FFSPR);
        printlcd(mystr);

        printlcd("\nBRESET=0x");
        translate_xnum_to_string( mystr,PSPR);
        printlcd(mystr);

        printlcd("\nARESET=0x");
        translate_xnum_to_string( mystr,RCSR);
        printlcd(mystr);
}

/* save reset cause at previous time for debug */
void save_reset_inf(void)
{
        PSPR = RCSR;
}


#else

#define output_reset_inf(sleepflag)
#define save_reset_inf()

#endif

/* from E680/A780 EMU , we must have the feature  VA+slide for E680 and VA+scroll key for A780 to trigger flash mode */
BOOL manual_enter_flash_mode(void)
{
	BOOL rev = TRUE;
        u32 i;
        /*  gpio - 100 and gpio 106 comprised matrix key */
        if( (KPDK&KPDK_DK0) && (KPAS == (0x04000013)) )
        {
            STSerialOutputString("\nezx_test: REC button is pressed!!!.\n");
            rev = TRUE;
        }
        else
        {
            STSerialOutputString("\nezx_test: REC button is NOT pressed!!!.\n");
            rev = FALSE;
        }

	return rev;
}


#ifdef OUTPUT_LOG_TO_STPORT
void st_uart_init(void)
{
        CKEN |= 0x00000020;

	set_GPIO_mode(GPIO46_STRXD_MD);
        set_GPIO_mode(GPIO47_STTXD_MD);

	while(( BTLSR & (0x1 << 5)) == 0) ;

        STLCR = 0x83;
	STDLL = 0x08;
        STDLH = 0;
        STLCR = 0x03;

	/* disenable FIFO */
	STFCR = 0x06;
	/* enable UART  */
	STIER = 0x40;
        while((STLSR &0x40) ==0);
}


/*
 * Output a single byte to the serial port.
 */
void STSerialOutputByte(const char c)
{
	/* wait for room in the tx FIFO */
	while(( STLSR & 0x40) == 0) ;

	STTHR = c;

	/* If \n, also do \r */
	if(c == '\n')
	{
		while (( STLSR & 0x40) == 0);
			STTHR = '\r';
	}
}


/*
 * Write a null terminated string to the serial port.
 */
void STSerialOutputString(const char *s) {

	while(*s != 0)
		STSerialOutputByte(*s++);

} /* SerialOutputString */


/*
 * Write the argument of the function in hexadecimal to the serial
 * port. If you want "0x" in front of it, you'll have to add it
 * yourself.
 */
void STSerialOutputHex(const u32 h)
{
	char c;
	int i;

	for(i = NIBBLES_PER_WORD - 1; i >= 0; i--) {
		c = (char)((h >> (i * 4)) & 0x0f);

		if(c > 9)
			c += ('A' - 10);
		else
			c += '0';

		STSerialOutputByte(c);
	}
}


/*
 * Write the argument of the function in decimal to the serial port.
 * We just assume that each argument is positive (i.e. unsigned).
 */
void STSerialOutputDec(const u32 d)
{
	int leading_zero = 1;
	u32 divisor, result, remainder;

	remainder = d;

	for(divisor = 1000000000;
	    divisor > 0;
	    divisor /= 10) {
		result = remainder / divisor;
		remainder %= divisor;

		if(result != 0 || divisor == 1)
			leading_zero = 0;

		if(leading_zero == 0)
			STSerialOutputByte((char)(result) + '0');
	}
}

#else
void st_uart_init(void)
{
}
void STSerialOutputByte(const char c)
{
}

void STSerialOutputString(const char *s)
{
}

void STSerialOutputHex(const u32 h)
{
}

void STSerialOutputDec(const u32 d)
{
}

#endif

//USB_gpio_init(): set gpio for USB
void USB_gpio_init()
{
    set_GPIO_mode(GPIO34_TXENB_MD);
    set_GPIO_mode(GPIO35_XRXD_MD );
    set_GPIO_mode(GPIO36_VMOUT_MD);
    set_GPIO_mode(GPIO39_VPOUT_MD);
    set_GPIO_mode(GPIO40_VPIN_MD );
    set_GPIO_mode(GPIO53_VMIN_MD );

  //  UP2OCR =  UP2OCR_HXOE;
    UP2OCR =  0x02000000;		/* select single end port */
  //UP2OCR_HXS|UP2OCR_HXOE|UP2OCR_SEOS;
  __REG(0x40f00044)|=0x00008000;
}

void FFUART_gpio_init()
{
    set_GPIO_mode(GPIO53_FFRXD_MD);
    set_GPIO_mode(GPIO39_FFTXD_MD);
}

#ifdef SERIAL_OUTPUT
__initlist(FFUART_gpio_init, INIT_LEVEL_INITIAL_HARDWARE);
#else
__initlist(USB_gpio_init, INIT_LEVEL_INITIAL_HARDWARE);
#endif

void K3flash_sync_mode(void)
{
        volatile u32 tmp;
        int i;

//        *((u32*)0x48000008)=0xFFF812BB;
        *((u32*)0x48000008)=0x7FF015CA;
        tmp=*((u32*)0x48000008);

        *((u16*)0x00004b84)=0x0060;  // chip I, RCR first
        for(i=0; i<10; i++);
        *((u16*)0x00004b84)=0x0003;  // chip I, RCR second
        for( i=0; i<10; i++);

        *((u16*)0x01004b84)=0x0060;  // chip 2, RCR first
        for( i=0; i<10; i++);
        *((u16*)0x01004b84)=0x0003;  // chip 2, RCR second
        for(i=0; i<10; i++);

        *((u32*)0x4800001c)=0x000060f1;  // SXCNFG
        for( i=0; i<10; i++);
        *((u32*)0x48000004) |= (1 << 14) | ( 1<< 13 ) | ( 1 << 12 );  // MDREFR
        for( i=0; i<10; i++);
}

/*
 * In order to improve performance, MMU should be turned on.
 * Note: when flash code, the attribute of flash area should be uncachable, unbufferable
 */
struct mem_area io_map[]={
	{0x00000000, 	0x02000000, 	0x0e},	// flash, 32 M, B=1, C=1
	{0x08000000,	0x08100000,     0x2},	// DoC access window
	{0x40000000,	0x04000000,     0x2},	// io Memory Map
	{0x44000000,	0x04000000,     0x2},	// LCD
	{0x48000000,	0x04000000,     0x2},	// Memory Ctl
	{0x4C000000,	0x04000000,     0x2},	// USB host
	{0xA0000000, 	0x00F00000,     0x0a},	// SDRAM Bank 0, 15M, B=0, C=1
	{0xA0F00000, 	0x00100000,     0x02},	// SDRAM Bank 0, 1M, B=0, C=0 (DMA buf)
	{0xA1000000, 	0x01000000,     0x0a},	// SDRAM Bank 0, 16M, B=0, C=1
	{0xAC000000, 	0x01000000,     0x02},	// SDRAM Bank 3, 16M, B=0, C=0
	{0,	0,	0x0}			// end
};

blob_status_t blob_status;
static char *version_str = PACKAGE " version " VERSION " for " BOARD_NAME "\n";
static int do_reload(char *what);

int main(void)
{
	static char commandline[COMMAND_LINE_SIZE];
	int retval = 0;

	s32 waitTimeout;
        u32 flag = 0;
        u32 sleepflag;
        u32 i;

#ifdef PARAM_START
	u32 conf;
#endif
        st_uart_init();
        STSerialOutputString("\n test: test ST UART works! OSCR = ");
        STSerialOutputHex(OSCR);
        STSerialOutputString("\n");
	/* call subsystems */
	init_subsystems();
        K3flash_sync_mode(); // set flash memory into sync mode

	/* initialise status */
	blob_status.paramType = fromFlash;
	blob_status.kernelType = fromFlash;
	blob_status.ramdiskType = fromFlash;
	blob_status.downloadSpeed = baud_115200;
	blob_status.terminalSpeed = baud_115200;
	blob_status.load_ramdisk = 1;

	/* get command line, add by Levis */
	if(strncmp((char *)CMD_FLASH_FLAG, CMD_FLAG, sizeof(CMD_FLAG)-1) == 0)
	{
		cmd_flag = 1;
		memcpy(blob_status.cmdline, (char *)CMD_FLASH_CONTENT, COMMAND_LINE_SIZE);
	}else {
		cmd_flag = 0;
		blob_status.cmdline[0] = '\0';
	}

	blob_status.boot_delay = 10;

	/* call serial_init() because the default 9k6 speed might not
           be what the user requested */
#if defined(H3600) || defined(SHANNON) || defined(IDR) || defined(BADGE4) || defined(JORNADA720)
	blob_status.terminalSpeed = baud_115200; /* DEBUG */
#endif
#if defined(PT_SYSTEM3)
	blob_status.terminalSpeed = baud_38400;
#endif
	serial_init(blob_status.terminalSpeed);

	bt_uart_init();
	BTSerialOutputString("\n ezx_test: test BT UART works!!! OSCR = ");
	BTSerialOutputHex(*(u32*)OSCR);
	BTSerialOutputString("\n");

	/* parse the core tag, for critical things like terminal speed */
#ifdef PARAM_START
	parse_ptag((void *) PARAM_START, &conf);
#endif
	/* get the amount of memory */
	get_memory_map();
//	EnableLCD_8bit_active();

        sleepflag = *(unsigned long *)(FLAG_ADDR);
	if ( check_flash_flag() ) {
		*(unsigned long *)(FLAG_ADDR) = 0;
        	EnableLCD_8bit_active();
		printlcd("\n reflash flag is set ");
		goto reflash;
	}
	/* I2C fix set 120 GPIO low*/
        /* clear EMU MUX1/MUX2 (low) to close the audio path to EMU */
	set_GPIO_mode(GPIO_EMU_MUX1|GPIO_OUT);
        GPCR(GPIO_EMU_MUX1) = GPIO_bit(GPIO_EMU_MUX1);
	set_GPIO_mode(GPIO_EMU_MUX2|GPIO_OUT);
        GPCR(GPIO_EMU_MUX2) = GPIO_bit(GPIO_EMU_MUX2);
	/* a17400: add decision to pass-through mode */
	if(check_pass_through_flag())
	  enter_pass_through_mode();

#if 1
	/* config GPIO_BB_RESET as output high */
        set_GPIO_mode(GPIO_BB_RESET |GPIO_OUT);
        GPSR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);

        /* config MCU_INT_SW GPIO  as input */
        set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_IN);
#endif
        if ( !check_valid_code()|| manual_enter_flash_mode() ) {
		/* add by linwq  2003-01-20 */
		EnableLCD_8bit_active();
		printlcd("\n reset BP ");

		/* reset BP  and keep MCU_INT_SW as output low */
                set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);
                GPCR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);
		restart_bp();
		printlcd("\n end reset BP ");

		/* enter reflash mode */
		BTSerialOutputString("\nezx_test: BB_RESET and MCU_INT_SW are ");
		BTSerialOutputHex( GPLR0);
		BTSerialOutputHex( GPLR1);
		BTSerialOutputString("\nezx_test: Now, enter reflash mode.\n");

		output_reset_inf(sleepflag);

reflash:
		if (bp_flag_is_set())
			printlcd("\n BP assert BP_WDI before reset");
		if (bp_flag_not_set())
			printlcd("\n No BP_WDI before reset");
		*(unsigned long *)(BPSIG_ADDR) = 0;

		// clear flash section descriptor C,B bits for burn code
		set_uncache_unbuffer(&io_map[0]);

		printlcd("\n start reflash code! ");
		printlcd("\n\nEZX AP bootloader.\nVersion 3.0 2004-05-18");

		/* hwuart_init(230400); */
		for(; ;) {
			if(ICPR & 0x800) {
				udc_int_hndlr(0x11, 0);
			}
		}

		Disablelcd();
		Disablebklight();
    		return 0;
	}

	/* Load kernel and ramdisk from flash to RAM */
	do_reload("kernel");

	BTSerialOutputString("\n ezx_test: start check BP_RDY or MCU_INT_SW!!! ");

bprestart:
	waitTimeout = BP_RDY_TIMEOUT;
	while ( waitTimeout > 0 ) {

		if ( (!( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) ))
			|| (!(  GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) )) ) {
			break;
		}

		if(check_bb_wdi2_is_low()) {
			GPCR(GPIO_WDI_AP) = GPIO_bit(GPIO_WDI_AP);
			while(1);
		}

		waitTimeout--;
	}

	BTSerialOutputString("\n ezx_test: passed the check!!! ");

	if ( waitTimeout <= 0 ) {
#ifndef FLASH_TOOL_TEST_VERSION
                /*  try to restart bp one time !  */
	        if(0 == flag) {
			flag = 1;
                    restart_bp();
			goto bprestart;
		}
#endif
		/* enter reflash mode */
		EnableLCD_8bit_active();
		printlcd("\n timeout reset BP! ");
		BTSerialOutputString("\n ezx_test: jump to reflash mode since time out!!! ");

		/* reset BP  and keep MCU_INT_SW as output low */
        set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);
        GPCR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);
		restart_bp();
		printlcd("\ntimeout end reset BP ");

		output_reset_inf(sleepflag);

		// clear flash section descriptor C,B bits for burn code
		set_uncache_unbuffer(&io_map[0]);

        	/* enter reflash mode */
		BTSerialOutputString("\nezx_test: Now, enter reflash mode.\n");

		printlcd("\n timeoutstart reflash code! ");
		printlcd("\n\nEZX AP bootloader.\nVersion 3.0 2004-05-18");

		/* hwuart_init(230400); */
		for(; ;) {
			if(ICPR & 0x800) {
				udc_int_hndlr(0x11, 0);
			}
		}

		Disablelcd();
		Disablebklight();

    		return 0;
	}

//	printlcd("start kernel ...\n");
//	Disablelcd();
//	Disablebklight();
//	parse_command("boot");
//	printlcd("\nNow start linux...\n");
        STSerialOutputString("\nezx_test: Now, boot linux!\n");
        BTSerialOutputString("\nezx_test: Now, boot linux!\n");

	save_reset_inf();

	if(retval == 0) {
		parse_command("boot");
	}

	return 0;
} /* main */

BOOL check_flash_flag(void)
{
	return (*(unsigned long *)(FLAG_ADDR) == REFLASH_FLAG);
}

BOOL check_bb_wdi2_is_low(void)
{
        u32 i;
        set_GPIO_mode(GPIO_BB_WDI2 | GPIO_IN );

        for(i=0; i<1000; i++);

        for(i=0; i<3; i++) {
		if(GPLR(GPIO_BB_WDI2) & GPIO_bit(GPIO_BB_WDI2)) {
			return FALSE;
		}
	}

        return TRUE;
}


static int do_reload(char *what)
{
	u32 *dst = 0;
	u32 *src = 0;
	int numWords;

	if(strncmp(what, "blob", 5) == 0) {
		dst = (u32 *)BLOB_RAM_BASE;
		src = (u32 *)BLOB_FLASH_BASE;
		numWords = BLOB_FLASH_LEN / 4;
		blob_status.blobSize = 0;
		blob_status.blobType = fromFlash;
		//SerialOutputString("Loading blob from flash ");
#ifdef PARAM_START
	} else if(strncmp(what, "param", 6) == 0) {
		dst = (u32 *)PARAM_RAM_BASE;
		src = (u32 *)PARAM_FLASH_BASE;
		numWords = PARAM_FLASH_LEN / 4;
		blob_status.paramSize = 0;
		blob_status.paramType = fromFlash;
		//SerialOutputString("Loading paramater block from flash ");
#endif
	} else if(strncmp(what, "kernel", 7) == 0) {
		dst = (u32 *)KERNEL_RAM_BASE;
		src = (u32 *)KERNEL_FLASH_BASE;
		numWords = KERNEL_FLASH_LEN / 4;
		blob_status.kernelSize = 0;
		blob_status.kernelType = fromFlash;
		//SerialOutputString("Loading kernel from flash ");
	} else if(strncmp(what, "ramdisk", 8) == 0) {
		dst = (u32 *)RAMDISK_RAM_BASE;
		src = (u32 *)RAMDISK_FLASH_BASE;
		numWords = RAMDISK_FLASH_LEN / 4;
		blob_status.ramdiskSize = 0;
		blob_status.ramdiskType = fromFlash;
		//SerialOutputString("Loading ramdisk from flash ");
	} else {
		printerror(EINVAL, what);
		return 0;
	}

	MyMemCpy(dst, src, numWords);
	//SerialOutputString(" done\n");

	return 0;
}

void restart_bp(void)
{
	int waitTimeout;
        GPCR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);

        waitTimeout = BB_RESET_TIMEOUT;
        while ( waitTimeout > 0 )
            waitTimeout--;
        GPSR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);
}


BOOL check_valid_code(void)
{
	BOOL rev = TRUE;

#if 0	// wait for ZQ
	u32* pbarkercode = BARKER_CODE_ADDRESS;

        if ( (*pbarkercode) != BARKER_CODE_VALID_VAL )
        {
            rev = FALSE;
        }
#endif
	return rev;
}
/*
 check_pass_through_flag()
 check if flag in memory for pass through mode is set
 return TRUE: pass-through mode
 return FALSE: no pass-thru mode
*/
#define PASS_THRU_FLAG_ADDR FLAG_ADDR
#define PASS_THRU_FLAG 0x12345678
BOOL check_pass_through_flag(void){
#if 1
  return (*(unsigned long *)(PASS_THRU_FLAG_ADDR) == PASS_THRU_FLAG);
#else  /* just for test phase */
  return TRUE;
#endif
}

/*
 enter_pass_through_mode
 enter USB pass_through mode and do all initial work for emulator.
 blob will stay in this function in life time
*/
void enter_pass_through_mode(void){
  EnableLCD_8bit_active();
  printlcd("b1213_1.\n ");
  //  STSerialOutputString("Enter pass-through mode.\n ");
  printlcd("Enter pass-through mode.\n ");

#if 0  //wave form test.we output square wave to test validaty of pin under test
 {
   u32 gpio=GPIO_BP_RDY;
   set_GPIO_mode(gpio | GPIO_OUT);

   while(1){
     u32 i=0x80000;

     while(i<=0)
       i--;
     GPSR(gpio) = GPIO_bit(gpio); /* hign level output */

     i=0x80000;			/* restore timeout */

     while(i<=0)
       i--;
     GPCR(gpio) = GPIO_bit(gpio); /* low level output */



   }
 }


#endif //end waveform test

  //step 1 finish last BP hand shake
#if 1   //we do handshake step 1 again, to make sure
  //   set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);
  //  GPSR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);
    //reset BP step by step
  GPSR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);

  GPCR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);

    set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);

    GPSR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);

    printlcd("bpreset low\n");
    //   restart_bp();
    //  set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_IN);

    {
      int i=BB_RESET_TIMEOUT;
      while(i>0)
	i--;

    }
    if(!( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) ))
      printlcd("error sw low\n");
    set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_IN);
    GPSR(GPIO_BB_RESET) = GPIO_bit(GPIO_BB_RESET);
    printlcd("bpreset high\n");

    while( (( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) ))
	   && ((  GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) )) );
#endif
  printlcd("pass-thru stab 0.\n ");
  while(GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) );

#if 0
  while(1);
#endif
  /* wait some times before clear MCU_INT_SW */
  //  STSerialOutputString("pass-through mode. stab 1.\n ");
 {
   u32 i=0x1;
   u32 count=0xccccccc;
   while (i>0){
     while(count>0){

       count--;
     }
     i--;

   }
 }
  printlcd("pass-thru stab 1.\n ");
  //config MCU_INT_SW as output low

  set_GPIO_mode(GPIO_MCU_INT_SW | GPIO_OUT);
  GPCR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);

  while(!(GPLR(GPIO_BP_RDY) & GPIO_bit(GPIO_BP_RDY) ))
    {
#if 0
      static int i=0;
      if(!( GPLR(GPIO_MCU_INT_SW) & GPIO_bit(GPIO_MCU_INT_SW) )&& i<=10){


	   printlcd("MCU_int_sw=low\n");
      i++;

      }
#endif

    }

  //  STSerialOutputString(" pass-through mode. stab 2.\n ");
  //  printlcd(" pass-thru stab 2.\n ");
  //config MCU_INT_SW as output high
  GPSR(GPIO_MCU_INT_SW) = GPIO_bit(GPIO_MCU_INT_SW);
  printlcd(" Handshake with BP...Pass\n ");
  printlcd(" init USB gpio\n ");


#if 0
  while(1);
#endif
//step 2 set USB to pass-through mode

  usb_gpio_pass_through_init();

  EnableLCD_8bit_active();
  printlcd("Now blob in\npass-through mode\n");
 {
   u32 i=0x1;
   u32 count=0xccccccc;
   while (i>0){
     while(count>0){

       count--;
     }
     i--;

   }
 }
  pcap_switch_off_usb();
  {
    int i=5000;
    while(i--);

  }
  //  printlcd(" Auto plug USB cable\n ");
  pcap_switch_on_usb();

  //open audio
  set_audio_ctl_to_bp();
  /* now enter pass-through mode successfully, clear flag bit */
  *(unsigned long *)(PASS_THRU_FLAG_ADDR) =OFF_FLAG;
  while(1);
  logstr("should not see me...!!!");
}

//this function just set usb to pass-through mode
//for general flash setting, see usb_gpio_init()
void usb_gpio_pass_through_init(void){
  GPCR3=0x40E00124;
  GPDR0=0xC0000000;
  GPDR1=0x01000094;
  GPDR2=0x00040000;
  GPDR3=0x01800000;
  GAFR0_U=0xF0000000;
  GAFR1_L=0x00034190;
  GAFR1_U=0x00010800;
  GAFR2_U=0x00A00000;
  GAFR3_U=0x0000000C;
  PSSR=0x00000020;
  UP2OCR=0x01000000;
  __REG(0x40600024)=1;


}

void set_audio_ctl_to_bp(void){
  SSP_PCAP_write_data_to_PCAP(11,0x3005);
  SSP_PCAP_write_data_to_PCAP(13,0x0);
  SSP_PCAP_write_data_to_PCAP(12,0x90901);
  SSP_PCAP_write_data_to_PCAP(26,0x200910);


}
