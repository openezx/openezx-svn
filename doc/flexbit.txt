General:
0x4B - Voice Recognizer
0x92 - P2P
0x9B - SIP
0xAA - WVIMPS IM client

Debug:
0x3F - start ap_tcmd or not

Setup:
0x68 - USB Net mode available (AT+MODE=11, not 99)
0x9A - Content Channels available

Opera:
0x6F - Browser Cache
0x7E - "Additional charges may apply" warning on startup
Unknown: 0E, 12, 18, 58, 5E, 5F
Unknown: 70, 78, 79, 7A, 7B, 7C, 7D, 80, 83, 84, 87, 8B, A4, A6, A8

The formula to test a flex bit is:
Line = floor(Bit / 32)
BitSet = (Value[Line] & (1 << (31 - (Bit % 32))))
