/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "p2kmoto.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <usb.h>

#include <time.h>

#ifdef UNIX_API
#include <termios.h>
#include <fcntl.h>
#endif

#ifdef WIN32_API
#include <windows.h>
#endif



//#define P2K_DEBUG
#define MY_DEBUG

// Intefaces for Motorola P2k phones
#define INTERFACE_ACCESSORIES 0x05
#define INTERFACE_DATA_LOGGING_MCU 0x06
#define INTERFACE_TEST_COMMAND 0x08

// Default Phone Vendor/Product ID
#define DEF_PHONE_AT_VENDOR 0x22b8
#define DEF_PHONE_AT_PRODUCT 0x4902
#define DEF_PHONE_P2K_VENDOR 0x22b8
#define DEF_PHONE_P2K_PRODUCT 0x4901

// Transfer direction
#define DIR_IN 0
#define DIR_OUT 1

#define VENDOR_CONTAINS "Motorola"
#define PRODUCT_CONTAINS "Motorola"

#ifdef P2K_DEBUG
#define FUNC(val)\
	char FUNC_NAME[]=val;\
	printf("=== [P2k API: %s]\n", val);
#define DEBUG( ... )\
	printf(__VA_ARGS__);
#else
#define FUNC(val)\
	char FUNC_NAME[]=val;
#define DEBUG(...) ;
#endif

#define RAISE(a, ...)\
{\
	printf("(E_%s.%d: %s)\n", FUNC_NAME, a, __VA_ARGS__);\
	return(a);\
}

#ifdef UNIX_API
#define SLEEP(val) usleep(val*1000);
#endif

#ifdef WIN32
#define SLEEP(val) sleep(val);
#endif

#define INT16 uint16_t
#define INT32 uint32_t

struct p2k_devList p2k_dev[]={
                   {0x0, 0x0, "NONE"},
                   {DEF_PHONE_AT_VENDOR, DEF_PHONE_AT_PRODUCT, "AT"},
                   {DEF_PHONE_P2K_VENDOR, DEF_PHONE_P2K_PRODUCT, "P2K"}
//                    (DEF_PHONE_FLASH_VENDOR, DEF_PHONE_FLASH_PRODUCT, "FLASH")
               };

// Phone device/handle
struct usb_device *phone;
usb_dev_handle *phoneHandle;
INT16 freeID;
unsigned char packetCount[16*2+2];
int p2k_cnt;
char ACMdevice[256];
char ACMcmd[]="AT+MODE=8\r\n";
char p2k_debug;
// int lastCnt;

struct flash_device flash_dev;
usb_dev_handle *flash_udev;
char flash_outBuf[10240];
char flash_inpBuf[10240];
char flash_wasCmd[1024];

#define ERR(val) { printf("%s\n%s\n",val, usb_strerror()); return -1; };


INT16 getInt16(unsigned char *s)
{
	INT16 res;
	res=s[0];
	res<<=8; res+=s[1];
	return (res);
}

INT32 getInt32(unsigned char *s)
{
	INT32 res;
	res=s[0];
	res<<=8; res+=s[1];
	res<<=8; res+=s[2];
	res<<=8; res+=s[3];
	return (res);
}

void setInt16(unsigned char *s, INT16 value)
{
	s[1]=value%0x100; value/=0x100;
	s[0]=value%0x100;
}

void setInt32(unsigned char *s, INT32 value)
{
	s[3]=value%0x100; value/=0x100;
	s[2]=value%0x100; value/=0x100;
	s[1]=value%0x100; value/=0x100;
	s[0]=value%0x100;
}

// Print array in hex mode
void p2k_showArr(unsigned char *bytes, int size)
{
#ifdef P2K_DEBUG
#define BYTES_IN_LINE 16

	int i;
	if (size==0) return;

	printf("   -------");
	for (i=0; i<16; i++)
	{
		printf("%3X",i);
	}
	printf("\n");

	for (i=0; i<size; i++)
	{
		if (i % 16 ==0)
		{
			if (i!=0) printf("\n");
			printf("   %06x: ",i);
		}
		printf("%02x ",bytes[i]);
	}
	printf("\n");
#endif
}

// Init lib
void p2k_init()
{
	usb_init();
	usb_find_busses();
	usb_find_devices();
	freeID=0;
	p2k_debug=0;
	p2k_cnt=-1;
}

// Set ACM device
void p2k_setACMdevice(const char * dev)
{
	strcpy(ACMdevice, dev);
}

// Set AT config
void p2k_setATconfig (unsigned int vendor, unsigned int product)
{
	p2k_dev[1].vendor=vendor;
	p2k_dev[1].product=product;
}

// Set P2K config
void p2k_setP2Kconfig (unsigned int vendor, unsigned int product)
{
	p2k_dev[2].vendor=vendor;
	p2k_dev[2].product=product;
}

int p2k_getATvendor()
{
	return p2k_dev[1].vendor;
}

int p2k_getATproduct()
{
	return p2k_dev[1].product;
}

int p2k_getP2Kvendor()
{
	return p2k_dev[2].vendor;
}

int p2k_getP2Kproduct()
{
	return p2k_dev[2].product;
}

void p2k_getACMdevice(char *dev)
{
	strcpy(dev,ACMdevice);
}


// Detects phone
int p2k_detectPhone()
{
	struct p2k_devInfo * usbDevices;
	
	usbDevices=p2k_getDevList();
	while (usbDevices->vendor >0)
	{
		if (strstr(usbDevices->manufacturerStr, VENDOR_CONTAINS) &&
			strstr(usbDevices->productStr, PRODUCT_CONTAINS))
		{
			if (usbDevices->product % 2 == 1)
				usbDevices->product++;
			p2k_setATconfig(usbDevices->vendor, usbDevices->product);
			p2k_setP2Kconfig(usbDevices->vendor, usbDevices->product-1);	
			return 0;
		}
		usbDevices++;
	}
	return -1;
}

// Get device list. Phone must not be opened
struct p2k_devInfo* p2k_getDevList()
{
	usb_find_busses();
	usb_find_devices();

	struct usb_bus *bus;
	struct usb_device *dev;
	usb_dev_handle *udev;

	int idx=0;
	unsigned int devVendor;
	unsigned int devProduct;
	char devManufacturerStr[200];
	char devProductStr[200];
	int ret;
	struct p2k_devInfo* lst;

	int cnt=0;

	for (bus = usb_busses; bus; bus = bus->next)
		for (dev = bus->devices; dev; dev = dev->next)
			cnt++;

	lst=(struct p2k_devInfo *) malloc (sizeof(struct p2k_devInfo) * (cnt+1));
	lst[cnt].vendor=-1;
	lst[cnt].product=-1;

	for (bus = usb_busses; bus; bus = bus->next)
	{
		for (dev = bus->devices; dev; dev = dev->next)
		{
			devVendor=dev->descriptor.idVendor;
			devProduct=dev->descriptor.idProduct;
			devManufacturerStr[0]=0;
			devProductStr[0]=0;

			udev = usb_open(dev);
			if (udev)
			{
				if (dev->descriptor.iManufacturer)
					ret = usb_get_string_simple(udev, dev->descriptor.iManufacturer,
					                            devManufacturerStr, sizeof(devManufacturerStr));

				if (dev->descriptor.iProduct)
					ret = usb_get_string_simple(udev, dev->descriptor.iProduct,
					                            devProductStr, sizeof(devProductStr));
				usb_close (udev);
			};

			lst[idx].vendor=devVendor;
			lst[idx].product=devProduct;
			strcpy(lst[idx].manufacturerStr, devManufacturerStr);
			strcpy(lst[idx].productStr, devProductStr);
			idx++;
		}
	}

	return lst;
}


// Find device by Vendor ID & Product ID
struct usb_device * p2k_findDevice(int vendor, int product)
{
	struct usb_bus *busses;
	struct usb_bus *bus;

	usb_find_busses();
	usb_find_devices();

	busses = usb_get_busses();

	for (bus = busses; bus; bus = bus->next)
	{
		struct usb_device *dev;

		for (dev = bus->devices; dev; dev = dev->next)
			if ((dev->descriptor.idVendor==vendor) && (dev->descriptor.idProduct==product))
				return(dev);
	}
	return(0x00);
}

// Return Current phone state
int p2k_findPhone()
{
	int i;
	for (i=1; i<=2; i++)
	{
		phone=(p2k_findDevice(p2k_dev[i].vendor, p2k_dev[i].product));
		if (phone)  return(i);
	}
	return(P2K_PHONE_NONE);
}

#ifdef UNIX_API
void p2k_setP2Kmode_UNIX()
{
	long acm;
		
	acm=open(ACMdevice, O_RDWR | O_NOCTTY | O_NONBLOCK );
	struct termios tio;
	bzero(&tio, sizeof(tio));
	tio.c_cflag= CRTSCTS | CS8 | CLOCAL | CREAD | O_NDELAY;
	cfsetispeed(&tio, 115200);
	tio.c_cflag &= ~CRTSCTS;
	tio.c_iflag = IGNPAR;
	tio.c_oflag = 0;
	tio.c_lflag = 0;
	tio.c_cc[VTIME] = 1;
	tio.c_cc[VMIN] = 0;
	tcflush(acm, TCIOFLUSH);
	tcsetattr(acm, TCSANOW, &tio);
	write(acm, ACMcmd, strlen(ACMcmd));
	tcdrain(acm);
	close(acm);
}
#endif

#ifdef WIN32_API
void p2k_setP2Kmode_WIN32()
{
	DCB dcb;
	HANDLE hCom;
	BOOL fSuccess;
	char *pcCommPort = ACMdevice;

	hCom = CreateFileA ( pcCommPort,
                    GENERIC_READ | GENERIC_WRITE,
                    0,    // must be opened with exclusive-access
                    NULL, // no security attributes
                    OPEN_EXISTING, // must use OPEN_EXISTING
                    0,    // not overlapped I/O
                    NULL  // hTemplate must be NULL for comm devices
                    );

   if (hCom == INVALID_HANDLE_VALUE) 
   {
       // Handle the error.
//        qDebug ("CreateFile failed with error %d.\n", GetLastError());
       return (0);
   }

   fSuccess = GetCommState(hCom, &dcb);

   if (!fSuccess) 
   {
      // Handle the error.
      printf ("GetCommState failed with error %d.\n", GetLastError());
      return (0);
   }

   // Fill in DCB: 57,600 bps, 8 data bits, no parity, and 1 stop bit.

   dcb.BaudRate = CBR_57600;     // set the baud rate
   dcb.ByteSize = 8;             // data size, xmit, and rcv
   dcb.Parity = NOPARITY;        // no parity bit
   dcb.StopBits = ONESTOPBIT;    // one stop bit

   fSuccess = SetCommState(hCom, &dcb);

   if (!fSuccess) 
   {
      // Handle the error.
      printf ("SetCommState failed with error %d.\n", GetLastError());
      return (0);
   }

//    qDebug ("Serial port %s successfully reconfigured.", pcCommPort);
 
   char s[100]="AT+MODE=8\r\n";
   unsigned long l1=strlen(s);
   unsigned long l2;

   WriteFile(hCom, s, l1, &l2, 0);

   return 1;
}
#endif

#ifdef DARWIN_API
void p2k_setP2Kmode_DARWIN()
{
	struct usb_bus *bus;
	struct usb_device *dev;
	usb_dev_handle *udev =NULL;
	int flag=0, res=0,timeout=10000,i,x;
        char buf[256]; 
	memset (buf,0,sizeof(buf));
	
	usb_init();
	usb_find_busses();
	usb_find_devices();

	for (bus = usb_busses; bus; bus = bus->next)
		for (dev = bus->devices; dev; dev = dev->next)
			if(dev->descriptor.idVendor == p2k_getATvendor && dev->descriptor.idProduct == p2k_getATproduct)
				udev = usb_open(dev);
	if (!udev)
	{
		printf("Phone not found\n");
		return 0;
        }
       // if (usb_detach_kernel_driver_np(udev, 0x01)) printf("Unable to detach kernel driver");
       // if (usb_set_configuration(udev,0x01)) printf("Unable to set configuration");
      //  if (usb_claim_interface(udev, 0x01)) printf("Unable to claim the interface");
	memset (buf,0,sizeof(buf));
        strcpy(buf,"AT+MODE=8\r\n");
        res=usb_bulk_write(udev,0x01,buf,strlen(buf),timeout);
        if (res>0)
        {
               	printf ("OK. bytes: %d <-\n", res );
        	do
        	{
                	memset(buf,0,sizeof(buf));
                	res=usb_bulk_read(udev,0x82, buf, sizeof(buf),timeout);
                	if (res<0)
                	{
                        	if (flag>0)
                        	{
                                	break;
                        	} else {
                                	flag=1;
                        	}
                	} else {
				printf("Answer: %s\n", buf);
                	}

        	}
        	while ((strstr(buf, "\nOK\r") == NULL) & (strstr(buf, "ERROR") == NULL));
         } else {
          	printf("WE: %s\n", usb_strerror());
          	usb_release_interface(udev, 0x01);
          	usb_close (udev);
        }
}
#endif

int p2k_setP2Kmode(int timeout)
{
	int stat=p2k_findPhone();
	if (stat == P2K_PHONE_NONE) return (-1);
	if (stat == P2K_PHONE_P2K) return (0);
	if (stat == P2K_PHONE_AT)
	{
#ifdef UNIX_API
		p2k_setP2Kmode_UNIX();
#endif
#ifdef WIN32_API
		p2k_setP2Kmode_WIN32();
#endif
#ifdef DARWIN_API
		p2k_setP2Kmode_DARWIN();
#endif
	}
	int t;
	t=time(NULL);
	while (time(NULL)-t <= timeout)
	{
		if (p2k_findPhone() == P2K_PHONE_P2K)
			return P2K_PHONE_P2K;
		SLEEP(100);
	}
	return -1;
	
}

// Connect to Phone. Call after p2k_findPhone
int p2k_connect(int timeout)
{
	int t;
	int success=0;
	
	FUNC("p2k_connect");
	phoneHandle=0;
    
	printf("1111\n");	
//	t=time(NULL);
//	do
//	{	
		phoneHandle = usb_open(phone);
		if (phoneHandle==0) RAISE(P2K_E_CANT_OPEN, "Unable to open phone\n");
//		if (!usb_set_configuration(phoneHandle,1))
//		{
//			success=1;
//			break;
//		}
//		usb_close(phoneHandle);
//		SLEEP(100);
//	} while (time(NULL)-t <= timeout);
	
//	if (!success) RAISE(P2K_E_CANT_SETCONFIG, "Unable to set configuration");
	
//	if (usb_claim_interface(phoneHandle, INTERFACE_TEST_COMMAND)) RAISE(P2K_E_CANT_CLAIMIFACE, "Unable to claim the interface");
	

	printf("22222\n");

	return(0);
}

// Open phone
int p2k_openPhone(int timeout)
{
	FUNC("p2k_openPhone");
	printf("ACM Device: %s\n",ACMdevice);
	printf("AT  Device: %04x:%04x\n",p2k_dev[1].vendor, p2k_dev[1].product);
	printf("P2K Device: %04x:%04x\n",p2k_dev[2].vendor, p2k_dev[2].product);
	int ph=p2k_findPhone();
	if (ph!=P2K_PHONE_P2K) RAISE(P2K_E_NOPHONE, "no p2k phone");
	return(p2k_connect(timeout));
}

// Close phone
int p2k_closePhone()
{
	FUNC("p2k_closePhone");
	if (phoneHandle<=0) return(-567);
	if (usb_close(phoneHandle)) RAISE(P2K_E_CANT_CLOSE, "Unable to close phone");
	return(0);
}

// Calculate phone answer size
int p2k_get_cmd_size(unsigned char * packetCount)
{
	INT16 cnt;
	INT16 size;
	int bufsize;
	cnt=getInt16(packetCount);
	size=getInt16(packetCount+2);
	bufsize=2*cnt+size+4;
	return(bufsize);
}

// Send control message to device, but with debug loging and error handling
// Return value is like usb_control_msg() from libusb
int p2k_sendControl(int dir, usb_dev_handle *dev, int requesttype, int request, int value,
                    int index, char *bytes, int size, int timeout)
{
	FUNC("p2k_sendControl");
	if (dev==0) RAISE(P2K_E_NOT_CONNECTED, "no connection");
	int ret;
#ifdef P2K_DEBUG
	if (dir==DIR_IN) printf("<==== "); else printf("====> ");
	printf("Control Message\n");
	printf("   requesttype=0x%02x, request=0x%02x, value=0x%02x, index=0x%02x, size=0x%02x (%04d), timeout=%04d\n",
	       requesttype, request, value, index, size, size, timeout);
	if (dir==DIR_OUT) p2k_showArr((unsigned char *)bytes, size);
#endif
	ret=usb_control_msg(dev, requesttype, request, value, index, bytes, size, timeout);
#ifdef P2K_DEBUG
	printf("   result=%04d",ret);
#endif
	if (ret<0) printf(" Error:[%s]\n",usb_strerror());
#ifdef P2K_DEBUG
	else printf("\n");
	if ((dir==DIR_IN) && (ret>0)) p2k_showArr((unsigned char *)bytes,ret);
	printf("\n");
#endif
	return(ret);
}

// Add header and set data to phone via p2k_sendControl
int p2k_outData(unsigned char * data, unsigned int size)
{
	/*
	41 02 00 00 08 00 x1 x1		- Setup packet
	x2 x2 x3 x3 x4 x4 00 00		- data
	x1 - ALl packet size 
	x2 - ID - packet counter
	x3 - command. for reboot = 22
	x4 - command arguments size 
	*/

	// Set Packet ID
	freeID++;
	freeID&=0xFFF;
	setInt16(data,freeID);
	
	return(p2k_sendControl(DIR_OUT, phoneHandle, 0x41, 0x02, 0x00, 0x06, (char*) data, size,1000));
}

// Request Packet Count
int p2k_inpSize(unsigned char * cmd, int size)
{
	/* Getting info about answer packets
	�1 00 00 00 08 00 08 00 */
	int t;
	int ret;

	t=time(NULL);

#ifdef P2K_DEBUG
	SLEEP(20);
#endif
	// Waiting 5 sec for each answer.
	while (time(NULL)-t<5)
	{
		ret = p2k_sendControl(DIR_IN, phoneHandle, 0xc1, 0x00, 0x00, 0x06, (char *)cmd, size, 1000);
		if ((ret<0) || (ret>2)) break;
#ifdef P2K_DEBUG
		else printf("Error answer. try again\n");
#endif
		SLEEP(10);
	}
	return(ret);

}

// Get data from phone and perform error handling
int p2k_inpData(unsigned char * data, unsigned int count, unsigned int size)
{
	FUNC("p2k_inpData");
	/* Getting phone answer:
	�1 01 x1 x1 08 00 x2 x2
	x1 - count of packet
	x2 - answer size

	Answer:
	0  1  2  3  4  5   6  7  8  9  0  1  2  3  4  5  6  6  7
	01 00 00 00 00 87  80 03 80 4a 00 7f 00 00 10
	01 00 00 00 00 09 80  04  80  4a 00 01 00 00 08
	x1 ?? ?? ?? x2 x2 [x3 x3 x4 x4 x5 x5 ?? ?? yy yy yy yy yy] ...
	x1 - Count of answers;
	x2 - Total size of answers
	x3 - PacketID or 8000h
	x4 - Only first byte!. 60 - Ok, no data, 80 - Ok, with data, other - errors
	x5 - packet size
	yy - packet data
	[] - Anser for packet
	*/

	int ret=p2k_sendControl(DIR_IN , phoneHandle, 0xc1, 0x01, 0x01, 0x06, (char*)data, size, 1000);
	if (ret<0) return (ret);

	int err;
	INT16 x2;
	x2=getInt16(data+4);
	
	err=0;
	if (err+=(data[0]!=count)) RAISE(P2K_E_ANSWER_E001, "E001");
	if (err+=(x2!=size-6)) RAISE(P2K_E_ANSWER_E001, "E002");
	return(ret);
}

// Check answer. Some error control
int p2k_check_packet_header(unsigned char * buf, int bufsize, int adr, char needdata, char checky)
{
	FUNC("CPH");

	INT16 packId;
	INT16 packSize;
	int myid;
	unsigned char packType;
	unsigned char iserr;
	packId=getInt16(buf+adr);
	
	myid= packId & 0xFFF;

	packType=buf[adr+2];
	packSize=getInt16(buf+adr+4);
	iserr=buf[adr+8];

	if (bufsize<0) RAISE(P2K_E_CPH_00, "E00");

	if (myid != freeID) RAISE(P2K_E_CPH_01, "E01");
	if (needdata)
	{
		if (packType!=0x80) RAISE(P2K_E_CPH_02a, "E02a");
	}
	else
	{
		if (packType!=0x60) RAISE(P2K_E_CPH_02b, "E02b");
	}
	if (checky && iserr) RAISE(P2K_E_CPH_03, "E03");
	return(packSize);
}

// Reboot phone.
int p2k_reboot()
{
	FUNC("p2k_reboot");
	//This is reboot p2k command
	unsigned char cmd1[]={0xFF, 0xFF, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00};
	unsigned char cmd2[0x0e];
	if (p2k_outData(cmd1, sizeof(cmd1))<0) RAISE(P2K_E_OUTDATA, "E001");
	if (p2k_inpSize(packetCount, sizeof(packetCount))<0) RAISE(P2K_E_INPSIZE, "E002");
	if (p2k_inpData(cmd2, 1, sizeof(cmd2))<0) RAISE(P2K_E_INPDATA, "E003");
	return(0);
}

int p2k_flashMode()
{
	FUNC("p2k_flash");
	unsigned char cmd1[]={0xFF, 0xFF, 0x00, 0xD, 0x00, 0x00, 0x00, 0x00};
	unsigned char * tmp;
	int sz;
	if (p2k_outData(cmd1, sizeof(cmd1))<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))<0) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	free(tmp);
	return(0);	
}

// Suspend phone
int p2k_suspend()
{
	FUNC("p2k_suspend");
	unsigned char cmd1[]={0xFF, 0xFF, 0x00, 0x36, 0x00, 0x01, 0x00, 0x00,0x00};
	unsigned char * tmp;
	int sz;
	if (p2k_outData(cmd1, sizeof(cmd1))<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))<0) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	free(tmp);
	return(0);
}

// Get drive information
int p2k_getDriveNames(unsigned char * buf)
{
	FUNC("p2k_getDriveName");

	//TODO: Support for E398 and other phones with Memory Flash card.
	//Temporary returns all drives separated by spaces. So, software must handle this

	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A};
	unsigned char * tmp;
	int sz;
	int buf_ps;
	int ps;
	// 0x00 0x4A - command code

	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E001")
		// 	usleep(100000);
		if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE,"E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp, sz, 6, 1, 1)<0) RAISE(P2K_E_CPH, "E004");
	buf_ps=0;
	ps=15;
	while (tmp[ps]!=0)
	{
		if (tmp[ps]==0xfe) tmp[ps]=0x20; //space
		buf[buf_ps++]=tmp[ps];
		ps+=2;
	}
	buf[buf_ps++]=0;
	free(tmp);
	return(0);
}

// Get phone model.
int p2k_getPhoneModel(unsigned char * buf)
{
	FUNC("p2k_getPhoneName");

	unsigned char cmd[0x10]={0xFF, 0xFF, 0x00, 0x20, 0x00, 0x08, 0x00, 0x00, 0x01, 0x17, 0x00, 0x01, 0x00, 0x00, 0x00};
	// 00 02 00 20 00 08 00 00 01 17 00 01 00 00 00 00
	unsigned char * tmp;
	int sz;
	int buf_ps;
	int ps;
	// 0x00 0x4A - Command code

	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E001");
	// 	usleep(100000);
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp, sz, 6, 1, 1)<0) RAISE(P2K_E_CPH, "E004");
	buf_ps=0;
	ps=0x10;
	while (tmp[ps]!=0)
	{
		buf[buf_ps++]=tmp[ps];
		ps+=2;
	}
	buf[buf_ps++]=0;

	free(tmp);
	return(0);
}

// Return Free space in device
int p2k_freeSpace(unsigned char * dev)
{
	FUNC("p2k_freeSpace");

	//TODO: Check for E398 Support

	unsigned char cmd[0x108] ={0xFF, 0xFF, 0x00, 0x4A, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x00};
	unsigned char * tmp;
	int i;
	int ps;
	int sz;
	int size=strlen((char*)dev);

	INT32 lng;

	ps=13;
	for (i=0; i< size; i++)
	{
		cmd[ps++]=dev[i];
		cmd[ps++]=0;
	}
	cmd[ps++]=0;
	cmd[ps++]=0;

	if (p2k_outData(cmd, sizeof(cmd))<=0) RAISE(P2K_E_OUTDATA, "E001");
	// 	usleep(100000);
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp, sz, 6, 1, 1)<0) RAISE(P2K_E_CPH, "E004");

	lng=getInt32(tmp+14);
	free(tmp);

	return(lng);
}

// Get File Count
int p2k_fileCount()
{
	
	FUNC("p2k_fileCount");

	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07};
	
	unsigned char * tmp;
	int sz;
	INT16 t;

	p2k_cnt=-1;

	if (p2k_outData(cmd, sizeof(cmd))<=0) RAISE(P2K_E_OUTDATA, "E001");
	// 	usleep(100000);
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);

	if (p2k_inpData(tmp, 0x01, sz)<0) RAISE(P2K_E_INPDATA, "E003");

	t=getInt16(tmp+14);
	free(tmp);
	p2k_cnt=t;
	return(t);
}

// Read SEEM record from phone
int p2k_readSeem(int x, int y, unsigned char * seemBuf, int seemBufSize)
{
	unsigned char cmd1[0x10]={0xFF, 0xFF, 0x00, 0x20, 0x00, 0x08, 0x00, 0x00, 0xAA, 0xAA, 0xBB, 0xBB, 0x00, 0x00, 0x00, 0x00};\
	// 	unsigned char * mem;
	INT16 xxxx;
	INT16 yyyy;
	INT16 cnt;
	INT16 size;
	INT16 packId;
	int bufsize;
	unsigned char * buf;
	int adr;
	// 	int dadr;
	unsigned short myid;
	INT16 packSize;
	unsigned char packType;
	int seemPos;
	int j;
	unsigned char iserr;

	FUNC("p2k_readSeem");
	setInt16(cmd1+8,x);
	setInt16(cmd1+10,y);
	
	if (p2k_outData(cmd1,sizeof(cmd1))<0) RAISE(P2K_E_OUTDATA, "E001");
	if (p2k_inpSize(packetCount, sizeof(packetCount))<=2) RAISE(P2K_E_INPSIZE,"E002");
	cnt=getInt16(packetCount);
	size=getInt16(packetCount+2);
		
	bufsize=2*cnt+size+4;
	buf = (unsigned char *) malloc (bufsize);
	if (p2k_inpData(buf, cnt, bufsize)!=bufsize) RAISE(P2K_E_INPDATA,"E003");

	adr=6;
	seemPos=0;

	int i;
	for (i=0; i<cnt; i++)
	{
		packId=getInt16(buf+adr);
		
		myid= packId & 0xFFF;
		packType=buf[adr+2];
		packSize=getInt16(buf+adr+4);
		iserr=buf[adr+8];

		if ((myid != freeID) || (packType!=0x80) || (iserr))
		{
			if (packType==0x60) adr+=7; else adr+=8+packSize;
			printf("(E01)");
			return(P2K_E_OLD);
		}
		for (j=0; j<packSize-1; j++)
		{
			if (seemPos>=seemBufSize)
			{
				printf("(E_RS_01_%04d_%04d\n",seemBufSize,packSize-1);
				return(P2K_E_OLD);
			}
			seemBuf[seemPos++]=buf[adr+9+j];
		}
		break;
	}
	return(seemPos);
}

// Write SEEM record to phone
int p2k_writeSeem(int x, int y, unsigned char * seemBuf, int seemSize)
{
#define MAX_SEEM_SIZE 102400

	unsigned char * temp_seem;
	unsigned char * temp_packet;

	FUNC("p2k_writeSeem");
	
	temp_packet=(unsigned char *) malloc (MAX_SEEM_SIZE);
	temp_seem=temp_packet+16;

	int actualSize;
	int ret;

	actualSize=p2k_readSeem(x,y, temp_seem, MAX_SEEM_SIZE-16);
	actualSize=seemSize;

	if (actualSize != seemSize) RAISE(P2K_E_INCORRECT_SEEMSIZE, "E001");

	memcpy(temp_seem,seemBuf,seemSize);
	temp_packet[2]=0x00; temp_packet[3]=0x2F;
	setInt16(temp_packet+4,seemSize+8);
	
	temp_packet[6]=0; temp_packet[7]=0;
	setInt16(temp_packet+8,x);
	
	setInt16(temp_packet+10,y);
	
	temp_packet[12]=0; temp_packet[13]=0;
	setInt16(temp_packet+14,seemSize);

	ret=p2k_outData(temp_packet,seemSize+16);
	if (ret!=seemSize+16) RAISE(ret,"E002");

	if (p2k_inpSize(packetCount, sizeof(packetCount))<=2) RAISE(P2K_E_INPSIZE, "E003");

	INT16 pcnt;
	INT16 psize;
	int bufsize;
	pcnt=getInt16(packetCount);
	psize=getInt16(packetCount+2);
	
	bufsize=2*pcnt+psize+4;

	if (p2k_inpData(temp_packet, pcnt, bufsize)!=bufsize) RAISE(P2K_E_INPDATA, "E004");
	ret=p2k_check_packet_header(temp_packet, pcnt, 6,1 ,1);
	if (ret<0) RAISE(ret, "E005");

	free(temp_packet);
	return(0);
}

// --------------
// FSAC functions
// --------------

int p2k_FSAC_getVersion(char *buf)
{
	FUNC("p2k_FSAC_getVersion");
	
	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09};
	unsigned char * tmp;
	int sz;
	int buf_ps;
	int ps;
	
	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E001")
		if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE,"E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);	
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp, sz, 6, 1, 0)<0) RAISE(P2K_E_CPH, "E004");
	sprintf(buf,"%02x.%02x",tmp[14],tmp[15]);
	free(tmp);
	return 0;
}


int p2k_FSAC_searchRequest(char *request)
{
	FUNC("p2k_FSAC_searchRequest");
	unsigned char cmd[0x108]={0xFF, 0xFF, 0x00, 0x4A, 0x01, 0x00, 
			0x00, 0x00, 0x00, 0x00, 0x00, 0x07};
	unsigned char * tmp;
	int sz;
	INT16 t;
	int i;
	int l;
	int ps=12;
	int nslash=0;
	
	p2k_cnt=-1;
	if (request)
	{
		// Parsing query
		l=strlen(request);
		for (i=0; i<l; i++)
		{
			cmd[ps++]=0x00;
			cmd[ps++]=request[i];
			if (request[i]=='/')
			{
				if (++nslash==2)
				{
					cmd[ps++]=0xFF;
					cmd[ps++]=0xFE;
				}
			}		
		}
	}
	while (ps<108)
		cmd[ps++]=0x00;
	
	if (p2k_outData(cmd, sizeof(cmd))<=0) RAISE(P2K_E_OUTDATA,"E001");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	
	if (p2k_inpData(tmp, 0x01, sz)<0) RAISE(P2K_E_INPDATA,"E003");
	
	// 	This package has incorrect yy. So we can not check it.
	if (p2k_check_packet_header(tmp,sz,6,1,0)<0) RAISE(P2K_E_CPH,"E004");
	t=getInt16(tmp+14);
	free(tmp);
	p2k_cnt=t;
	return(t);
}

// Get File List
int p2k_FSAC_fileList(p2k_onFile onGetFile)
{
	FUNC("p2k_fileList");
	
	int fCnt;
	unsigned char cmd[] = {0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x04, 0x00, 0x00, 0x00,  0x00, 0x00, 0x08};
	unsigned char * tmp = 0;
	unsigned char * recAddr;
	int sz;
	int sz1;
	INT16 rCnt;
	int inRec;
	int i;
	int curidx=0;
	int fnameSize;
	int recSize;
	
	struct p2k_fileInfo nfo;
	fCnt=p2k_cnt;
	if (fCnt<0) RAISE(fCnt, "E000");
	sz=-1;
	
	while (curidx < fCnt)
	{
		if (p2k_outData(cmd, sizeof(cmd))<=0) RAISE (P2K_E_OUTDATA, "E001");
		if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
		sz1=p2k_get_cmd_size(packetCount);
		if (sz1>sz)
		{
			if (sz!=-1) free(tmp);
			sz=sz1;
			tmp=(unsigned char *) malloc (sz);
		}
		
		if (p2k_inpData(tmp, 0x01, sz1)<0) RAISE (P2K_E_INPDATA, "E003");
		if (p2k_check_packet_header(tmp,sz,6,1,1)<0) RAISE(P2K_E_CPH, "E004");
		
		rCnt=tmp[0x0f];
		
		inRec=rCnt;	//Files in reply. We must calculate rec size - FIX for C350
		if ((sz1-0x12) % inRec)
		{
		RAISE(P2K_E_BUG,"(BUG) Unsupported filelist answer: sz1=%d, inRec=%d. Please Report!!!", sz1, inRec);
		}
		recSize=(sz1-0x12)/inRec;
		fnameSize=recSize-8;
		
		recAddr=tmp+0x12;
		for (i=0; i<inRec; i++)
		{
			curidx++;	
			nfo.id=curidx;
			strcpy(nfo.name, (char *) recAddr);
			nfo.size=getInt32(recAddr+fnameSize+4);
			nfo.owner=recAddr[fnameSize+3];
			nfo.attr=recAddr[fnameSize+1];
			
			onGetFile(nfo);
			
			recAddr+=recSize;
		}
	}
	if (tmp) free(tmp);
	return(0);
}


// Get Volume list separated by space. For example "/a /b"
int p2k_FSAC_getVolumes(char *buf)
{
	FUNC("p2k_FSAC_getVolumes");
	
	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A};
	unsigned char * tmp;
	int sz;
	int buf_ps;
	int ps;
	
	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E001")
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE,"E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp, sz, 6, 1, 1)<0) RAISE(P2K_E_CPH, "E004");
	buf_ps=0;
	ps=15;
	while (tmp[ps]!=0)
	{
		if (tmp[ps]==0xfe) tmp[ps]=0x20; //space
		buf[buf_ps++]=tmp[ps];
		ps+=2;
	}
	buf[buf_ps++]=0;
	free(tmp);
	return(0);
}

// Get Volume free space. By default for /a
int p2k_FSAC_getVolumeFreeSpace(char *volume)
{
	FUNC("p2k_FSAC_getVolumeFreeSpace");
	
	unsigned char cmd[0x108] ={0xFF, 0xFF, 0x00, 0x4A, 0x01, 0x00, 0x00, 0x00, 
			0x00, 0x00, 0x00, 0x0B, 0x00, 0x00,0x00,0x00};
	unsigned char * tmp;
	int i;
	int ps;
	int sz;
	INT32 lng;
	
	if (volume)
	{
		int size=strlen(volume);
		ps=14;
		for (i=0; i< size; i++)
		{
			cmd[ps++]=0;
			cmd[ps++]=volume[i];
		}
		cmd[ps++]=0;
		cmd[ps++]=0;
	}
	if (p2k_outData(cmd, sizeof(cmd))<=0) RAISE(P2K_E_OUTDATA, "E001");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp, sz, 6, 1, 1)<0) RAISE(P2K_E_CPH, "E004");
	lng=getInt32(tmp+14);
	free(tmp);
	return(lng);	
}

// Delete file from phon.
int p2k_FSAC_delete(char * fname)
{
	FUNC("FSAC_Delete")
	//                        0     1     2     3     4     5     6     7    8       9     A     B    C     D     E     F
			unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0xAA, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05};
	unsigned char * buf;
	unsigned char * tmp;
	int bufsize;
	int sz;
	INT16 t;

	bufsize=sizeof(cmd)+strlen(fname);
	buf=(unsigned char *) malloc (bufsize);
	memcpy(buf,cmd,sizeof(cmd));
	memcpy(buf+sizeof(cmd), fname, strlen(fname));
	setInt16(buf+0x04,bufsize-8);

	if (p2k_outData(buf, bufsize)<=0) RAISE (P2K_E_OUTDATA, "E001");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);

	tmp=(unsigned char*) malloc (sz);

	if (p2k_inpData(tmp, 0x01, sz)<0) RAISE (P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH, "E004");

	free(tmp);
	free(buf);
	return(0);

}

// Open file
int p2k_FSAC_open(char * fname, unsigned char attr)
{
	FUNC("FSAC_Open");
	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0xAA, 0xAA, 0x00, 0x00,   0x00, 0x00, 0x00, 0x00,   0x00, 0x00, 0x00, 0xAA};
	int bufsize;
	unsigned char * buf;
	unsigned char * tmp;
	int sz;
	INT16 tWord;

	bufsize=sizeof(cmd)+strlen(fname);
	buf=(unsigned char *)malloc(bufsize);
	memcpy(buf,cmd, sizeof(cmd));
	memcpy(buf+sizeof(cmd),fname,strlen(fname));

	setInt16(buf+0x04,bufsize-8);
	
	if (attr!=0xFF)
		buf[0x0F]=attr;
	else
	{
		buf[0x0F]=0xFF;
		buf[0x0E]=0xFF;
		buf[0x0D]=0xFF;
		buf[0x0C]=0xFF;
	}

	if (p2k_outData(buf, bufsize)<=0) RAISE (P2K_E_OUTDATA, "E001");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E002");
	sz=p2k_get_cmd_size(packetCount);

	tmp=(unsigned char*) malloc (sz);

	if (p2k_inpData(tmp, 0x01, sz)<0) RAISE (P2K_E_INPDATA, "E003");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH,"E004");

	free(tmp);
	free(buf);
	return(0);
}

// Close File
int p2k_FSAC_close()
{
	FUNC("FSAC_Close");

	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04};
	unsigned char * tmp;
	int sz;

	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH, "E04");
	free(tmp);
	return(0);
}

// Seek
int p2k_FSAC_seek(unsigned long offset, char dir)
{
	FUNC("FSAC_Seek");
	unsigned char cmd[]={0x00, 0x7C, 0x00, 0x4A, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xAA, 0xAA, 0xAA, 0xAA, 0x00 };
	unsigned char * tmp;
	int sz;

	INT32 t;
	setInt32(cmd+0x0C,offset);
	
	cmd[0x10]=dir;

	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH,"E04");
	free(tmp);
	return(0);
}

// Read from file
int p2k_FSAC_read(unsigned char * buf, int size)
{
	FUNC("FSAC_Read");
	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0xAA, 0xAA};
	INT16 t;
	int sz;
	unsigned char * tmp;
	int ret;


		
	if (size>0x400) RAISE(P2K_E_BUFFER_TOOLONG, "E00");
	setInt16(cmd+0x0e,t);
	
	if (p2k_outData(cmd, sizeof(cmd))<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E02");
	
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)!=size+0xE) RAISE(P2K_E_INPDATA, "E03");
	if ((ret=p2k_check_packet_header(tmp,sz,6,1,0))!=size) RAISE(P2K_E_CPH,"E04");

	memcpy(buf, tmp+0x0E, size);

	free(tmp);
	return(0);
}

// Write to file
int p2k_FSAC_write(unsigned char * tbuf, int size)
{
	FUNC("FSAC_Write");
	unsigned char cmd[] ={0xFF, 0xFF, 0x00, 0x4A, 0xAA, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0xAA, 0xAA};
	unsigned char * buf;
	unsigned char * tmp;
	int bufsize;
	INT16 t;
	int sz;

	if (size>0x400) RAISE(P2K_E_BUFFER_TOOLONG, "E00");

	bufsize=size+sizeof(cmd);
	buf=0;
	buf = (unsigned char *) malloc (bufsize);
	memcpy(buf, cmd, sizeof(cmd));
	memcpy(buf+0x10, tbuf, size);

	setInt16(buf+0x04,size+8);

	setInt16(buf+0x0e,size);

	if (p2k_outData(buf, bufsize)<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=0;
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH, "E04");

	free(buf);
	free(tmp);

	return(0);
}

int p2k_proba()
{
	p2k_reboot();
	FUNC("p2k_proba");
	unsigned char cmd1[]={0xFF, 0xFF, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x00};
	unsigned char * tmp;
	int sz;
	if (p2k_outData(cmd1, sizeof(cmd1))<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))<0) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	free(tmp);
	return(0);	
}

int p2k_FSAC_createDir(char * dirname)
{
	FUNC("FSAC_createDir");

	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0E,
		0x00, 0x00, 0x00, 0x10};
	unsigned char *buf;
	unsigned char *tmp;
	int sz;
			
	sz=strlen(dirname)+0x10;
	buf=(unsigned char*) malloc(sz);
	memcpy(buf,cmd,sizeof(cmd));
	memcpy(buf+0x10, dirname, strlen(dirname));
	buf[0x4]=(sz-8) / 0x100;
	buf[0x5]=(sz-8) % 0x100;
	
	if (p2k_outData(buf, sz)<0) RAISE(P2K_E_OUTDATA, "E01");
	// 	usleep(100000);
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH,"E04");
	free(tmp);
	return(0);	
}

int p2k_FSAC_removeDir(char * dirname)
{
	FUNC("FSAC_removeDir");

	unsigned char cmd[]={0xFF, 0xFF, 0x00, 0x4A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F,
		0x00, 0x00, 0x00, 0x10};
	unsigned char *buf;
	unsigned char *tmp;
	int sz;
			
	sz=strlen(dirname)+0x10;
	buf=(unsigned char*) malloc(sz);
	memcpy(buf,cmd,sizeof(cmd));
	memcpy(buf+0x10, dirname, strlen(dirname));
	buf[0x4]=(sz-8) / 0x100;
	buf[0x5]=(sz-8) % 0x100;
	
	if (p2k_outData(buf, sz)<0) RAISE(P2K_E_OUTDATA, "E01");
	if (p2k_inpSize(packetCount, sizeof(packetCount))!=4) RAISE(P2K_E_INPSIZE, "E02");
	sz=p2k_get_cmd_size(packetCount);
	tmp=(unsigned char *)malloc(sz);
	if (p2k_inpData(tmp , 0x01 , sz)<0) RAISE(P2K_E_INPDATA, "E03");
	if (p2k_check_packet_header(tmp,sz,6,0,0)<0) RAISE(P2K_E_CPH,"E04");
	free(tmp);
	return(0);	
}

// FLASH IO

int p2k_newDevices()
{
	return usb_find_devices();
}

int flash_getDeviceList(struct flash_device* arr, int cnt)
{
// 	usb_find_busses();
	usb_find_devices();

	struct usb_bus *bus;
	struct usb_device *dev;
	usb_dev_handle *udev;
	char devstr[1024];
	int filled=0;
	
	
	for (bus = usb_busses; bus; bus = bus->next)
	{
		for (dev = bus->devices; dev; dev = dev->next)
		{
			int tmp=dev->descriptor.idProduct%16;
 			//printf("--> %x %d\n",dev->descriptor.idProduct,tmp%16);
			if (dev->descriptor.idVendor==0x22b8 && (tmp==3 || tmp==8 || tmp==9))
			{
				//Searching for interfaces with 2 bulk endpoints
				if (dev->descriptor.bNumConfigurations==1 && dev->config)
				{
					int nifaces=dev->config[0].bNumInterfaces;
					int iface;
					for (iface=0; iface<nifaces; iface++)
					{
						int nendpoints=dev->config[0].interface[iface].altsetting[0].bNumEndpoints;
						if (nendpoints==2)
						{
							int ep1=dev->config[0].interface[iface].altsetting[0].endpoint[0].bEndpointAddress;
							int ep2=dev->config[0].interface[iface].altsetting[0].endpoint[1].bEndpointAddress;
							
							if (ep1>ep2)
							{
								int t=ep1; ep1=ep2; ep2=t;
							}
							
							if ((dev->config[0].interface[iface].altsetting[0].endpoint[0].bmAttributes & 0x02) &&
								ep1<0x80 &&
								(dev->config[0].interface[iface].altsetting[0].endpoint[1].bmAttributes & 0x02) &&
								ep2>0x80)
							{
								int iiface=dev->config[0].interface[iface].altsetting[0].iInterface;
								char s[1024];
								s[0]=0;
								devstr[0]=0;
								if (iiface || dev->descriptor.iProduct)
								{
									usb_dev_handle *udev;
									udev=usb_open(dev);
									if (udev)
									{
										if (iiface) usb_get_string_simple(udev,iiface,s,sizeof(s)-1);
										if (dev->descriptor.iProduct)
											usb_get_string_simple(udev,dev->descriptor.iProduct,devstr,sizeof(devstr)-1);
										usb_close(udev);
									}
								}
								
								printf("Flash device: %04x:%04x. [%s::%s] interface=%0x2d, endpoints=%02x, %02x\n", dev->descriptor.idVendor,dev->descriptor.idProduct,devstr,s,iface,
								ep1,ep2
								);
								
								arr[filled].vendor=0x22b8;
								arr[filled].product=dev->descriptor.idProduct;
								arr[filled].interface=iface;
								arr[filled].epOut=ep1;
								arr[filled].epIn=ep2;
								strcpy(arr[filled].productStr,devstr);
								strcpy(arr[filled].interfaceStr,s);
								filled++;
							}
						}
					}
				}
			}
		}
	}
	return filled;
}

void flash_setDevice(const struct flash_device dev)
{
	memcpy(&flash_dev,&dev,sizeof(struct flash_device));	
}

int flash_open()
{
	usb_find_busses();
	usb_find_devices();

	struct usb_bus *bus;
	struct usb_device *dev;
	int isok=0;
	
	for (bus = usb_busses; bus; bus = bus->next)
	{
		for (dev = bus->devices; dev; dev = dev->next)
		{
			if (dev->descriptor.idVendor==flash_dev.vendor && dev->descriptor.idProduct==flash_dev.product)
			{
				flash_udev=usb_open(dev);
				if (!flash_udev) ERR("open");
				isok=1;
				break;
			}
		}
	}
	
	if (!isok) return -1;
		
	if (usb_set_configuration(flash_udev,1)) ERR("usb_set_configuration");
	if (usb_claim_interface(flash_udev,flash_dev.interface)) ERR("usb_claim_interface");
	
	
	usb_clear_halt(flash_udev, 0x01);
	usb_clear_halt(flash_udev, 0x82);
	
	return 0;
}

int flash_replug()
{
	return usb_reset(flash_udev);
}

void flash_close()
{
	return usb_close(flash_udev);
}

int flash_write(char *buf, int cnt, int timeout)
{
#ifdef MY_DEBUG	
	int l;
	printf("<=== WRITE");
	for (l=0; l<cnt; l++){if (l%16==0) printf("\n"); printf(" %02x",(unsigned char) buf[l]);}
	printf("\n");
#endif
	int r=usb_bulk_write(flash_udev, flash_dev.epOut, buf, cnt, timeout);
	return r;
}

int flash_writeCommand(char *cmd, char *par, int parsize, int timeout)
{
	int r;
	int l=strlen(cmd);
	strcpy(flash_wasCmd, cmd);
	int ps=0;
	flash_outBuf[ps++]=0x02;
	memcpy(flash_outBuf+ps, cmd, l);
	ps+=l;
	
	if (parsize)
	{
		flash_outBuf[ps++]=0x1e;
		memcpy(flash_outBuf+ps, par, parsize);
		ps+=parsize;
	}
	
	flash_outBuf[ps++]=0x03;
	
#ifdef MY_DEBUG	
	printf("<=== WRITE: %s", cmd);
	for (l=0; l<ps; l++) {if (l%16==0) printf("\n");printf(" %02x",(unsigned char) flash_outBuf[l]);}
	printf("\n");
#endif
	
	r=usb_bulk_write(flash_udev, flash_dev.epOut, flash_outBuf, ps, timeout);
	if (r<0) ERR("write");
	return r;
}

int flash_read(char *buf, int cnt, int timeout)
{
	int r=usb_bulk_read(flash_udev, flash_dev.epIn, buf, cnt, timeout);
	if (r<0) return r;
		
#ifdef MY_DEBUG	
	int l;
	printf("===> READ");
	for (l=0; l<r; l++){if (l%16==0) printf("\n"); printf(" %02x",(unsigned char) buf[l]);}
	printf("\n");
#endif
	
	return r;
}

int flash_readCommand(char * par, int timeout)
{
	int l;
	int r=usb_bulk_read(flash_udev, flash_dev.epIn, flash_inpBuf, sizeof(flash_inpBuf), timeout);
	flash_inpBuf[r]=0;
	if (r<=0) return -1;
	
#ifdef MY_DEBUG	
	printf("===> READ: %s", flash_inpBuf);
	for (l=0; l<r; l++){if (l%16==0) printf("\n"); printf(" %02x",(unsigned char) flash_inpBuf[l]);}
	printf("\n");
#endif
	int ps=1;
	while (flash_inpBuf[ps]!=0x03 && flash_inpBuf[ps]!=0x1e)
		ps++;
	char ch=flash_inpBuf[ps];
	flash_inpBuf[ps]=0;
	if (ch==0x03) return 0;
	
	par[r-ps-1]=0;
			
	memcpy(par, flash_inpBuf+ps+1, r-ps-1);
	return (r-ps-1);
}
