/***************************************************************************
 *   Copyright (C) 2005 by Dmitry Nezhevenko                               *
 *   dion@inhex.net                                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QtGui>

#include "mainwindow.h"
#include "phone.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	ui.setupUi(this);
	phone=new Phone(this);

	connect(phone,SIGNAL(onMessage(const QString& )),
		this, SLOT(sltMessage(const QString& )));

	connect(phone,SIGNAL(onProgress1(int, int )),
		this, SLOT(sltProgress1(int, int )));

	connect(phone,SIGNAL(onProgress2(int, int )),
		this, SLOT(sltProgress2(int, int )));

	connect(phone,SIGNAL(onDeviceList(void*, int )),
		this, SLOT(sltDeviceList(void*, int )),Qt::QueuedConnection);

// 	connect(this, SIGNAL(sigTest()), p
	devLst=0;
	devCnt=0;
// 	setDevice(-1);
	phone->start();
}

MainWindow::~ MainWindow()
{
	phone->terminate();
	phone->wait();
}

void MainWindow::sltDeviceList(void *lst1, int cnt)
{
	struct flash_device* lst;
	lst=(struct flash_device*) lst1;
	if (devLst)
	{
		free(devLst);
		devLst=0;
	}
	devCnt=cnt;
	devLst = (struct flash_device *) malloc (sizeof(struct flash_device)*cnt);
	memcpy(devLst,lst,sizeof(struct flash_device)*cnt);
	ui.lstDevices->clear();
	QString st;
	for (int i=0; i<cnt; i++)
	{
		st=QString("%1:%2").arg(
			QString::number(lst[i].vendor,16).rightJustified(4,QChar('0')),
			QString::number(lst[i].product,16).rightJustified(4,QChar('0')));

			if (!strlen(lst[i].productStr) || !strlen(lst[i].interfaceStr))
			{
				st.append(" [Unable to get Device name]");
			}
			else
			{
				st.append(QString(" [%1 :: %2]").arg(lst[i].productStr, lst[i].interfaceStr));
			}

		ui.lstDevices->addItem(st);
	}
	if (cnt==0) setDevice(-1); else
	if (cnt==1) setDevice(0);
}

void MainWindow::sltMessage(const QString& msg)
{
	ui.textBrowser->append(msg);
}

void MainWindow::setDevice(int i)
{
	QString st;
	if (i<0 || i>devCnt) st="(none)";
	else
	{
		st=QString("%1:%2").arg(
			QString::number(devLst[i].vendor,16).rightJustified(4,QChar('0')),
			QString::number(devLst[i].product,16).rightJustified(4,QChar('0')));

		st.append(QString(" [%1 :: %2]").arg(devLst[i].productStr, devLst[i].interfaceStr));
	}
	ui.txtCurrentDevice->setText(st);

	phone->setDevice(i);
}

void MainWindow::on_btnUseDevice_clicked()
{
	setDevice(ui.lstDevices->currentRow());
}

void MainWindow::on_btnSendCommand_clicked()
{
	qDebug()<<"Click";
	QString s1=ui.txtCmd->text();
	QString s2=ui.txtArg->text();
	phone->sendCmd(s1,s2);
}

void MainWindow::on_btnSendLoader_clicked()
{
	QString fn=QFileDialog::getOpenFileName();
	if (fn.isNull()) return;
	phone->sendLoader(ui.txtFileAddr->text(),ui.txtJumpAddr->text(),fn);
}

void MainWindow::on_btnSendFile_clicked()
{
	QString fn=QFileDialog::getOpenFileName();
	phone->sendFile(ui.txtFileAddr->text(),fn);
}

void MainWindow::on_btnClear_clicked()
{
	ui.textBrowser->clear();
}

void MainWindow::sltProgress1(int a, int b)
{
	if (b==0) b=1;
	ui.progressBar1->setMaximum(b);
	ui.progressBar1->setValue(a);
}

void MainWindow::sltProgress2(int a, int b)
{
	int a1;
	if (b==0)
		a1=0;
	else
		a1=(int)(((double) a)/((double)b)*100.0);
// 	qDebug() << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" <<  a << b << a1;
	ui.progressBar2->setValue(a1);
}

void MainWindow::on_btnErase_clicked()
{
	if (QMessageBox::question(this,tr("Confirm"),tr("Do you want to erase phone memory?"),QMessageBox::Yes, QMessageBox::No)!=QMessageBox::Yes) return;

	phone->erase();
}

void MainWindow::on_btnFullFlash_clicked()
{
	QString fn=QFileDialog::getOpenFileName();
	phone->fullFlash(fn);
}

