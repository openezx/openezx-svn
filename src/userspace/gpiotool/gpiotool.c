/*
 * gpiotool - user-level access to PXA27x GPIO registers
 *
 * (C) 2006 by Harald Welte <laforge@openezx.org>
 *
 * This program is Free Software and licensed under GNU GPL Version 2,
 * as published by the Free Software Foundation.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>

#define _GNU_SOURCE
#include <getopt.h>

#include "mmio.h"
#include "gpio.h"
#include "gpio_regs.h"

static unsigned long get_physaddr(char *optarg)
{
	unsigned long phys_addr;
	char *endptr;

	phys_addr = strtoul(optarg, &endptr, 16);
	if (phys_addr == 0 && optarg == endptr)
		phys_addr = mmio_getmembyname(optarg);
	if (phys_addr == 0) {
		fprintf(stderr, "unknown reg/addr `%s'\n", optarg);
		exit(1);
	}

	return phys_addr;
}

/* Parse an 'alternate function' string into its numerical form */
static unsigned int get_af(char *arg)
{
	char *cur;
	unsigned int af = 0;

	for (cur = strtok(arg, ":|"); cur;  cur = strtok(NULL, ":|")) {
		if (!strcmp(cur, "OUT"))
			af |= GPIO_OUT;
		else if (!strcmp(cur, "AF1"))
			af |= GPIO_ALT_FN_1_IN;
		else if (!strcmp(cur, "AF2"))
			af |= GPIO_ALT_FN_2_IN;
		else if (!strcmp(cur, "AF3"))
			af |= GPIO_ALT_FN_3_IN;
	}
	return af;
}

static struct option opts[] = {
	/* genric mmio functions */
	{ "read",	1, 0, 'r' },
	{ "write",	1, 0, 'w' },
	/* read/write a single gpio pin */
	{ "read-gpio",	1, 0, 'R' },
	{ "write-gpio",	1, 0, 'W' },
	{ "dump-gpio",	0, 0, 'D' },
	/* get/set the alternate function of a gpio pin */
	{ "read-af",	1, 0, 'g' },
	{ "write-af",	1, 0, 's' },
	{ "dump-af", 	0, 0, 'd' },
	{ "help",	0, 0, 'h' },
	{ "msleep",	1, 0, 'm' }
};

int main(int argc, char **argv)
{
	int c;
	unsigned long phys_addr;
	u_int32_t gpio, val;

	mmio_init();

	while (1) {
		int option_index = 0;

		c = getopt_long(argc, argv, "r:w::R:W::Dg:s::dhm:", opts,
				&option_index);
		if (c == -1)
			break;

		switch (c) {
			int i;
		case 'r':
			phys_addr = get_physaddr(optarg);
			printf("Read from 0x%08lx: 0x%08x\n", phys_addr, mmio_rd(phys_addr));
			break;
		case 'w':
			phys_addr = get_physaddr(optarg);
			val = strtoul(argv[optind], NULL, 16);
			mmio_wr(phys_addr, val);
			printf("Wrote to  0x%08lx: 0x%08x\n", phys_addr, val);
			break;
		case 'R':
			gpio = strtoul(optarg, NULL, 0);
			printf("GPIO %3u is %u\n", gpio, pxa_gpio_read(gpio));
			break;
		case 'W':
			gpio = strtoul(optarg, NULL, 0);
			val = strtoul(argv[optind], NULL, 0);
			pxa_gpio_set(gpio, val);
			printf("GPIO %3u wrote %u\n", gpio, val);
			break;
		case 'D':
			for (i = 0; i < 119; i++)
				printf("GPIO %3u is %u\n", i, pxa_gpio_read(i));
			break;
		case 'g':
			gpio = strtoul(optarg, NULL, 0);
			val = pxa_gpio_mode_get(gpio);
			printf("GPIO %3u has mode %s\n", gpio, 
				pxa_gpio_mode2txt(val));
			break;
		case 's':
			gpio = strtoul(optarg, NULL, 0);
			val = get_af(argv[optind]);
			pxa_gpio_mode_set(gpio | val);
			printf("GPIO %3u set to mode %s(0x%x)\n",
				gpio, pxa_gpio_mode2txt(val), val);
			break;
		case 'd':
			for (i = 0; i < 119; i++) {
				val = pxa_gpio_mode_get(i);
				printf("GPIO %3u has mode %s\n", i, 
					pxa_gpio_mode2txt(val));
			}
			break;
		case 'h':
			printf("You have to specify one of --read, --write, "
			       "--read-gpio, --write-gpio, --read-af or --write-af\n");
			break;
		case 'm':
			val = strtoul(optarg, NULL, 10);
			usleep(val*1000);
			break;
		}
	}

	mmio_fini();

	return 0;
}
