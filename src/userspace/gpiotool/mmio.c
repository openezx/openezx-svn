/*
 * gpiotool - user-level access to PXA27x GPIO registers
 *
 * (C) 2006 by Harald Welte <laforge@openezx.org>
 *
 * This program is Free Software and licensed under GNU GPL Version 2,
 * as published by the Free Software Foundation.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "mmio_regs.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#if 0
#define DEBUGP(x, args ...) fprintf(stderr, x, ## args)
#else
#define DEBUGP(x, args ...)
#endif

static int fd;
static int page_size;

unsigned long mmio_getmembyname(char *name)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(regnames); i++) {
		if (!strcasecmp(name, regnames[i].name))
			return regnames[i].addr;
	}

	return 0L;
}

static int phys2page_offset(unsigned long phys_addr, unsigned long *page_addr)
{
	unsigned long *mem;
	unsigned long i;

	phys_addr &= ~(sizeof(mem[0]) - 1);
	*page_addr = phys_addr & ~(page_size - 1);
	i = (phys_addr - *page_addr) / sizeof(mem[0]);

	return i;
}

u_int32_t mmio_rd(unsigned long phys_addr)
{
	unsigned long page_addr, i;
	unsigned long *mem;
	u_int32_t ret;

	DEBUGP("mmio_rd(phys_addr=0x%08lx): ", phys_addr);

	i = phys2page_offset(phys_addr, &page_addr);

	mem = mmap(NULL, page_size, PROT_READ, MAP_SHARED, fd, page_addr);
	if (mem == MAP_FAILED) {
		perror("mmap()");
		return -1;
	}

	ret = mem[i];

	munmap(mem, page_size);

	DEBUGP("returning 0x%08x\n", ret);

	return ret;
}

int mmio_wr(unsigned long phys_addr, u_int32_t value)
{
	unsigned long page_addr, i;
	unsigned long *mem;

	DEBUGP("mmio_wr(phys_addr=0x%08lx, value=0x%08x): ",
		phys_addr, value);

	i = phys2page_offset(phys_addr, &page_addr);

	mem = mmap(NULL, page_size, PROT_WRITE, MAP_SHARED, fd, page_addr);
	if (mem == MAP_FAILED) {
		perror("mmap()");
		return -1;
	}

	mem[i] = value;

	munmap(mem, page_size);

	DEBUGP("returning success\n");

	return 0;
}

int mmio_init(void)
{
	page_size = getpagesize();
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd < 0)
		return fd;

	return 0;
}

void mmio_fini(void)
{
	close (fd);
}

